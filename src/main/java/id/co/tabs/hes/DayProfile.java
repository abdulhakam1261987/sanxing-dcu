package id.co.tabs.hes;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DayProfile {
//	@JsonProperty("DAY_ID")
	private int DAY_ID;
//	@JsonProperty("items")
	private List<DayProfileAction> items;

	public DayProfile(){};
	/**
	 * @return the dAY_ID
	 */
	@JsonGetter("DAY_ID")
	public int getDAY_ID() {
		return DAY_ID;
	}
	/**
	 * @param dAY_ID the dAY_ID to set
	 */
	public void setDAY_ID(int dAY_ID) {
		DAY_ID = dAY_ID;
	}
	/**
	 * @return the items
	 */
	@JsonGetter("items")
	public List<DayProfileAction> getItems() {
		return items;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(List<DayProfileAction> items) {
		this.items = items;
	}

}
