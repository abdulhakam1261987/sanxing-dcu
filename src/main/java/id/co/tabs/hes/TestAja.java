/*
 * 
 */
package id.co.tabs.hes;

import gurux.common.GXCommon;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.Security;
import gurux.dlms.secure.GXDLMSSecureClient;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Tab Solutions
 */
public class TestAja {
    
    public static void main(String[] args) {
        try {
            GXDLMSSecureClient client = new GXDLMSSecureClient(true);
            client.setUseLogicalNameReferencing(true);
            client.setAuthentication(Authentication.LOW);
            client.setClientAddress(48);
            client.setInterfaceType(InterfaceType.HDLC);
            client.setServerAddress(GXDLMSSecureClient.getServerAddress(1, 12288));
            client.setPassword("22222222");
//            if (client.getAuthentication().equals(Authentication.HIGH_GMAC)) {
//                client.getCiphering().setSecurity(Security.AUTHENTICATION_ENCRYPTION);
//                client.getCiphering().setAuthenticationKey("3333333333333333".getBytes("ASCII"));
//                client.getCiphering().setBlockCipherKey(blo   ckCipherKey.getBytes("ASCII"));
//                client.getCiphering().setSystemTitle("AUX00000".getBytes("ASCII"));
//                if (invocation_counter != 0) {
//                    client.getCiphering().setInvocationCounter(invocation_counter);
//                }
//            } else {
//                client.setPassword("22222222");
//            }
            byte[][] bses = client.aarqRequest();
            for (int i = 0; i < bses.length; i++) {
                System.out.println(GXCommon.bytesToHex(bses[i]));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
}
