package id.co.tabs.hes;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DayProfileList {
	List<DayProfile> listDayProfile;

	/**
	 * @return the listDayProfile
	 */
	public List<DayProfile> getListDayProfile() {
		return listDayProfile;
	}

	/**
	 * @param listDayProfile the listDayProfile to set
	 */
	public void setListDayProfile(List<DayProfile> listDayProfile) {
		this.listDayProfile = listDayProfile;
	}

}
