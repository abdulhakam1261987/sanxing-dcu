/*
 * 
 */
package id.co.tabs.hes.object;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class MeterType {

    private final int meterType;

    private final int meterCommType;

    private final int meterProtocolType;

    private final int meterPrepaidType;

    private final int meterWiringType;

    private final String password;

    public MeterType(int meterType, int meterCommType, int meterProtocolType, int meterPrepaidType, 
            int meterWiringType) {
        if (meterType > 2 && meterType < 1) {
            this.meterType = 1;
        } else {
            this.meterType = meterType;
        }
        this.meterCommType = meterCommType;
        this.meterProtocolType = meterProtocolType;
        this.meterPrepaidType = meterPrepaidType;
        if (meterWiringType > 3 && meterWiringType < 1) {
            this.meterWiringType = 1;
        } else {
            this.meterWiringType = meterWiringType;
        }
        this.password = "";
    }

    public MeterType(int meterType, int meterCommType, int meterProtocolType, int meterPrepaidType, 
            int meterWiringType, String password) {
        if (meterType > 2 && meterType < 1) {
            this.meterType = 1;
        } else {
            this.meterType = meterType;
        }
        this.meterCommType = meterCommType;
        this.meterProtocolType = meterProtocolType;
        this.meterPrepaidType = meterPrepaidType;
        if (meterWiringType > 3 && meterWiringType < 1) {
            this.meterWiringType = 1;
        } else {
            this.meterWiringType = meterWiringType;
        }
        this.password = password;
    }

    public long getMeterType() {
        return meterType;
    }

    public long getMeterCommType() {
        return meterCommType;
    }

    public long getMeterProtocolType() {
        return meterProtocolType;
    }

    public long getMeterPrepaidType() {
        return meterPrepaidType;
    }

    public long getMeterWiringType() {
        return meterWiringType;
    }

    public String getPassword() {
        return password;
    }
    
}
