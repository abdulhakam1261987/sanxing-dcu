package id.co.tabs.hes.object;

import gurux.dlms.GXDLMSGateway;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.Security;
import gurux.dlms.secure.GXDLMSSecureClient;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class Meter {

    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(Meter.class.getName());
    /**
     * Nomor meter
     */
    private String noMeter;
    /**
     * True jika pembacaan menggunakan Logical Name. False jika pembacaan
     * menggunakan Short Name. Default : true.
     */
    private boolean useLogicalNameReferencing;
    /**
     * Jenis authentikasi yang digunakan meter. Available value (None, Low,
     * High, HighMd5, HighSha1, HighGMac, HighSha256). Default : Low.
     *
     * @see Authentication
     */
    private Authentication authentication;
    /**
     * Interface Type yang digunakan meter. Available value (HDLC, WRAPPER, PDU,
     * WIRELESS_MBUS). Default : HDLC.
     *
     * @see InterfaceType
     */
    private InterfaceType interfaceType;
    /**
     * Meter client address. Default : 0.
     */
    private int clientAddress;
    /**
     * Meter logical address. Default : 0.
     */
    private int logicalAddress;
    /**
     * Meter physical address. Default : 0.
     */
    private int physicalAddress;
    /**
     * Meter Address Size. Default : 0.
     */
    private int addressSize;
    /**
     * Password meter. Digunakan jika meter menggunakan Low Authentication.
     * Default : null.
     */
    private String password;
    /**
     * Meter security. Available value (NONE, AUTHENTICATION, ENCRYPTION,
     * AUTHENTICATION_ENCRYPTION). Default : NONE.
     *
     * @see Security
     */
    private Security security;
    /**
     * Meter system title. Default : null.
     */
    private String systemTitle;
    /**
     * Meter block cipher key. Default : null.
     */
    private String blockCipherKey;
    /**
     * Meter authentication key. Default : null.
     */
    private String authenticationKey;
    /**
     * Meter invocation counter. Default : 0.
     */
    private int invocation_counter;

    public Meter() {
        this.noMeter = "";
        this.useLogicalNameReferencing = true;
        this.authentication = Authentication.NONE;
        this.interfaceType = InterfaceType.HDLC;
        this.clientAddress = 16;
        this.logicalAddress = 0;
        this.physicalAddress = 1;
        this.addressSize = 0;
        this.password = "";
        this.security = Security.NONE;
        this.systemTitle = "";
        this.blockCipherKey = "";
        this.authenticationKey = "";
        this.invocation_counter = 0;
    }

    public Meter(String noMeter, boolean useLogicalNameReferencing, Authentication authentication, 
            InterfaceType interfaceType, int clientAddress, int logicalAddress, int physicalAddress, 
            int addressSize, String password) {
        this.noMeter = noMeter;
        this.useLogicalNameReferencing = useLogicalNameReferencing;
        this.authentication = authentication;
        this.interfaceType = interfaceType;
        this.clientAddress = clientAddress;
        this.logicalAddress = logicalAddress;
        this.physicalAddress = physicalAddress;
        this.addressSize = addressSize;
        this.password = password;
        this.security = Security.NONE;
        this.systemTitle = "";
        this.blockCipherKey = "";
        this.authenticationKey = "";
        this.invocation_counter = 0;
    }

    public Meter(String noMeter, boolean useLogicalNameReferencing, Authentication authentication, 
            InterfaceType interfaceType, int clientAddress, int logicalAddress, int physicalAddress, 
            int addressSize, String password, Security security, String systemTitle, String blockCipherKey, 
            String authenticationKey, int invocation_counter) {
        this.noMeter = noMeter;
        this.useLogicalNameReferencing = useLogicalNameReferencing;
        this.authentication = authentication;
        this.interfaceType = interfaceType;
        this.clientAddress = clientAddress;
        this.logicalAddress = logicalAddress;
        this.physicalAddress = physicalAddress;
        this.addressSize = addressSize;
        this.password = password;
        this.security = security;
        this.systemTitle = systemTitle;
        this.blockCipherKey = blockCipherKey;
        this.authenticationKey = authenticationKey;
        this.invocation_counter = invocation_counter;
    }

    public String getNoMeter() {
        return noMeter;
    }

    public void setNoMeter(String noMeter) {
        this.noMeter = noMeter;
    }

    public boolean isUseLogicalNameReferencing() {
        return useLogicalNameReferencing;
    }

    public void setUseLogicalNameReferencing(boolean useLogicalNameReferencing) {
        this.useLogicalNameReferencing = useLogicalNameReferencing;
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public InterfaceType getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(InterfaceType interfaceType) {
        this.interfaceType = interfaceType;
    }

    public int getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(int clientAddress) {
        this.clientAddress = clientAddress;
    }

    public int getLogicalAddress() {
        return logicalAddress;
    }

    public void setLogicalAddress(int logicalAddress) {
        this.logicalAddress = logicalAddress;
    }

    public int getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(int physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public int getAddressSize() {
        return addressSize;
    }

    public void setAddressSize(int addressSize) {
        this.addressSize = addressSize;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public String getSystemTitle() {
        return systemTitle;
    }

    public void setSystemTitle(String systemTitle) {
        this.systemTitle = systemTitle;
    }

    public String getBlockCipherKey() {
        return blockCipherKey;
    }

    public void setBlockCipherKey(String blockCipherKey) {
        this.blockCipherKey = blockCipherKey;
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public int getInvocation_counter() {
        return invocation_counter;
    }

    public void setInvocation_counter(int invocation_counter) {
        this.invocation_counter = invocation_counter;
    }

    /**
     * Get server address meter. Nilai merupakan kombinasi dari logical address
     * di high byte dan physical address di low bytenya.
     *
     * @return Server address dalam bentuk bilangan cacah (Integer)
     * @see GXDLMSSecureClient
     */
    public int getServerAddress() {
        if (this.addressSize == 0) {
            return GXDLMSSecureClient.getServerAddress(this.logicalAddress, this.physicalAddress);
        } else {
            return GXDLMSSecureClient.getServerAddress(this.logicalAddress,
                    this.physicalAddress, this.addressSize);
        }
    }

    @Override
    public String toString() {
        String rslt = "==========================================================================="
                + "\n\t" + "Nomor meter = " + noMeter
                + "\n\t" + "Use logical name referencing = " + useLogicalNameReferencing
                + "\n\t" + "Authentication = " + authentication
                + "\n\t" + "Client address = " + clientAddress
                + "\n\t" + "Interface type = " + interfaceType
                + "\n\t" + "Physical address = " + physicalAddress;
        if (!interfaceType.equals(InterfaceType.WRAPPER)) {
            rslt += "\n\t" + "Logical address = " + logicalAddress;
        }
        rslt += "\n\t" + "Server address = " + getServerAddress();
        if (addressSize != 0) {
            rslt += "\n\t" + "Address size = " + addressSize + " bytes";
        }
        if (authentication.equals(Authentication.LOW)) {
            rslt += "\n\t" + "Password = " + password;
        }
        if (authentication.equals(Authentication.HIGH_GMAC)) {
            rslt += "\n\t" + "Security = " + security;
            rslt += "\n\t" + "System title = " + systemTitle;
            rslt += "\n\t" + "Block cipher key = " + blockCipherKey;
            rslt += "\n\t" + "Authentication key = " + authenticationKey;
            if (invocation_counter != 0) {
                rslt += "\n\t" + "Invocation counter = " + invocation_counter;
            }
        }
        rslt += "\n===========================================================================";
        return rslt;
    }

    /**
     * Get GXDLMSSecureClient. Object untuk pembentukan pesan komunikasi.
     *
     * @return GXDLMSSecureClient
     * @see GXDLMSSecureClient
     */
    public GXDLMSSecureClient getClient() {
        try {
            GXDLMSSecureClient client = new GXDLMSSecureClient(true);
            client.setUseLogicalNameReferencing(useLogicalNameReferencing);
            client.setAuthentication(authentication);
            client.setClientAddress(clientAddress);
            client.setInterfaceType(interfaceType);
            client.setServerAddress(getServerAddress());
            if (client.getAuthentication().equals(Authentication.HIGH_GMAC)) {
                client.getCiphering().setSecurity(security);
                client.getCiphering().setAuthenticationKey(authenticationKey.getBytes("ASCII"));
                client.getCiphering().setBlockCipherKey(blockCipherKey.getBytes("ASCII"));
                client.getCiphering().setSystemTitle(systemTitle.getBytes("ASCII"));
                if (invocation_counter != 0) {
                    client.getCiphering().setInvocationCounter(invocation_counter);
                }
            } else {
                client.setPassword(password);
            }
            GXDLMSGateway gateway = new GXDLMSGateway();
            gateway.setNetworkId((short) 0);
            gateway.setPhysicalDeviceAddress(noMeter.getBytes());
            client.setGateway(gateway);
            return client;
        } catch (UnsupportedEncodingException ex) {
            LOG.log(Level.SEVERE, ex.toString());
            return null;
        }
    }

    /**
     * Print meter info.
     */
    public void printMeterInfo() {
        LOG.log(Level.INFO, this.toString());
    }
}
