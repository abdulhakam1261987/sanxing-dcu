/*
 * 
 */
package id.co.tabs.hes.object;

import gurux.common.GXCommon;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Class untuk mendefinisikan object meter pada DCU meter archive
 *
 * @author Tab Solutions
 */
public class MeterArchiveObj {

    private final String meterSerialNumber;

    private final MeterType meterType;

    public static List<MeterArchiveObj> parseMeterList(Object value) {
        List<MeterArchiveObj> meterArchiveObjs = new ArrayList<>();
        if (value instanceof List) {
            for (Object tmp : (List<?>) value) {
                if (tmp instanceof byte[]) {
                    MeterArchiveObj mao = new MeterArchiveObj(new String((byte[]) tmp), null);
                    meterArchiveObjs.add(mao);
                } else {
                    if (tmp instanceof List) {
                        String meterSerialNumber = "";
                        int meterType = 1;
                        int meterCommType = 1;
                        int meterProtocolType = 1;
                        int meterPrepaidType = 1;
                        int meterWiringType = 1;
                        String password = "";
                        for (Object object : (List) tmp) {
                            if (object instanceof List) {
                                int index = 1;
                                for (Object object1 : (List) object) {
                                    switch (index) {
                                        case 1:
                                            meterType = Integer.parseInt(object1.toString());
                                            break;
                                        case 2:
                                            meterCommType = Integer.parseInt(object1.toString());
                                            break;
                                        case 3:
                                            meterProtocolType = Integer.parseInt(object1.toString());
                                            break;
                                        case 4:
                                            meterPrepaidType = Integer.parseInt(object1.toString());
                                            break;
                                        case 5:
                                            meterWiringType = Integer.parseInt(object1.toString());
                                            break;
                                        case 6:
                                            password = object1.toString();
                                            break;
                                    }
                                    index++;
                                }
                            } else if (object instanceof byte[]) {
                                meterSerialNumber = new String((byte[]) object);
                            }
                        }
                        MeterArchiveObj mao = new MeterArchiveObj(meterSerialNumber, meterType, meterCommType,
                                meterProtocolType, meterPrepaidType, meterWiringType, password);
                        meterArchiveObjs.add(mao);
                    } else {
                        MeterArchiveObj mao = new MeterArchiveObj(new String((byte[]) tmp), null);
                        meterArchiveObjs.add(mao);
                    }
                }
            }
            return meterArchiveObjs;
        } else {
            return meterArchiveObjs;
        }
    }

    public MeterArchiveObj(String meterSerialNumber, MeterType meterType) {
        this.meterSerialNumber = meterSerialNumber;
        this.meterType = meterType;
    }

    public MeterArchiveObj(String meterSerialNumber, int meterType, int meterCommType, int meterProtocolType,
            int meterPrepaidType, int meterWiringType) {
        this.meterSerialNumber = meterSerialNumber;
        MeterType mt = new MeterType(meterType, meterCommType, meterProtocolType, meterPrepaidType,
                meterWiringType);
        this.meterType = mt;
    }

    public MeterArchiveObj(String meterSerialNumber, int meterType, int meterCommType, int meterProtocolType,
            int meterPrepaidType, int meterWiringType, String password) {
        this.meterSerialNumber = meterSerialNumber;
        MeterType mt = new MeterType(meterType, meterCommType, meterProtocolType, meterPrepaidType,
                meterWiringType, password);
        this.meterType = mt;
    }

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public MeterType getMeterType() {
        return meterType;
    }

    @Override
    public String toString() {
        return meterSerialNumber + "{"
                + "\n\tMeter serial number = " + meterSerialNumber + ","
                + "\n\tMeter type = " + meterType.getMeterType() + " ("
                + (meterType.getMeterType() == 1 ? "Single phase"
                : (meterType.getMeterType() == 2 ? "Poly phase" : "Unknown")) + "),"
                + "\n\tModule type = " + meterType.getMeterCommType() + " ("
                + (meterType.getMeterCommType() == 1 ? "GPRS"
                : (meterType.getMeterCommType() == 2 ? "PLC"
                : (meterType.getMeterCommType() == 3 ? "RF"
                : (meterType.getMeterCommType() == 4 ? "485"
                : (meterType.getMeterCommType() == 5 ? "Ethernet"
                : (meterType.getMeterCommType() == 6 ? "RF/PLC" : "Unknown")))))) + "),"
                + "\n\tMeter protocol = " + meterType.getMeterProtocolType() + " ("
                + (meterType.getMeterProtocolType() == 1 ? "DLMS" : "Unknown") + "),"
                + "\n\tPrepaid type = " + meterType.getMeterPrepaidType() + " ("
                + (meterType.getMeterPrepaidType() == 0 ? "Charge energy"
                : (meterType.getMeterPrepaidType() == 4 ? "Charge money"
                : (meterType.getMeterPrepaidType() == 255 ? "No prepaid" : "Unknown"))) + "),"
                + "\n\tWiring type = " + meterType.getMeterWiringType() + " ("
                + (meterType.getMeterWiringType() == 1 ? "2 phase 2 wire"
                : (meterType.getMeterWiringType() == 2 ? "3 phase 3 wire"
                : (meterType.getMeterWiringType() == 3 ? "3 phase 4 wire" : "Unknown"))) + "),"
                + "\n\tPassword = "
                + (meterType.getPassword().equals("") || meterType.getPassword().equals(null)
                ? "NULL" : meterType.getPassword())
                + '}';
    }

}
