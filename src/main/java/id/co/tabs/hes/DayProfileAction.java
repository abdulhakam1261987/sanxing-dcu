package id.co.tabs.hes;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DayProfileAction {
//	@JsonProperty("START_TIME")
	private String START_TIME;
//	@JsonProperty("SCRIPT")
	private String SCRIPT;
//	@JsonProperty("SELECTOR")
	private String SELECTOR;

	public DayProfileAction(){}

	/**
	 * @return the sTART_TIME
	 */
	 @JsonGetter("START_TIME")
	public String getSTART_TIME() {
		return START_TIME;
	}
	/**
	 * @param sTART_TIME the sTART_TIME to set
	 */
	public void setSTART_TIME(String sTART_TIME) {
		START_TIME = sTART_TIME;
	}
	/**
	 * @return the sCRIPT
	 */
	@JsonGetter("SCRIPT")
	public String getSCRIPT() {
		return SCRIPT;
	}
	/**
	 * @param sCRIPT the sCRIPT to set
	 */
	public void setSCRIPT(String sCRIPT) {
		SCRIPT = sCRIPT;
	}
	/**
	 * @return the sELECTOR
	 */
	@JsonGetter("SELECTOR")
	public String getSELECTOR() {
		return SELECTOR;
	}
	/**
	 * @param sELECTOR the sELECTOR to set
	 */
	public void setSELECTOR(String sELECTOR) {
		SELECTOR = sELECTOR;
	}

}
