package id.co.tabs.hes;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import id.co.tabs.hes.MongoInstance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

public class MongoDB {

//    private static final Logger LOGGER = Logger.getLogger(MongoDB.class.getName());
//    private static MongoClient mongoClient;
    private static MongoInstance mg = MongoInstance.getGlobalsInstance();
    private static String Mongodb = mg.getDatabase();
    private static String usr = mg.getUser();
    private static String pwd = mg.getPassword();
    private static String addrs = mg.getUrl();
    private static Integer port = Integer.parseInt(mg.getPort());
//
//    private static MongoClient getConnection() {
//        if(mongoClient==null){
//            MongoCredential credential = MongoCredential.createCredential(usr, Mongodb, pwd.toCharArray());
//            mongoClient = new MongoClient(new ServerAddress(addrs, port), Arrays.asList(credential));
//        }
//        return mongoClient;
//    }
//
//    private static void log(final Level level, final String message) {
//        LOGGER.log(level, message);
//    }
//
//    public static HashMap<String, String> getDataObis(String obisParam) {
//
//        MongoClient mongoClient = getConnection();
//        DB db = mongoClient.getDB(Mongodb);
//        DBCollection collection = db.getCollection("DATA_OBIS");
//        DBCursor cursor = collection.find(new BasicDBObject(obisParam, new BasicDBObject("$ne", 0)));
//        HashMap<String, String> list = new HashMap<String, String>();
//        if (cursor.size() < 1) {
//            return null;
//        }
//        for (DBObject result : cursor) {
//            list.put(result.get("LOGICAL_NAME").toString(), result.get("NAMA_FIELD").toString());
//        }
//        return list;
//    }
//
//    public List<DBObject> getData(String collectionName, BasicDBObject query, String sort) throws Exception {
//        System.out.println("------------------------");
//        log(Level.INFO, "getData..." + collectionName);
//        MongoClient mongoClient = getConnection();
//        DB db = mongoClient.getDB(Mongodb);
//        DBCollection collection = db.getCollection(collectionName);
//        DBCursor cursor = null;
//        if (sort == null || sort == "") {
//            cursor = collection.find(query).sort(new BasicDBObject("_id", 1));
//        } else {
//            cursor = collection.find(query).sort(new BasicDBObject(sort, 1));
//        }
//        log(Level.INFO, "DBCursor..." + collectionName);
//        List<DBObject> fields = new ArrayList<DBObject>();
//        while (cursor.hasNext()) {
//            DBObject csr = cursor.next();
//            fields.add(csr);
//        }
//        // mongoClient.close();
//        return fields;
//    }
//
//    public static boolean setData(String collectionName, Document doc) throws Exception {
//        MongoClient mongoClient = getConnection();
//
//        // connect to database mongo
//        MongoDatabase database = mongoClient.getDatabase(Mongodb);
//
//        // connect table instant
//        MongoCollection<Document> collections = database.getCollection(collectionName);
//        // insert ke collection (table) instant
//        collections.insertOne(doc);
//        // mongoClient.close();
//        return true;
//    }
//
    public static boolean setMultiData(String collectionName, List<Document> docs) throws Exception {
        System.out.println("setMultiData " + collectionName + " : " + docs.size() + " ");
        MongoClient mongoClient = getConnection();

        // connect to database mongo
        MongoDatabase database = mongoClient.getDatabase(Mongodb);

        // connect table instant
        MongoCollection<Document> collections = database.getCollection(collectionName);
        // insert ke collection (table) instant
        collections.insertMany(docs);
        // mongoClient.close();
        return true;
    }
//
//    public static boolean updateOne(String collectionName, Bson filter, Bson query) throws Exception {
//        MongoClient mongoClient = getConnection();
//
//        // connect to database mongo
//        MongoDatabase database = mongoClient.getDatabase(Mongodb);
//
//        // connect table instant
//        MongoCollection<Document> collections = database.getCollection(collectionName);
//        // update one ke collection (table) instant
//        collections.updateOne(filter, query);
//        // mongoClient.close();
//        return true;
//    }
//
//    public static List<DBObject> getData(String collectionName, BasicDBObject query, String sort, DBObject projection)
//            throws Exception {
//        MongoClient mongoClient = getConnection();
//        DB db = mongoClient.getDB(Mongodb);
//        DBCollection collection = db.getCollection(collectionName);
//        DBObject Projection = new BasicDBObject("_id", 0);
//        if (projection != null) {
//            Projection = projection;
//        }
//        DBCursor cursor = null;
//        if (sort == null || sort == "") {
//            cursor = collection.find(query, Projection).sort(new BasicDBObject("_id", 1));
//        } else {
//            cursor = collection.find(query, Projection).sort(new BasicDBObject(sort, 1));
//        }
//
//        List<DBObject> fields = new ArrayList<DBObject>();
//        while (cursor.hasNext()) {
//            DBObject csr = cursor.next();
//            fields.add(csr);
//        }
//        // mongoClient.close();
//        return fields;
//    }
	
//	@Autowired
//	private static Environment env;
	
	private static MongoDB globalsInstance = new MongoDB();
	public static MongoDB getInstance() {
        return globalsInstance;
    }
	private static MongoClient mongoClient;

//	private static MongoDLMS mg = MongoDLMS.getGlobalsInstance();
//	private static final Logger log = LoggerFactory.getLogger(MongoDB.class);
//	public static String Mongodb = mg.getDatabase();
//	private static String usr = mg.getUser();
//	private static String pwd = mg.getPassword();
//	private static String addrs = mg.getUrl();
//	private static Integer port = Integer.parseInt(mg.getPort());

	/**
	 * @return the globalsInstance
	 */
	public static MongoDB getGlobalsInstance() {
		return globalsInstance;
	}
	/**
	 * @param globalsInstance the globalsInstance to set
	 */
	public static void setGlobalsInstance(MongoDB globalsInstance) {
		MongoDB.globalsInstance = globalsInstance;
	}
	
	public static MongoClient getConnection() {
		// System.out.println("DB : "+Mongodb);
		// System.out.println("UserName : "+usr);
		// System.out.println("Password : "+pwd);
		if(mongoClient==null){
			MongoCredential credential = MongoCredential.createCredential(usr, Mongodb, pwd.toCharArray());
			mongoClient = new MongoClient(new ServerAddress(addrs, port), Arrays.asList(credential));
		}
		return mongoClient;
		// MongoClient mongoClient = mongoClients;//new MongoClient(new
		// MongoClientURI(env.getProperty("spring.data.mongodb.uri")));
		// return mongoClient;
	}

	public static HashMap<String, String> getDataObis(String obisParam) {
//		System.out.println("getDataObis " + obisParam);
		MongoClient mongoClient = getConnection();
		DB db = mongoClient.getDB(Mongodb);
		DBCollection collection = db.getCollection("DATA_OBIS");
		DBCursor cursor = collection.find(new BasicDBObject(obisParam, new BasicDBObject("$ne", 0)));
		HashMap<String, String> list = new HashMap<String, String>();
		if (cursor.size() < 1) {
			return null;
		}
		for (DBObject result : cursor) {
			list.put(result.get("LOGICAL_NAME").toString(), result.get("NAMA_FIELD").toString());
		}
		return list;
	}

	public static List<DBObject> getData(String collectionName, BasicDBObject query, String sort) throws Exception {
		MongoClient mongoClient = getConnection();
		DB db = mongoClient.getDB(Mongodb);
		DBCollection collection = db.getCollection(collectionName);
		DBObject removeIdProjection = new BasicDBObject("_id", 0);
		removeIdProjection.put("TGL_MULAI", 0);
		removeIdProjection.put("TGL_SELESAI", 0);
		removeIdProjection.put("UID", 0);
		removeIdProjection.put("ACTION", 0);
		removeIdProjection.put("ACTION_ON_SERVER", 0);
		removeIdProjection.put("ACTION_STATUS", 0);
		removeIdProjection.put("AREA", 0);
		removeIdProjection.put("AUTHENTICATION_KEY", 0);
		removeIdProjection.put("AUTHENTICATION_LEVEL", 0);
		removeIdProjection.put("BLOCK_CIPHER_KEY", 0);
		removeIdProjection.put("CLIENT_ADDRESS", 0);
		removeIdProjection.put("CT_PRIMER", 0);
		removeIdProjection.put("CT_RASIO", 0);
		removeIdProjection.put("CT_SEKUNDER", 0);
		removeIdProjection.put("DAYA", 0);
		removeIdProjection.put("DETAIL_FEEDER_GD", 0);
		removeIdProjection.put("DETAIL_FEEDER_GI", 0);
		removeIdProjection.put("DETAIL_GD", 0);
		removeIdProjection.put("DETAIL_TRAFO_GD", 0);
		removeIdProjection.put("FUNGSI_DEVICE", 0);
		removeIdProjection.put("FUNGSI_GD", 0);
		removeIdProjection.put("FUNGSI_GI", 0);
		removeIdProjection.put("ID_GD", 0);
		removeIdProjection.put("ID_GI", 0);
		removeIdProjection.put("ID_JURUSAN", 0);
		removeIdProjection.put("ID_PELANGGAN", 0);
		removeIdProjection.put("ID_PEMILIK", 0);
		removeIdProjection.put("ID_PLN", 0);
		removeIdProjection.put("IP_ADDRESS", 0);
		removeIdProjection.put("JENIS", 0);
		removeIdProjection.put("JENIS_GARDU", 0);
		removeIdProjection.put("JENIS_MEDIA", 0);
		removeIdProjection.put("JENIS_TARIF", 0);
		removeIdProjection.put("KD_AREA", 0);
		removeIdProjection.put("KD_DAYA", 0);
		removeIdProjection.put("KD_GD", 0);
		removeIdProjection.put("KD_GI", 0);
		removeIdProjection.put("KD_PEMBANGKIT", 0);
		removeIdProjection.put("KD_PUSAT", 0);
		removeIdProjection.put("KD_UNIT_BISNIS", 0);
		removeIdProjection.put("LAST_ACTION", 0);
		removeIdProjection.put("LOGICAL_ADDRESS", 0);
		removeIdProjection.put("NAMA_GD", 0);
		removeIdProjection.put("NAMA_GI", 0);
		removeIdProjection.put("NAMA_JURUSAN", 0);
		removeIdProjection.put("NAMA_PELANGGAN", 0);
		removeIdProjection.put("NO_DEVICE", 0);
		removeIdProjection.put("NO_SELULER", 0);
		removeIdProjection.put("NO_SIMCARD", 0);
		removeIdProjection.put("PASSWORD_METER", 0);
		removeIdProjection.put("PEMBANGKIT", 0);
		removeIdProjection.put("PHASA", 0);
		removeIdProjection.put("PHYSICAL_ADDRESS", 0);
		removeIdProjection.put("PORT", 0);
		removeIdProjection.put("PUSAT", 0);
		removeIdProjection.put("SECURITY", 0);
		removeIdProjection.put("STATUS_METER", 0);
		removeIdProjection.put("SYSTEM_TITLE", 0);
		removeIdProjection.put("TYPE_DEVICE", 0);
		removeIdProjection.put("UNIT_BISNIS", 0);
		removeIdProjection.put("VT_PRIMER", 0);
		removeIdProjection.put("VT_RASIO", 0);
		removeIdProjection.put("VT_SEKUNDER", 0);
		removeIdProjection.put("TGL_BACA_UTC", 0);
		removeIdProjection.put("DATA_METER", 0);
		
		DBCursor cursor = null;
		if (sort == null || sort == "") {
			cursor = collection.find(query, removeIdProjection).sort(new BasicDBObject("_id", 1));
		} else {
			cursor = collection.find(query, removeIdProjection).sort(new BasicDBObject(sort, 1));
		}

		List<DBObject> fields = new ArrayList<DBObject>();
		while (cursor.hasNext()) {
			DBObject csr = cursor.next();
			fields.add(csr);
		}
//		mongoClient.close();
		return fields;
	}

	public static List<DBObject> getDataPenarikan(String collectionName, BasicDBObject query, String sort)
			throws Exception {
		MongoClient mongoClient = getConnection();
		DB db = mongoClient.getDB(Mongodb);
		DBCollection collection = db.getCollection(collectionName);
		DBObject removeIdProjection = new BasicDBObject("_id", 0);
		// DBCursor cursor = null;
		DBObject sortDates;
		if (sort == null || sort == "") {
			sortDates = new BasicDBObject("$sort", new BasicDBObject("_id", -1));
		} else {
			sortDates = new BasicDBObject("$sort", new BasicDBObject(sort, 1));
		}
		BasicDBObject match = new BasicDBObject("$match", query);
		List<DBObject> stages = new ArrayList<DBObject>();
		stages.add(match);
		DBObject milisec = new BasicDBObject("$subtract", new Object[] { "$TGL_SELESAI", "$TGL_MULAI" });

		DBObject toSecond = new BasicDBObject("$divide", new Object[] { milisec, 1000 });

		DBObject roundUp = new BasicDBObject("$ceil", toSecond);

		DBObject value = new BasicDBObject("$addFields", new BasicDBObject("WAKTU_PENARIKAN", roundUp));

		DBObject project = new BasicDBObject("$project", removeIdProjection);
		stages.add(value);
		stages.add(sortDates);
		stages.add(project);
		AggregationOutput output = collection.aggregate(stages);

		List<DBObject> fields = new ArrayList<DBObject>();

		for (DBObject result : output.results()) {
			fields.add(result);
		}
//		mongoClient.close();
		return fields;
	}

//	public static List<DBObject> getDataProfile(String collectionName, BasicDBObject query, String sort)
//			throws Exception {
//		MongoClient mongoClient = getConnection();
//		DB db = mongoClient.getDB(Mongodb);
//		DBCollection collection = db.getCollection(collectionName);
//		DBObject removeIdProjection = new BasicDBObject("_id", 0);
//		// removeIdProjection.put("TGL_MULAI", 0);
//		// removeIdProjection.put("TGL_SELESAI", 0);
//		removeIdProjection.put("UID", 0);
//		removeIdProjection.put("MERK_METER", 0);
//		removeIdProjection.put("TYPE_METER", 0);
//		removeIdProjection.put("KD_PUSAT", 0);
//		removeIdProjection.put("KD_UNIT_BISNIS", 0);
//		removeIdProjection.put("KD_PEMBANGKIT", 0);
//		removeIdProjection.put("KD_AREA", 0);
//		removeIdProjection.put("JENIS", 0);
//		removeIdProjection.put("OBIS", 0);
//		removeIdProjection.put("TGL_BACA_UTC", 0);
//		// removeIdProjection.put("TGL_SELESAI", 0);
//		DBCursor cursor = null;
//		if (sort == null || sort == "") {
//			cursor = collection.find(query, removeIdProjection).sort(new BasicDBObject("_id", 1));
//		} else {
//			cursor = collection.find(query, removeIdProjection).sort(new BasicDBObject(sort, -1));
//		}
//
//		List<DBObject> fields = new ArrayList<DBObject>();
//		while (cursor.hasNext()) {
//			DBObject csr = cursor.next();
//			fields.add(csr);
//		}
//		mongoClient.close();
//		return fields;
//	}
	
	public static List<DBObject> getDataProfile(String collectionName, BasicDBObject query, String sort) throws Exception {
		MongoClient mongoClient = MongoDB.getConnection();
		DB db = mongoClient.getDB(MongoDB.Mongodb);
		DBCollection collection = db.getCollection(collectionName);
		DBObject removeIdProjection = new BasicDBObject("_id", 0);
		removeIdProjection.put("TGL_MULAI", 0);
		removeIdProjection.put("TGL_SELESAI", 0);
		removeIdProjection.put("UID", 0);
		removeIdProjection.put("ACTION", 0);
		removeIdProjection.put("ACTION_ON_SERVER", 0);
		removeIdProjection.put("ACTION_STATUS", 0);
		removeIdProjection.put("AREA", 0);
		removeIdProjection.put("AUTHENTICATION_KEY", 0);
		removeIdProjection.put("AUTHENTICATION_LEVEL", 0);
		removeIdProjection.put("BLOCK_CIPHER_KEY", 0);
		removeIdProjection.put("CLIENT_ADDRESS", 0);
		removeIdProjection.put("CT_PRIMER", 0);
		removeIdProjection.put("CT_RASIO", 0);
		removeIdProjection.put("CT_SEKUNDER", 0);
		removeIdProjection.put("DAYA", 0);
		removeIdProjection.put("DETAIL_FEEDER_GD", 0);
		removeIdProjection.put("DETAIL_FEEDER_GI", 0);
		removeIdProjection.put("DETAIL_GD", 0);
		removeIdProjection.put("DETAIL_TRAFO_GD", 0);
		removeIdProjection.put("FUNGSI_DEVICE", 0);
		removeIdProjection.put("FUNGSI_GD", 0);
		removeIdProjection.put("FUNGSI_GI", 0);
		removeIdProjection.put("ID_GD", 0);
		removeIdProjection.put("ID_GI", 0);
		removeIdProjection.put("ID_JURUSAN", 0);
		removeIdProjection.put("ID_PELANGGAN", 0);
		removeIdProjection.put("ID_PEMILIK", 0);
		removeIdProjection.put("ID_PLN", 0);
		removeIdProjection.put("IP_ADDRESS", 0);
		removeIdProjection.put("JENIS", 0);
		removeIdProjection.put("JENIS_GARDU", 0);
		removeIdProjection.put("JENIS_MEDIA", 0);
		removeIdProjection.put("JENIS_TARIF", 0);
		removeIdProjection.put("KD_AREA", 0);
		removeIdProjection.put("KD_DAYA", 0);
		removeIdProjection.put("KD_GD", 0);
		removeIdProjection.put("KD_GI", 0);
		removeIdProjection.put("KD_PEMBANGKIT", 0);
		removeIdProjection.put("KD_PUSAT", 0);
		removeIdProjection.put("KD_UNIT_BISNIS", 0);
		removeIdProjection.put("LAST_ACTION", 0);
		removeIdProjection.put("LOGICAL_ADDRESS", 0);
		removeIdProjection.put("MERK_METER", 0);
		removeIdProjection.put("NAMA_GD", 0);
		removeIdProjection.put("NAMA_GI", 0);
		removeIdProjection.put("NAMA_JURUSAN", 0);
		removeIdProjection.put("NAMA_PELANGGAN", 0);
		removeIdProjection.put("NO_DEVICE", 0);
		removeIdProjection.put("NO_METER", 0);
		removeIdProjection.put("NO_SELULER", 0);
		removeIdProjection.put("NO_SIMCARD", 0);
		removeIdProjection.put("PASSWORD_METER", 0);
		removeIdProjection.put("PEMBANGKIT", 0);
		removeIdProjection.put("PHASA", 0);
		removeIdProjection.put("PHYSICAL_ADDRESS", 0);
		removeIdProjection.put("PORT", 0);
		removeIdProjection.put("PUSAT", 0);
		removeIdProjection.put("SECURITY", 0);
		removeIdProjection.put("STATUS_METER", 0);
		removeIdProjection.put("SYSTEM_TITLE", 0);
		removeIdProjection.put("TIPE_METER", 0);
		removeIdProjection.put("TYPE_DEVICE", 0);
		removeIdProjection.put("UNIT_BISNIS", 0);
		removeIdProjection.put("VT_PRIMER", 0);
		removeIdProjection.put("VT_RASIO", 0);
		removeIdProjection.put("VT_SEKUNDER", 0);
		removeIdProjection.put("TGL_BACA_UTC", 0);
		removeIdProjection.put("DATA_METER", 0);
//		removeIdProjection.put("TGL_SELESAI", 0);
		
		BasicDBObject match = new BasicDBObject(
			    "$match", query
			);
		
		DBCursor cursor = null;
		DBObject sortDates;
		if(sort==null || sort==""){
			sortDates = new BasicDBObject("$sort", new BasicDBObject("_id", -1));
		}else{
			sortDates = new BasicDBObject("$sort", new BasicDBObject(sort, -1));
		}
		
		List<DBObject> stages = new ArrayList<DBObject>();
		stages.add(match);
//		DBObject milisec = new BasicDBObject("$subtract", 
//		                                     new Object[] {"$TGL_SELESAI", "$TGL_MULAI"});
		
//		DBObject toSecond = new BasicDBObject("$divide", 
//                new Object[] {milisec, 1000});
		
//		DBObject roundUp = new BasicDBObject("$ceil", 
//				toSecond);
		
//		DBObject value = new BasicDBObject("$addFields", 
//				new BasicDBObject("WAKTU_PENARIKAN", roundUp));

		DBObject project = new BasicDBObject("$project", removeIdProjection);
//		stages.add(value);
		stages.add(sortDates);
		stages.add(project);
		AggregationOutput output = collection.aggregate(stages);

		List<DBObject> fields = new ArrayList<DBObject>() ;

		for (DBObject result : output.results()) {
			fields.add(result);
		}
		
//		mongoClient.close();
		return fields;
	}
	
	public static List<DBObject> getDataRemvProjection(String collectionName, BasicDBObject query, String sort, DBObject removeIdProjection) throws Exception {
		MongoClient mongoClient = MongoDB.getConnection();
		DB db = mongoClient.getDB(MongoDB.Mongodb);
		DBCollection collection = db.getCollection(collectionName);
		
		BasicDBObject match = new BasicDBObject(
			    "$match", query
			);
		
		DBCursor cursor = null;
		DBObject sortDates;
		if(sort==null || sort==""){
			sortDates = new BasicDBObject("$sort", new BasicDBObject("_id", -1));
		}else{
			sortDates = new BasicDBObject("$sort", new BasicDBObject(sort, -1));
		}
		
		List<DBObject> stages = new ArrayList<DBObject>();
		stages.add(match);
//		DBObject milisec = new BasicDBObject("$subtract", 
//		                                     new Object[] {"$TGL_SELESAI", "$TGL_MULAI"});
		
//		DBObject toSecond = new BasicDBObject("$divide", 
//                new Object[] {milisec, 1000});
		
//		DBObject roundUp = new BasicDBObject("$ceil", 
//				toSecond);
		
//		DBObject value = new BasicDBObject("$addFields", 
//				new BasicDBObject("WAKTU_PENARIKAN", roundUp));

		DBObject project = new BasicDBObject("$project", removeIdProjection);
//		stages.add(value);
		stages.add(sortDates);
		stages.add(project);
		AggregationOutput output = collection.aggregate(stages);

		List<DBObject> fields = new ArrayList<DBObject>() ;

		for (DBObject result : output.results()) {
			fields.add(result);
		}
		
//		mongoClient.close();
		return fields;
	}

	public static List<DBObject> getData(String collectionName, BasicDBObject query, String sort, DBObject projection)
			throws Exception {
		MongoClient mongoClient = getConnection();
		DB db = mongoClient.getDB(Mongodb);
		DBCollection collection = db.getCollection(collectionName);
		DBObject Projection = new BasicDBObject("_id", 0);
		if (projection != null) {
			Projection = projection;
		}
		DBCursor cursor = null;
		if (sort == null || sort == "") {
			cursor = collection.find(query, Projection).sort(new BasicDBObject("_id", 1));
		} else {
			cursor = collection.find(query, Projection).sort(new BasicDBObject(sort, 1));
		}

		List<DBObject> fields = new ArrayList<DBObject>();
		while (cursor.hasNext()) {
			DBObject csr = cursor.next();
			fields.add(csr);
		}
//		mongoClient.close();
		return fields;
	}

	public static boolean setData(String collectionName, Document doc) throws Exception {
		MongoClient mongoClient = getConnection();

		// connect to database mongo
		MongoDatabase database = mongoClient.getDatabase(Mongodb);

		// connect table instant
		MongoCollection<Document> collections = database.getCollection(collectionName);
		// insert ke collection (table) instant
		collections.insertOne(doc);
//		mongoClient.close();
		return true;
	}

	public static boolean updateOne(String collectionName, Bson filter, Bson query) throws Exception {
		MongoClient mongoClient = getConnection();

		// connect to database mongo
		MongoDatabase database = mongoClient.getDatabase(Mongodb);

		// connect table instant
		MongoCollection<Document> collections = database.getCollection(collectionName);
		// update one ke collection (table) instant
		collections.updateOne(filter, query);
//		mongoClient.close();
		return true;
	}

	public static boolean deleteOne(String collectionName, Bson filter) throws Exception {
		MongoClient mongoClient = getConnection();

		// connect to database mongo
		MongoDatabase database = mongoClient.getDatabase(Mongodb);

		// connect table instant
		MongoCollection<Document> collections = database.getCollection(collectionName);
		// update one ke collection (table) instant
		collections.deleteOne(filter);
//		mongoClient.close();
		return true;
	}

	public static List<DBObject> getLamaWaktu(String collectionName, BasicDBObject query, String sort)
			throws Exception {
		MongoClient mongoClient = MongoDB.getConnection();
		DB db = mongoClient.getDB(MongoDB.Mongodb);
		DBCollection collection = db.getCollection(collectionName);

		DBObject milisec = new BasicDBObject("$subtract", new Object[] { "$TGL_SELESAI", "$TGL_MULAI" });

		DBObject toSecond = new BasicDBObject("$divide", new Object[] { milisec, 1000 });

		DBObject roundUp = new BasicDBObject("$ceil", toSecond);

		DBObject value = new BasicDBObject("$addFields", new BasicDBObject("WAKTU_PENARIKAN", roundUp));

		List<DBObject> pipeline = new ArrayList<DBObject>(Arrays.asList(new BasicDBObject("$match", query),
				new BasicDBObject("$group",
						new BasicDBObject("_id", "$UID").append("JUMLAH_DATA", new BasicDBObject("$sum", 1))
								.append("TGL_MULAI", new BasicDBObject("$min", "$TGL_MULAI")).append("TGL_SELESAI",
										new BasicDBObject("$max", "$TGL_SELESAI"))),
				new BasicDBObject("$addFields", new BasicDBObject("WAKTU", roundUp))));

		AggregationOutput output = collection.aggregate(pipeline);

		List<DBObject> fields = new ArrayList<DBObject>();

		for (DBObject result : output.results()) {
			fields.add(result);
		}

//		mongoClient.close();
		return fields;
	}
}
