/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.tabs.hes.dlms;

/**
 *
 * @author Rudiman
 */

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

public final class Aes128Ecb {
    private static final String ALGORITM = "AES/ECB/NoPadding";
    SecretKeySpec skeySpec;
         
    public Aes128Ecb( String sKey ) {
        byte[] keyValue = hexStringToByteArray( sKey );
        skeySpec = new SecretKeySpec( keyValue, "AES" );
    }
         
    public byte[] encrypt( byte[] data ) throws Exception {
        Cipher cipher = Cipher.getInstance( ALGORITM );
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        return cipher.doFinal(data);
    }
    
    public byte[] decrypt( byte[] data ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance( ALGORITM );
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        return cipher.doFinal(data);  
    }
    
    public byte[] hexStringToByteArray( String in ) {
        int len = in.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(in.charAt(i), 16) << 4) + Character.digit(in.charAt(i+1), 16));
        }
        return data;
    }    
    
    public String byteArrayToHexString( byte[] in ) {
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
}