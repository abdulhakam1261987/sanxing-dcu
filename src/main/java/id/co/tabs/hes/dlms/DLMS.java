package id.co.tabs.hes.dlms;

import java.io.File;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;
import org.json.simple.JSONObject;

import com.mongodb.BasicDBObject;

import gurux.common.GXCommon;
import gurux.common.enums.TraceLevel;
import gurux.dlms.GXBitString;
import gurux.dlms.GXByteBuffer;
import gurux.dlms.GXDLMSConverter;
import gurux.dlms.GXDLMSException;
import gurux.dlms.GXDLMSGateway;
import gurux.dlms.GXDateTime;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.ClockStatus;
import gurux.dlms.enums.Command;
import gurux.dlms.enums.Conformance;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.DateTimeExtraInfo;
import gurux.dlms.enums.DateTimeSkips;
import gurux.dlms.enums.ErrorCode;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Unit;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSObjectCollection;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSScript;
import gurux.dlms.objects.GXDLMSScriptAction;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSeasonProfile;
import gurux.dlms.objects.GXDLMSWeekProfile;
import gurux.dlms.objects.GXXmlWriterSettings;
import gurux.dlms.objects.IGXDLMSBase;
import gurux.dlms.secure.GXDLMSSecureClient;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.ResultRead;
import id.co.tabs.hes.Utils;
import id.co.tabs.hes.dcu.reader.SkaladanUnit;
import id.co.tabs.hes.obis.descriptor.ObisDescriptor;
import id.co.tabs.hes.obis.descriptor.PhysicalUnit;
import id.co.tabs.hes.object.Meter;
import id.co.tabs.hes.object.MeterArchiveObj;
import java.util.Calendar;
import java.util.Map;

public class DLMS {

    private static final Logger LOGGER = Logger.getLogger(DLMS.class.getName());
    private String logPrefix;
    TraceLevel Trace;
    private final IDLMSMedia media;
    private GXDLMSSecureClient client = new GXDLMSSecureClient(true, 1, 1, Authentication.LOW, "Aa12345.",
            InterfaceType.WRAPPER);
    private Object dataMeter;

    public DLMS(IDLMSMedia media) {
        this.media = media;
//        this.client.setAuthentication(Authentication.HIGH_GMAC);
//        this.client.setClientAddress(1);
//        this.client.setServerAddress(GXDLMSSecureClient.getServerAddress(0, 1));
//        this.client.setInterfaceType(InterfaceType.WRAPPER);
//        this.client.getCiphering().setSecurity(Security.AUTHENTICATION_ENCRYPTION);
//        try {
//            this.client.getCiphering().setAuthenticationKey("0000000000000000".getBytes("ASCII"));
//            this.client.getCiphering().setBlockCipherKey("0000000000000000".getBytes("ASCII"));
//            this.client.getCiphering().setSystemTitle("00000001".getBytes("ASCII"));
//            this.client.getCiphering().setInvocationCounter(0);
//        } catch (Exception exception) {
//            LOGGER.log(Level.INFO, "Setting client error : " + exception.toString());
//        }
        this.client.setAutoIncreaseInvokeID(true);
    }

    public void setClient() {
        GXDLMSSecureClient clientTmp = new GXDLMSSecureClient(true, 1, 1, Authentication.LOW, "Aa12345.",
                InterfaceType.WRAPPER);
        this.client = clientTmp;
    }

    public void setClient(Meter meter) {
        this.client = meter.getClient();
    }

    public GXDLMSSecureClient getClient() {
        return this.client;
    }

    public void setLogPrefix(String logPrefix) {
        this.logPrefix = logPrefix;
    }

    private void log(final Level level, final String message) {
        LOGGER.log(level, logPrefix + " " + message);
    }

    public void readDLMSPacket(final byte[][] data) throws Exception {
        final GXReplyData reply = new GXReplyData();
        for (final byte[] it : data) {
            reply.clear();
            readDLMSPacket(it, reply);
        }
    }
    
    public void readDLMSPacketMeter(final byte[][] data, String noMeter) throws Exception {
        final GXReplyData reply = new GXReplyData();
        for (final byte[] it : data) {
            reply.clear();
            readDLMSPacket(addMeterFrame(noMeter, it), reply);
        }
    }

    /**
     * Read DLMS Data from the device. If access is denied return null.
     */
    private void readDLMSPacket(final byte[] data, final GXReplyData reply) throws Exception {
        if (!reply.getStreaming() && (data == null || data.length == 0)) {
            return;
        }
        final GXReplyData notify = new GXReplyData();
        reply.setError((short) 0);
        Integer pos = 0;
        boolean succeeded = false;

        GXByteBuffer rd = new GXByteBuffer();
        while (!succeeded) {
            if (!reply.isStreaming()) {
                media.send(data);
            }

            succeeded = media.receive();
            if (!succeeded) {
                // Try to read again...
                if (pos++ == 3) {
                    throw new RuntimeException("Failed to receive reply from the device in given time.");
                }
                log(Level.WARNING, "Data send failed. Try to resend " + pos.toString() + "/3");
            }
        }
        rd = new GXByteBuffer(media.getReceivedData());
        int msgPos = 0;
        // Loop until whole DLMS packet is received.
        try {
            while (!client.getData(rd, reply, notify)) {
                if (notify.getData().getData() != null) {
                    // Handle notify.
                    if (!notify.isMoreData()) {
                        handleNotifyMessages(notify);
                        notify.clear();
                        msgPos = rd.position();
                    }
                    continue;
                }

                while (!media.receive()) {
                    // If echo.
                    if (reply.isEcho()) {
                        media.send(data);
                    }
                    // Try to read again...
                    if (++pos == 3) {
                        throw new Exception("Failed to receive reply from the device in given time.");
                    }
                    log(Level.WARNING, "Data send failed. Try to resend " + pos.toString() + "/3");
                }
                rd.position(msgPos);
                rd.set(media.getReceivedData());
            }
        } catch (final Exception e) {
            log(Level.SEVERE, "RX: \t" + rd.toString());
            throw e;
        }

        log(Level.FINE, "RX: \t" + rd.toString());
        if (reply.getError() != 0) {
            if (reply.getError() == ErrorCode.REJECTED.getValue()) {
                Thread.sleep(1000);
                readDLMSPacket(data, reply);
            } else {
                throw new GXDLMSException(reply.getError());
            }
        }
    }
    
     /**
     * Read DLMS Data from the device. If access is denied return null.
     */
    private void readDLMSPacketMeter(final byte[] data, final GXReplyData reply, String noMeter) throws Exception {
        readDLMSPacket(addMeterFrame(noMeter, data), reply);
    }

    /**
     * Handle received notify messages.
     *
     * @param reply Received data.
     * @throws Exception
     */
    public void handleNotifyMessages(final GXReplyData reply) throws Exception {
        final List<Entry<GXDLMSObject, Integer>> items = new ArrayList<Entry<GXDLMSObject, Integer>>();
        final Object value = client.parseReport(reply, items);
        // If Event notification or Information report.
        if (value == null) {
            for (final Entry<GXDLMSObject, Integer> it : items) {
                if (reply.getCommand() == Command.EVENT_NOTIFICATION) {
                    log(Level.INFO, "EVENT NOTIFICATION " + it.getKey().toString() + " Value:"
                            + it.getKey().getValues()[it.getValue() - 1]);
                } else if (reply.getCommand() == Command.INFORMATION_REPORT) {
                    log(Level.INFO, "REPORT NOTIFICATION " + it.getKey().toString() + " Value:"
                            + it.getKey().getValues()[it.getValue() - 1]);
                }
            }
        } else // Show data notification.
        {
            if (value instanceof List<?>) {
                for (final Object it : (List<?>) value) {
                    log(Level.INFO, "DATA NOTIFICATION " + "Value:" + String.valueOf(it));
                }
            } else {
                log(Level.INFO, "DATA NOTIFICATION " + "Value:" + String.valueOf(value));
            }
        }
        reply.clear();
    }

    public void readDataBlock(final byte[][] data, final GXReplyData reply) throws Exception {
        if (data != null) {
            for (final byte[] it : data) {
                reply.clear();
                readDataBlock(it, reply);
            }
        }
    }

    /**
     * Reads next data block.
     *
     * @param data
     * @return
     * @throws Exception
     */
    private void readDataBlock(byte[] data, final GXReplyData reply) throws Exception {
        if (data != null && data.length != 0) {
            readDLMSPacket(data, reply);
            while (reply.isMoreData()) {
                if (reply.isStreaming()) {
                    data = null;
                } else {
                    data = client.receiverReady(reply);
                }
                readDLMSPacket(data, reply);
            }
        }
    }

    public GXDLMSObject getObject(String ln) {
        return client.getObjects().findByLN(ObjectType.NONE, ln);
    }

    /**
     * Initializes connection.
     *
     * @param port
     * @throws InterruptedException
     * @throws Exception
     */
    public void initializeConnection() throws Exception {
        Integer tries = 0;
        final GXReplyData reply = new GXReplyData();
        printClientInfo();
        try {
            if (tries > 0) {
                log(Level.WARNING, "Connecting attempt #" + tries.toString());
            }
            reply.clear();
            log(Level.INFO, "Sending AARQ...");
            readDataBlock(client.aarqRequest(), reply);
            log(Level.INFO, "Parsing AARE...");
            client.parseAareResponse(reply.getData());
            reply.clear();
            if (client.getIsAuthenticationRequired()) {
                log(Level.INFO, "Get Application Association Request...");
                readDataBlock(client.getApplicationAssociationRequest(), reply);
                client.parseApplicationAssociationResponse(reply.getData());
            }

            // LOGGER.info("*AuthenticationRequired: " + client.getIsAuthenticationRequired());
            // if (client.getAuthentication().getValue() > Authentication.LOW.getValue()) {
            //     for (byte[] it : client.getApplicationAssociationRequest()) {
            //         readDLMSPacket(it, reply);
            //     }
            //     client.parseApplicationAssociationResponse(reply.getData());
            // }
            // break;
        } catch (Exception ex) {
            // if (tries == 3) {
            throw ex;
            // } else {
            //     client.disconnectRequest(true);
            //     log(Level.WARNING, ex.getMessage());
            //     tries = tries + 1;
            //     log(Level.INFO, "Delaying next attempt...");
            //     Thread.sleep(tries * 10000);
            // }
        }
        // }
    }

    public void initializeMeterConnection(String noMeter) throws Exception {
        GXDLMSGateway meterGateway = client.getGateway();
        Integer tries = 0;
        final GXReplyData reply = new GXReplyData();
        printClientInfo();
        java.nio.ByteBuffer replyBuff;
        if (client.getInterfaceType() == InterfaceType.WRAPPER) {
            replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
        } else {
            replyBuff = java.nio.ByteBuffer.allocate(100);
        }
        try {
            if (tries > 0) {
                log(Level.WARNING, "Connecting attempt #" + tries.toString());
            }
            byte[] data = client.snrmRequest();
            if (data.length != 0) {
                log(Level.INFO, "Sending SNRM");
                readDLMSPacket(data, reply);
                client.parseUAResponse(reply.getData());
                int size = (int) ((((Number) client.getLimits().getMaxInfoTX())
                        .intValue() & 0xFFFFFFFFL) + 40);
                replyBuff = java.nio.ByteBuffer.allocate(size);
            }
            reply.clear();
            log(Level.INFO, "Sending AARQ...");
            readDataBlock(client.aarqRequest(), reply);
            log(Level.INFO, "Parsing AARE...");
            client.parseAareResponse(reply.getData());
            reply.clear();
            if (client.getAuthentication().getValue() > Authentication.LOW
                    .getValue()) {
                log(Level.INFO, "Get Application Association Request...");
                client.setGateway(null);
                for (byte[] it : client.getApplicationAssociationRequest()) {
                    readDLMSPacket(addMeterFrame(noMeter, it), reply);
                }
                client.parseApplicationAssociationResponse(reply.getData());
                client.setGateway(meterGateway);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    byte[] addMeterFrame(String noMeter, byte[] it) {
        byte[] bs = new byte[noMeter.getBytes().length + 3];
        bs[0] = (byte) 0xE6;
        bs[1] = (byte) 0x00;
        bs[2] = (byte) noMeter.getBytes().length;
        for (int i = 3; i < bs.length; i++) {
            bs[i] = noMeter.getBytes()[i - 3];
        }
        byte[] message = new byte[it.length + bs.length];
        for (int i = 0; i < 7; i++) {
            message[i] = it[i];
        }
        message[7] = (byte) (it[7] + bs.length);
        for (int i = 8; i < bs.length + 8; i++) {
            message[i] = bs[i - 8];
        }
        int indexIt = 8;
        for (int i = bs.length + 8; i < (bs.length + 8) + (it.length - 8); i++) {
            message[i] = it[indexIt];
            indexIt++;
        }
        return message;
    }

    /**
     * Writes value to DLMS object with selected attribute index.
     *
     * @param item
     * @param attributeIndex
     * @throws Exception
     */
    public void writeMeter(final GXDLMSObject item, final int attributeIndex, String noMeter) throws Exception {
        GXDLMSGateway meterGateway = client.getGateway();
        client.setGateway(null);
        final byte[][] data = client.write(item, attributeIndex);
        byte[][] bses = data;
        for (int i = 0; i < data.length; i++) {
            bses[i] = addMeterFrame(noMeter, data[i]);
        }
        readDLMSPacket(bses);
        client.setGateway(meterGateway);
    }

    /*--
        Hexing send List Meter sebelum membaca meter
    --*/
    public void sendListMeter() {
        try {
            final GXReplyData reply = new GXReplyData();
            String sKey = "00000000000000000000000000000000";
            Aes128Ecb gen = new Aes128Ecb(sKey);
            String listMeter = "0001000100010059C101C100010001600182FF02000101020C0A000A08323032343736383611001601112B110111101101092000000000000000000000000000000000000000000000000000000000000000001101090830303030303030300300";
            byte[] listMeterByte = gen.hexStringToByteArray(listMeter);
            // media.send(listMeterByte);
            readDataBlock(listMeterByte, reply);
        } catch (Exception e) {
        }
    }

    public void sendMeter() throws Exception {
        final GXReplyData reply = new GXReplyData();
        String sKey = "00000000000000000000000000000000";
        Aes128Ecb gen = new Aes128Ecb(sKey);
        String listMeter = "0001000100010018E600083230323437363836C001C100010000600100FF0200";
        byte[] listMeterByte = gen.hexStringToByteArray(listMeter);
        // media.send(listMeterByte);
        readDataBlock(listMeterByte, reply);
    }

    public void getAssociationView(Boolean useCache, String cacheName) throws Exception {
        final GXDLMSObjectCollection objects;
        String fileName = "./cache/" + cacheName;

        if (useCache && (new File(fileName).exists())) {
            log(Level.INFO, "Get Association View (cached)...");
            objects = GXDLMSObjectCollection.load(fileName);
            client.getObjects().clear();
            client.getObjects().addAll(objects);
        } else {
            final GXReplyData reply = new GXReplyData();
            log(Level.INFO, "Get Association View...");
            readDataBlock(client.getObjectsRequest(), reply);
            objects = client.parseObjects(reply.getData(), false);
            GXXmlWriterSettings settings = new GXXmlWriterSettings();
            settings.setValues(false);
            settings.setOld(false);
            objects.save(fileName, settings);
        }

        // Get description of the objects.
        final GXDLMSConverter converter = new GXDLMSConverter();
        converter.updateOBISCodeInformation(objects);
    }

    public void getAssociationViewNoCache() throws Exception {
        final GXDLMSObjectCollection objects;
        final GXReplyData reply = new GXReplyData();
        log(Level.INFO, "Get Association View...");
        readDataBlock(client.getObjectsRequest(), reply);
        objects = client.parseObjects(reply.getData(), false);
        GXXmlWriterSettings settings = new GXXmlWriterSettings();
        settings.setValues(false);
        settings.setOld(false);
        // Get description of the objects.
        final GXDLMSConverter converter = new GXDLMSConverter();
        converter.updateOBISCodeInformation(objects);
        log(Level.INFO, "Logical name\tDeskripsi\tObject type");
        log(Level.INFO, "------------------------------------------------------------------");
        for (GXDLMSObject object : objects) {
            log(Level.INFO, object.getLogicalName() + "\t" + object.getDescription() + "\t"
                    + object.getObjectType());
        }
    }

    public void dumpObjects() {
        log(Level.INFO, "Association View Dump:");
        for (final GXDLMSObject o : client.getObjects()) {
            log(Level.INFO, String.format("%-30s %-25s %s", o.getClass().getSimpleName(), o.getLogicalName(),
                    o.getDescription()));
        }
    }

    /**
     * Reads selected DLMS object with selected attribute index.
     *
     * @param item
     * @param attributeIndex
     * @return
     * @throws Exception
     */
    public Object read(final GXDLMSObject item, final int attributeIndex) throws Exception {
        final byte[] data = client.read(item.getName(), item.getObjectType(), attributeIndex)[0];
        final GXReplyData reply = new GXReplyData();
        readDataBlock(data, reply);
        // Update data type on read.
        if (item.getDataType(attributeIndex) == DataType.NONE) {
            item.setDataType(attributeIndex, reply.getValueType());
        }
        return client.updateValue(item, attributeIndex, reply.getValue());
    }

    /**
     * Reads selected DLMS object with selected attribute index.
     *
     * @param item
     * @param attributeIndex
     * @param noMeter
     * @return
     * @throws Exception
     */
    public Object readMeter(final GXDLMSObject item, final int attributeIndex, String noMeter) throws Exception {
        GXDLMSGateway meterGateway = client.getGateway();
        client.setGateway(null);
        final byte[] data = client.read(item.getName(), item.getObjectType(), attributeIndex)[0];
        final GXReplyData reply = new GXReplyData();
        byte[] messages = data;
//        if (client.getAuthentication().getValue() > Authentication.LOW
//                .getValue()) {
        messages = addMeterFrame(noMeter, messages);
//        }
        readDataBlockMeter(messages, reply, noMeter);
        // Update data type on read.
        if (item.getDataType(attributeIndex) == DataType.NONE) {
            item.setDataType(attributeIndex, reply.getValueType());
        }
        client.setGateway(meterGateway);
        return client.updateValue(item, attributeIndex, reply.getValue());
    }

    /*
     * Read list of attributes.
     */
    public void readList(final List<Entry<GXDLMSObject, Integer>> list) throws Exception {
        if (list.size() != 0) {
            final byte[][] data = client.readList(list);
            final GXReplyData reply = new GXReplyData();
            final List<Object> values = new ArrayList<Object>(list.size());
            for (final byte[] it : data) {
                readDataBlock(it, reply);
                // Value is null if data is send in multiple frames.
                if (reply.getValue() != null) {
                    values.addAll((List<?>) reply.getValue());
                }
                reply.clear();
            }
            if (values.size() != list.size()) {
                throw new Exception("Invalid reply. Read items count do not match.");
            }
            client.updateValues(list, values);
        }
    }

    /**
     * Writes value to DLMS object with selected attribute index.
     *
     * @param item
     * @param attributeIndex
     * @throws Exception
     */
    public void write(final GXDLMSObject item, final int attributeIndex) throws Exception {
        final byte[][] data = client.write(item, attributeIndex);
        readDLMSPacket(data);
    }

    public void writeMeterList(final GXDLMSObject item, final int attributeIndex,
            List<MeterArchiveObj> meterArchiveObjs) throws Exception {
        byte[][] data = client.write(item, attributeIndex);
        byte[][] bses = data;
        try {
            List<Byte> list = new ArrayList<>();
            list.add((byte) 0xC1);
            list.add((byte) 0x1);
            list.add((byte) 0xC1);
            list.add((byte) 0x0);
            list.add((byte) 0x1);
            list.add((byte) 0x0);
            list.add((byte) 0x0);
            list.add((byte) 0x60);
            list.add((byte) 0x36);
            list.add((byte) 0x1);
            list.add((byte) 0xFF);
            list.add((byte) 0x2);
            list.add((byte) 0x0);
            list.add((byte) 0x1);
            list.add((byte) meterArchiveObjs.size());
            for (MeterArchiveObj meterArchiveObj : meterArchiveObjs) {
                list.add((byte) 0x2);
                list.add((byte) 0x2);
                list.add((byte) 0x9);
                list.add((byte) meterArchiveObj.getMeterSerialNumber().getBytes().length);
                for (byte aByte : meterArchiveObj.getMeterSerialNumber().getBytes()) {
                    list.add((byte) aByte);
                }
                list.add((byte) 0x2);
                list.add((byte) 0x6);
                list.add((byte) 0x16);
                list.add((byte) meterArchiveObj.getMeterType().getMeterType());
                list.add((byte) 0x16);
                list.add((byte) meterArchiveObj.getMeterType().getMeterCommType());
                list.add((byte) 0x16);
                list.add((byte) meterArchiveObj.getMeterType().getMeterProtocolType());
                list.add((byte) 0x16);
                list.add((byte) meterArchiveObj.getMeterType().getMeterPrepaidType());
                list.add((byte) 0x16);
                list.add((byte) meterArchiveObj.getMeterType().getMeterWiringType());
                list.add((byte) 0xA);
                list.add((byte) meterArchiveObj.getMeterType().getPassword().getBytes().length);
                for (byte aByte : meterArchiveObj.getMeterType().getPassword().getBytes()) {
                    list.add((byte) aByte);
                }
            }
            int ur = 0;
            for (byte[] bs : data) {
//                System.out.println(GXCommon.bytesToHex(bs));
                List<Byte> listBytes = new ArrayList<>();
                for (int i = 0; i < 7; i++) {
                    listBytes.add(bs[i]);
                }
                listBytes.add((byte) list.size());
                for (Byte byte1 : list) {
                    listBytes.add(byte1);
                }
                byte[] bs1 = new byte[listBytes.size()];
                for (int i = 0; i < bs1.length; i++) {
                    bs1[i] = listBytes.get(i);
                }
                bses[ur] = bs1;
            }
//            for (byte[] bse : bses) {
//                log(Level.INFO, GXCommon.bytesToHex(bse));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        final byte[][] data = client.write(item, attributeIndex);
        readDLMSPacket(bses);
    }

    /**
     * Read profile generic (Historical) data.
     */
    public void getProfileGenerics(GXDLMSObjectCollection profileGenerics) throws Exception {
        Object[] cells;

        for (final GXDLMSObject it : profileGenerics) {
            if (!(it instanceof GXDLMSProfileGeneric)) {
                continue;
            }
            log(Level.INFO, "-------- Reading " + it.getClass().getSimpleName() + " " + it.getName().toString() + " "
                    + it.getDescription());

            final long entriesInUse = ((Number) read(it, 7)).longValue();
            final long entries = ((Number) read(it, 8)).longValue();
            log(Level.INFO, "Entries: " + String.valueOf(entriesInUse) + "/" + String.valueOf(entries));
            final GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it;
            // If there are no columns.
            if (entriesInUse == 0 || pg.getCaptureObjects().size() == 0) {
                continue;
            }
            ///////////////////////////////////////////////////////////////////
            // Read first item.
            try {
                cells = readRowsByEntry(pg, 1, 1);
                for (final Object rows : cells) {
                    for (final Object cell : (Object[]) rows) {
                        if (cell instanceof byte[]) {
                            log(Level.INFO, GXCommon.bytesToHex((byte[]) cell) + " | ");
                        } else {
                            log(Level.INFO, cell + " | ");
                        }
                    }
                    log(Level.INFO, "");
                }
            } catch (final Exception ex) {
                log(Level.SEVERE, "Error! Failed to read first row: " + ex.getMessage());
                // Continue reading if device returns access denied error.
            }
            ///////////////////////////////////////////////////////////////////
            // Read last day.
            try {
                final java.util.Calendar start = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("UTC"));
                start.set(java.util.Calendar.HOUR_OF_DAY, 0); // set hour to
                // midnight
                start.set(java.util.Calendar.MINUTE, 0); // set minute in
                // hour
                start.set(java.util.Calendar.SECOND, 0); // set second in
                // minute
                start.set(java.util.Calendar.MILLISECOND, 0);
                start.add(java.util.Calendar.DATE, -1);

                final java.util.Calendar end = java.util.Calendar.getInstance();
                end.set(java.util.Calendar.MINUTE, 0); // set minute in hour
                end.set(java.util.Calendar.SECOND, 0); // set second in
                // minute
                end.set(java.util.Calendar.MILLISECOND, 0);
                cells = readRowsByRange((GXDLMSProfileGeneric) it, start.getTime(), end.getTime());
                for (final Object rows : cells) {
                    for (final Object cell : (Object[]) rows) {
                        if (cell instanceof byte[]) {
                            System.out.print(GXCommon.bytesToHex((byte[]) cell) + " | ");
                        } else {
                            log(Level.INFO, cell + " | ");
                        }
                    }
                    log(Level.INFO, "");
                }
            } catch (final Exception ex) {
                log(Level.SEVERE, "Error! Failed to read last day: " + ex.getMessage());
                // Continue reading if device returns access denied error.
            }
        }
    }

    public List<JSONObject> getProfileGenericsByDates(GXDLMSProfileGeneric pg, java.util.Calendar start, java.util.Calendar end) throws Exception {
        Object[] cells;
        if (pg.getCaptureObjects().size() == 0) {
            return null;
        }
        ///////////////////////////////////////////////////////////////////
        // Read first item.
        List<JSONObject> Ljson = new ArrayList<JSONObject>();
        try {

            List<Entry<GXDLMSObject, GXDLMSCaptureObject>> li = pg.getCaptureObjects();
            cells = readRowsByEntry(pg, 1, 1);
            for (final Object rows : cells) {
                int i = 1;

                JSONObject json = new JSONObject();
                for (final Object cell : (Object[]) rows) {

                    String obis = li.get(i - 1).getKey().getLogicalName();
                    if (cell instanceof byte[]) {
                        log(Level.INFO, obis + " " + GXCommon.bytesToHex((byte[]) cell) + " | ");
                        json.put(obis, GXCommon.bytesToHex((byte[]) cell));
                    } else if (cell instanceof GXDateTime) {
                        GXDateTime dt = (GXDateTime) cell;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                        log(Level.INFO, obis + " " + sdf.format(dt.getValue()) + " | ");
                        json.put(obis, sdf.format(dt.getValue()));

                    } else {
                        log(Level.INFO, obis + " " + cell + " | ");
                        json.put(obis, cell);
                    }
                    i++;

                }
                Ljson.add(json);
                log(Level.INFO, "");

            }
        } catch (final Exception ex) {
            log(Level.SEVERE, "Error! Failed to read first row: " + ex.getMessage());
            // Continue reading if device returns access denied error.
        }
        ///////////////////////////////////////////////////////////////////
        // Read last day.
        try {
            // final java.util.Calendar start = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("UTC"));
            // start.set(java.util.Calendar.HOUR_OF_DAY, 0); // set hour to
            //                                               // midnight
            // start.set(java.util.Calendar.MINUTE, 0); // set minute in
            //                                          // hour
            // start.set(java.util.Calendar.SECOND, 0); // set second in
            //                                          // minute
            // start.set(java.util.Calendar.MILLISECOND, 0);
            // start.add(java.util.Calendar.DATE, -1);

            // final java.util.Calendar end = java.util.Calendar.getInstance();
            // end.set(java.util.Calendar.MINUTE, 0); // set minute in hour
            // end.set(java.util.Calendar.SECOND, 0); // set second in
            //                                        // minute
            // end.set(java.util.Calendar.MILLISECOND, 0);
            List<Entry<GXDLMSObject, GXDLMSCaptureObject>> li = pg.getCaptureObjects();
            // cells = readRowsByRange(pg, start.getTime(), end.getTime());
            // List<JSONObject> Ljson = new ArrayList<>();
            cells = readRowsByEntry(pg, 1, 1);
            for (final Object rows : cells) {
                JSONObject json = new JSONObject();
                int i = 1;
                for (final Object cell : (Object[]) rows) {

                    String obis = li.get(i - 1).getKey().getLogicalName();
                    if (cell instanceof byte[]) {
                        log(Level.INFO, obis + " " + GXCommon.bytesToHex((byte[]) cell) + " | ");
                        json.put(obis, GXCommon.bytesToHex((byte[]) cell));

                    } else if (cell instanceof GXDateTime) {
                        GXDateTime dt = (GXDateTime) cell;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                        log(Level.INFO, obis + " " + sdf.format(dt.getValue()) + " | ");
                        json.put(obis, sdf.format(dt.getValue()));

                    } else {
                        log(Level.INFO, obis + " " + cell + " | ");
                        json.put(obis, cell);
                    }
                    i++;

                }
                Ljson.add(json);
                log(Level.INFO, "");
            }
        } catch (final Exception ex) {
            log(Level.SEVERE, "Error! Failed to read last day: " + ex.getMessage());
            // Continue reading if device returns access denied error.
        }
        return Ljson;
        // }
    }

    public List<Document> getProfileGenericsMongo(GXDLMSProfileGeneric pg, String jenis, java.util.Calendar start, java.util.Calendar end,
            String UID, Params p) throws Exception {
        Object[] cells;
        if (pg.getCaptureObjects().size() == 0) {
            return null;
        }
        ///////////////////////////////////////////////////////////////////
        // Read first item.
        List<JSONObject> Ljson = new ArrayList<JSONObject>();
        HashMap<String, String> dataObisList = MongoDB.getDataObis(jenis);
        List<Document> documents = new ArrayList<>();
        java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
        // try {

        //     List<Entry<GXDLMSObject, GXDLMSCaptureObject>> li = pg.getCaptureObjects();
        //     cells = readRowsByEntry(pg, 1, 1);
        //     for (final Object rows : cells) {
        //         int i = 1;
        //         Document doc = new Document("TGL_MULAI", now.getTime())
        // 			.append("UID", UID)
        // 			.append("MERK_METER", p.getMerkMeter())
        // 			.append("TYPE_METER", p.getTypeMeter())
        // 			.append("NO_METER", p.getNoMeter())
        // 			.append("KD_PUSAT", p.getKdPusat())
        // 			.append("KD_UNIT_BISNIS", p.getKdUnitBisnis())
        // 			.append("KD_PEMBANGKIT", p.getKdPembangkit())
        // 			.append("KD_AREA", p.getKdArea())
        // 			.append("JENIS", p.getJenis())
        // 			.append("OBIS", pg.getLogicalName())
        // 			.append("DESKRIPSI", pg.getDescription());
        //         // JSONObject json =new JSONObject();
        //         for (final Object cell : (Object[]) rows) {
        //             String obis = li.get(i - 1).getKey().getLogicalName();
        //             if (cell instanceof byte[]) {
        //                 log(Level.INFO, obis+" "+ GXCommon.bytesToHex((byte[]) cell) + " | ");
        //                 // json.put(obis, GXCommon.bytesToHex((byte[]) cell));
        //                 if (dataObisList.containsKey(obis)) {
        //                     // doc.remove(dataObisList.get(obis));
        //                     doc.append(dataObisList.get(obis), GXCommon.bytesToHex((byte[]) cell));
        //                 } else {
        //                     doc.append("Obs" + obis.replace(".", ""), GXCommon.bytesToHex((byte[]) cell));
        //                 }
        //             } else if (cell instanceof GXDateTime) {
        //                 GXDateTime dt = (GXDateTime) cell;
        //                 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        //                 sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
        //                 log(Level.INFO, obis+" "+ sdf.format(dt.getValue()) + " | ");
        //                 // json.put(obis, sdf.format(dt.getValue()));
        //                 if (dataObisList.containsKey(obis)) {
        //                     // doc.remove(dataObisList.get(obis));
        //                     doc.append(dataObisList.get(obis), sdf.format(dt.getValue()));
        //                 } else {
        //                     doc.append("Obs" + obis.replace(".", ""), sdf.format(dt.getValue()));
        //                 }
        //             } else {
        //                 log(Level.INFO,obis+" "+ cell + " | ");
        //                 // json.put(obis, cell);
        //                 if (dataObisList.containsKey(obis)) {
        //                     // doc.remove(dataObisList.get(obis));
        //                     doc.append(dataObisList.get(obis), cell);
        //                 } else {
        //                     doc.append("Obs" + obis.replace(".", ""), cell);
        //                 }
        //             }
        //             i++;
        //         }
        //         documents.add(doc);
        //         // Ljson.add(json);
        //         log(Level.INFO, "");
        //     }
        // } catch (final Exception ex) {
        //     log(Level.SEVERE, "Error! Failed to read first row: " + ex.getMessage());
        //     // Continue reading if device returns access denied error.
        // }
        ///////////////////////////////////////////////////////////////////
        // Read last day.
        try {
            // final java.util.Calendar start = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("UTC"));
            // start.set(java.util.Calendar.HOUR_OF_DAY, 0); // set hour to
            //                                               // midnight
            // start.set(java.util.Calendar.MINUTE, 0); // set minute in
            //                                          // hour
            // start.set(java.util.Calendar.SECOND, 0); // set second in
            //                                          // minute
            // start.set(java.util.Calendar.MILLISECOND, 0);
            // start.add(java.util.Calendar.DATE, -1);

            // final java.util.Calendar end = java.util.Calendar.getInstance();
            // end.set(java.util.Calendar.MINUTE, 0); // set minute in hour
            // end.set(java.util.Calendar.SECOND, 0); // set second in
            //                                        // minute
            // end.set(java.util.Calendar.MILLISECOND, 0);
            log(Level.INFO, "Start ++++++++++++++++++++++ " + start.getTime());
            log(Level.INFO, "End ++++++++++++++++++++++ " + end.getTime());
            List<Entry<GXDLMSObject, GXDLMSCaptureObject>> li = pg.getCaptureObjects();
            cells = readRowsByRange(pg, start.getTime(), end.getTime());
            // List<JSONObject> Ljson = new ArrayList<>();
            log(Level.INFO, "Total Data : " + cells.length + "");
            for (final Object rows : cells) {
                Document doc = new Document("TGL_MULAI", now.getTime())
                        .append("UID", UID)
                        .append("MERK_METER", p.getMerkMeter())
                        .append("TYPE_METER", p.getTypeMeter())
                        .append("NO_METER", p.getNoMeter())
                        .append("KD_PUSAT", p.getKdPusat())
                        .append("KD_UNIT_BISNIS", p.getKdUnitBisnis())
                        .append("KD_PEMBANGKIT", p.getKdPembangkit())
                        .append("KD_AREA", p.getKdArea())
                        .append("JENIS", p.getJenis())
                        .append("OBIS", pg.getLogicalName())
                        .append("DESKRIPSI", pg.getDescription());
                int i = 1;
                for (final Object cell : (Object[]) rows) {

                    String obis = li.get(i - 1).getKey().getLogicalName();
                    if (cell instanceof byte[]) {
                        log(Level.INFO, obis + " " + GXCommon.bytesToHex((byte[]) cell) + " | ");
                        // json.put(obis, GXCommon.bytesToHex((byte[]) cell));
                        if (dataObisList.containsKey(obis)) {
                            // doc.remove(dataObisList.get(obis));
                            doc.append(dataObisList.get(obis), GXCommon.bytesToHex((byte[]) cell));
                        } else {
                            doc.append("Obs" + obis.replace(".", ""), GXCommon.bytesToHex((byte[]) cell));
                        }
                    } else if (cell instanceof GXDateTime) {
                        GXDateTime dt = (GXDateTime) cell;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                        log(Level.INFO, obis + " " + sdf.format(dt.getValue()) + " | ");
                        if (dataObisList.containsKey(obis)) {
                            if (obis.equals("TGL_BACA")) {
                                doc.append(dataObisList.get(obis), dt.getValue());
                            } else {
                                doc.append(dataObisList.get(obis), sdf.format(dt.getValue()));
                            }
                        } else {
                            doc.append("Obs" + obis.replace(".", ""), sdf.format(dt.getValue()));
                        }
                    } else {
                        log(Level.INFO, obis + " " + cell + " | ");
                        if (dataObisList.containsKey(obis)) {
                            doc.append(dataObisList.get(obis), cell);
                        } else {
                            doc.append("Obs" + obis.replace(".", ""), cell);
                        }
                    }
                    i++;

                }
                documents.add(doc);
                // Ljson.add(json);
                log(Level.INFO, "");
            }
        } catch (final Exception ex) {
            log(Level.SEVERE, "Error! Failed to read last day: " + ex.getMessage());
            // Continue reading if device returns access denied error.
        }
        return documents;
        // }
    }

    /*
     * Returns columns of profile Generic.
     */
    public List<Entry<GXDLMSObject, GXDLMSCaptureObject>> GetColumns(final GXDLMSProfileGeneric pg) throws Exception {
        final Object entries = read(pg, 7);
        System.out.println("Reading Profile Generic: " + pg.getLogicalName() + " " + pg.getDescription() + " entries:"
                + entries.toString());
        final GXReplyData reply = new GXReplyData();
        final byte[] data = client.read(pg.getName(), pg.getObjectType(), 3)[0];
        readDataBlock(data, reply);
        client.updateValue((GXDLMSObject) pg, 3, reply.getValue());
        return pg.getCaptureObjects();
    }

    /**
     * Read Profile Generic's data by entry start and count.
     *
     * @param pg
     * @param index
     * @param count
     * @return
     * @throws Exception
     */
    public Object[] readRowsByEntry(final GXDLMSProfileGeneric pg, final int index, final int count) throws Exception {
        final byte[][] data = client.readRowsByEntry(pg, index, count);
        final GXReplyData reply = new GXReplyData();
        readDataBlock(data, reply);
        return (Object[]) client.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Read Profile Generic's data by range (start and end time).
     *
     * @param pg
     * @param sortedItem
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    public Object[] readRowsByRange(final GXDLMSProfileGeneric pg, final Date start, final Date end) throws Exception {
        final GXReplyData reply = new GXReplyData();
        final byte[][] data = client.readRowsByRange(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) client.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Read Profile Generic's data by range (start and end time).
     *
     * @param pg
     * @param sortedItem
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    public Object[] readRowsByRangeMeter(final GXDLMSProfileGeneric pg, final Calendar start,
            final Calendar end, String noMeter) throws Exception {
        LOGGER.log(Level.INFO, "Start = " + start.getTime());
        LOGGER.log(Level.INFO, "End = " + end.getTime());
        GXDLMSGateway meterGateway = client.getGateway();
        client.setGateway(null);
        final GXReplyData reply = new GXReplyData();
        final byte[][] data = client.readRowsByRange(pg, start, end);
        byte[][] bses = data;
        for (int i = 0; i < data.length; i++) {
            bses[i] = addMeterFrame(noMeter, data[i]);
        }
        readDataBlockMeter(data, reply, noMeter);
        client.setGateway(meterGateway);
        return (Object[]) client.updateValue(pg, 2, reply.getValue());
    }

    private void readDataBlockMeter(final byte[][] data, final GXReplyData reply, String noMeter) throws Exception {
        if (data != null) {
            for (final byte[] it : data) {
                reply.clear();
                readDataBlockMeter(it, reply, noMeter);
            }
        }
    }

    /**
     * Reads next data block.
     *
     * @param data
     * @return
     * @throws Exception
     */
    private void readDataBlockMeter(byte[] data, final GXReplyData reply, String noMeter) throws Exception {
        if (data != null && data.length != 0) {
            readDLMSPacket(data, reply);
            while (reply.isMoreData()) {
                if (reply.isStreaming()) {
                    data = null;
                } else {
                    data = addMeterFrame(noMeter, client.receiverReady(reply));
                }
                readDLMSPacket(data, reply);
            }
        }
    }

    /*
     * Read profile generic columns from the meter.
     */
    public void getProfileGenericColumns() {
        final GXDLMSObjectCollection profileGenerics = client.getObjects().getObjects(ObjectType.PROFILE_GENERIC);
        for (final GXDLMSObject it : profileGenerics) {
            log(Level.INFO, "Profile Generic " + it.getName() + " Columns:");
            final GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it;
            // Read columns.
            try {
                read(pg, 3);
                boolean first = true;
                final StringBuilder sb = new StringBuilder();
                for (final Entry<GXDLMSObject, GXDLMSCaptureObject> col : pg.getCaptureObjects()) {
                    if (!first) {
                        sb.append(" | ");
                    }
                    sb.append(col.getKey().getName());
                    sb.append(" ");
                    final String desc = col.getKey().getDescription();
                    if (desc != null) {
                        sb.append(desc);
                    }
                    first = false;
                }
                log(Level.INFO, sb.toString());
            } catch (final Exception ex) {
                log(Level.SEVERE, "Err! Failed to read columns:" + ex.getMessage());
                // Continue reading.
            }
        }
    }

    public void getProfileGenericColumnsById(GXDLMSProfileGeneric pg) {
        // final GXDLMSObjectCollection profileGenerics = client.getObjects().getObjects(ObjectType.PROFILE_GENERIC);
        // for (final GXDLMSObject it : profileGenerics) {
        //     log(Level.INFO, "Profile Generic " + it.getName() + " Columns:");
        //     final GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it;
        // Read columns.
        try {
            read(pg, 3);
            boolean first = true;
            final StringBuilder sb = new StringBuilder();
            for (final Entry<GXDLMSObject, GXDLMSCaptureObject> col : pg.getCaptureObjects()) {
                if (!first) {
                    sb.append(" | ");
                }
                sb.append(col.getKey().getName());
                sb.append(" ");
                final String desc = col.getKey().getDescription();
                if (desc != null) {
                    sb.append(desc);
                }
                first = false;
            }
            log(Level.INFO, sb.toString());
        } catch (final Exception ex) {
            log(Level.SEVERE, "Err! Failed to read columns:" + ex.getMessage());
            // Continue reading.
        }
        // }
    }

    /**
     * Read all data from the meter except profile generic (Historical) data.
     */
    void getReadOut() {
        for (final GXDLMSObject it : client.getObjects()) {
            if (!(it instanceof IGXDLMSBase)) {
                // If interface is not implemented.
                log(Level.WARNING, "Unknown Interface: " + it.getObjectType().toString());
                continue;
            }

            if (it instanceof GXDLMSProfileGeneric) {
                // Profile generic are read later
                // because it might take so long time
                // and this is only a example.
                continue;
            }
            log(Level.INFO, "-------- Reading " + it.getClass().getSimpleName() + " " + it.getName().toString() + " "
                    + it.getDescription());
            for (final int pos : ((IGXDLMSBase) it).getAttributeIndexToRead(true)) {
                try {
                    final Object val = read(it, pos);
                    showValue(pos, val);
                } catch (final Exception ex) {
                    log(Level.SEVERE, "Error! Index: " + pos + " " + ex.getMessage());
                    log(Level.SEVERE, ex.toString());
                    // Continue reading.
                }
            }
        }
    }

    public String showValue(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = GXCommon.bytesToHex((byte[]) val);
        } else if (val instanceof Double) {
            final NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            final StringBuilder sb = new StringBuilder();
            for (final Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                if (tmp instanceof byte[]) {
                    sb.append(GXCommon.bytesToHex((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            final StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                final Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(GXCommon.bytesToHex((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        log(Level.INFO, "Index: " + pos + " Value: " + String.valueOf(val));
        return String.valueOf(val);
    }

    void readScalerAndUnits() throws Exception {
        GXDLMSObjectCollection objs = client.getObjects().getObjects(
                new ObjectType[]{ObjectType.REGISTER, ObjectType.DEMAND_REGISTER, ObjectType.EXTENDED_REGISTER});
        try {
            if (client.getNegotiatedConformance().contains(Conformance.MULTIPLE_REFERENCES)) {
                List<Entry<GXDLMSObject, Integer>> list = new ArrayList<Entry<GXDLMSObject, Integer>>();
                for (GXDLMSObject it : objs) {
                    if (it instanceof GXDLMSRegister) {
                        list.add(new GXSimpleEntry<GXDLMSObject, Integer>(it, 3));
                    }
                    if (it instanceof GXDLMSDemandRegister) {
                        list.add(new GXSimpleEntry<GXDLMSObject, Integer>(it, 4));
                    }
                }
                readList(list);
            }
        } catch (Exception e) {
            // Some meters are set multiple references, but don't support it.
            client.getNegotiatedConformance().remove(Conformance.MULTIPLE_REFERENCES);
        }
        if (!client.getNegotiatedConformance().contains(Conformance.MULTIPLE_REFERENCES)) {
            for (GXDLMSObject it : objs) {
                try {
                    if (it instanceof GXDLMSRegister) {
                        read(it, 3);
                    } else if (it instanceof GXDLMSDemandRegister) {
                        read(it, 4);
                    }
                } catch (Exception e) {
                    // Actaric SL7000 can return error here. Continue reading.
                }
            }
        }
    }

    public void readValues(GXDLMSObjectCollection objs) throws Exception {
        for (final GXDLMSObject it : objs) {
            try {
                if ((it instanceof GXDLMSClock) || (it instanceof GXDLMSData)) {
                    read(it, 2);
                } else if (it instanceof GXDLMSRegister) {
                    read(it, 2);
                } else if ((it instanceof GXDLMSDemandRegister) || (it instanceof GXDLMSActionSchedule)) {
                    read(it, 4);
                } else {
                    log(Level.INFO, it.getClass().getName());
                }
                logObject(it);
            } catch (final Exception e) {
                log(Level.SEVERE, e.getMessage());
                // Actaric SL7000 can return error here. Continue reading.
            }
        }
    }

    void logObject(GXDLMSObject obj) {
        if (obj instanceof GXDLMSClock) {
            log(Level.INFO, String.format("%-15s %-60s: %s", obj.getLogicalName(), obj.getDescription(),
                    ((GXDLMSClock) obj).getTime().toString()));
        } else if (obj instanceof GXDLMSData) {
            log(Level.INFO, String.format("%-15s %-60s: %s", obj.getLogicalName(), obj.getDescription(),
                    ((GXDLMSData) obj).getValue().toString()));
        } else if (obj instanceof GXDLMSRegister) {
            GXDLMSRegister r = (GXDLMSRegister) obj;
            log(Level.INFO, String.format("%-15s %-60s: %.3f %s", r.getLogicalName(), r.getDescription(),
                    ((Number) r.getValue()).intValue(), r.getUnit().toString()));
        } else if (obj instanceof GXDLMSDemandRegister) {
            GXDLMSDemandRegister r = (GXDLMSDemandRegister) obj;
            log(Level.INFO, String.format("%-15s %-60s: %10s %s", r.getLogicalName(), r.getDescription(),
                    r.getCurrentAverageValue().toString(), r.getUnit().toString()));
        } else if (obj instanceof GXDLMSActionSchedule) {
            log(Level.INFO, String.format("%-15s %-60s: %s", obj.getLogicalName(), obj.getDescription(),
                    ((GXDLMSActionSchedule) obj).getExecutionTime()[0].toString()));
        } else {
            log(Level.INFO, "Unspecified logger handler for: " + obj.getClass().getName());
        }
    }

    //Costum by Yonni
    public List<ResultRead> getRegister(GXObisCode skipItem) throws Exception, InterruptedException {
//		System.out.println("Start getData");
        List<ResultRead> rslt = new ArrayList<ResultRead>();
        try {
            GXDLMSObject objForMeterType = objFromCode(skipItem);
            try {
                int[] posId = ((IGXDLMSBase) objForMeterType).getAttributeIndexToRead(true);
                if (objForMeterType.getObjectType() == ObjectType.CLOCK) {
                    posId = new int[]{2};
                }
                System.out.println("posId --> " + posId.length);
                for (int pos : posId) {
                    try {
                        System.out.println("posId ke-" + pos);
                        String type = "";
                        Object val = read(objForMeterType, pos);

                        if (val instanceof GXDateTime) {
                            val = ((GXDateTime) val).getValue();
                            type = "Date";
                        } else if (val instanceof byte[]) {
                            val = new String((byte[]) val);
                            type = "Byte";
                        } else if (val instanceof Double) {
                            NumberFormat formatter = NumberFormat.getNumberInstance();
                            val = formatter.format(val);
                            type = "Number";
                        } else if (val != null && val.getClass().isArray()) {
                            String str = "";
                            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                                if (!str.equals("")) {
                                    str += ", ";
                                }
                                Object tmp = Array.get(val, pos2);
                                if (tmp instanceof byte[]) {
                                    str += GXCommon.bytesToHex((byte[]) tmp);
                                } else {
                                    str += String.valueOf(tmp);
                                }
                            }
                            val = str;
                            type = "String";
                        }
                        dataMeter = val;
                        System.out.println("---> " + dataMeter);
                        ResultRead rr = new ResultRead<>();
                        if (objForMeterType.getObjectType() == ObjectType.EXTENDED_REGISTER) {
                            if (pos == 2) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("");
                                rslt.add(rr);
                            } else if (pos == 5) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_TGL");
                                rslt.add(rr);
                            }
                        } else if (objForMeterType.getObjectType() == ObjectType.DEMAND_REGISTER) {
                            if (pos == 2) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("");
                                rslt.add(rr);
                            } else if (pos == 3) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_LAST_AVG");
                                rslt.add(rr);
                            } else if (pos == 6) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_TGL");
                                rslt.add(rr);
                            } else if (pos == 7) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_TGL_MULAI");
                                rslt.add(rr);
                            } else if (pos == 8) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_PERIOD");
                                rslt.add(rr);
                            } else if (pos == 9) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("_NUMBER_PERIOD");
                                rslt.add(rr);
                            }
                        } else {
                            if (pos == 2) {
                                rr.setType(type);
                                rr.setResult(dataMeter);
                                rr.setAlias("");
                                rslt.add(rr);
                            }
                        }
                    } catch (Exception ex1) {
                        ex1.printStackTrace();
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                dataMeter = null;
                ResultRead rr = new ResultRead<>();
                rr.setType("");
                rr.setResult(null);
                rslt.add(rr);
//				System.out.println("Error1! " + ex.getMessage());
                if (ex.getMessage() != null && ex.getMessage().equals("Failed to receive reply from the device in given time.")) {
                    throw new Exception("Failed to receive reply from the device in given time.");
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            dataMeter = null;
            ResultRead rr = new ResultRead<>();
            rr.setType("");
            rr.setResult(null);
            rslt.add(rr);
//			System.out.println("Error2! " + ex.getMessage());
            if (ex.getMessage() != null && ex.getMessage().equals("Failed to receive reply from the device in given time.")) {
                throw new Exception("Failed to receive reply from the device in given time.");
            }
        }
        return rslt;
    }

    public Object readObject(GXDLMSObject item, int attributeIndex) throws Exception {
//		System.out.println(attributeIndex);
        byte[] data = client.read(item.getName(), item.getObjectType(), attributeIndex)[0];
        GXReplyData reply = new GXReplyData();

        readDataBlock(data, reply);
        // Update data type on read.
        if (item.getDataType(attributeIndex) == DataType.NONE) {
            item.setDataType(attributeIndex, reply.getValueType());
        }
        return client.updateValue(item, attributeIndex, reply.getValue());
    }

    public GXDLMSObject objFromCode(GXObisCode code) {
        if (null != code.getObjectType()) {
            System.out.println(code.getLogicalName());
            System.out.println((ObjectType) code.getObjectType());
            System.out.println(code.getVersion());
            switch (code.getObjectType()) {
                case CLOCK: {
                    GXDLMSClock obj = new GXDLMSClock();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
                case DATA: {
                    GXDLMSData obj = new GXDLMSData();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
                case REGISTER: {
                    GXDLMSRegister obj = new GXDLMSRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;

                }
                case EXTENDED_REGISTER: {
                    GXDLMSExtendedRegister obj = new GXDLMSExtendedRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
                case DEMAND_REGISTER: {
                    GXDLMSDemandRegister obj = new GXDLMSDemandRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
                case PROFILE_GENERIC: {
                    GXDLMSProfileGeneric obj = new GXDLMSProfileGeneric();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
                default: {
                    GXDLMSObject obj = new GXDLMSObject();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    obj.setVersion(code.getVersion());
                    return obj;
                }
            }
        } else {
            return null;
        }
    }

    public ObjectType getObjectType(String type) {
        if (isNumeric(type)) {
            return ObjectType.forValue(Integer.parseInt(type));
        }
        switch (type) {
            case "Register":
                return ObjectType.REGISTER;
            case "Data":
                return ObjectType.DATA;
            case "Clock":
                return ObjectType.CLOCK;
            case "ExtendRegister":
                return ObjectType.EXTENDED_REGISTER;
            default:
                return ObjectType.REGISTER;
        }
    }

    public Object convertData(ResultRead rr) {
        if (rr.getType().equals("Number")) {
            System.out.println(rr.getResult().toString());

            NumberFormat nf_in = NumberFormat.getNumberInstance(Locale.getDefault());
            try {
                return nf_in.parse(rr.getResult().toString()).doubleValue();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        } else if (rr.getType().equals("Date")) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
            return sdf.format(rr.getResult());
        } else {
            return rr.getResult();
        }
    }

    public Object convertData(ResultRead rr, String merkMeter, String classId) {
        if (rr.getType().equals("Number")) {
            NumberFormat nf_in = NumberFormat.getNumberInstance(Locale.getDefault());
//			if (merkMeter.equals("EDMI")) {
//				data.replace(, newChar)
//				nf_in = NumberFormat.getNumberInstance(Locale.UK);
//			}
//			System.out.println("Locale: " + currentLocale.getDisplayName());
//			Currency currency = Currency.getInstance(currentLocale);
//			NumberFormat formatter = NumberFormat.getNumberInstance();
//			System.out.println("Numeric Code: " + formatter.getCurrency().getNumericCode());
//			System.out.println("Currency Code: " + currency.getCurrencyCode());
//			System.out.println("Symbol: " + currency.getSymbol());
//			System.out.println("Default Fraction Digits: " + currency.getDefaultFractionDigits());
//
//			System.out.println();
            try {
//				System.out.println("<<==="+Locale.getDefault());
//				System.out.println("<<==="+rr.getResult().toString());
//				System.out.println("<<==="+nf_in.parse(rr.getResult().toString()).doubleValue());
                return nf_in.parse(rr.getResult().toString()).doubleValue();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        } else if (rr.getType().equals("Date")) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            if (!merkMeter.equals("EDMI")) {
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
            } else {
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
            }

            return sdf.format(rr.getResult());
        } else if (rr.getType().equals("Byte")) {
            return rr.getResult().toString();
        } else {
            return rr.getResult();
        }
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public String ifNull(Object txt) {
        if (txt == null) {
            return "";
        }
        return txt.toString();
    }

    public void close() {
        try {
            // if (Media != null && Media.isOpen()) {
//			System.out.println("DisconnectRequest");
            GXReplyData reply = new GXReplyData();
            try {
                readDataBlock(client.releaseRequest(), reply);
            } catch (Exception e) {
                // All meters don't support release.
            }
            reply.clear();
            readDLMSPacket(client.disconnectRequest(), reply);
            // Media.close();
            // }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes value to DLMS object with selected attribute index.
     *
     * @param item
     * @param attributeIndex
     * @throws Exception
     */
    public void writeObject(GXDLMSObject item, int attributeIndex) throws Exception {
        byte[][] data = client.write(item, attributeIndex);
        readDLMSPacket(data);
    }

    /*
	 * Read profile generic columns from the meter.
     */
    public void getProfileGenericColumns(GXDLMSProfileGeneric pg) {
        // Read columns.
        try {
            read(pg, 3);
        } catch (Exception ex) {
//			writeTrace("Err! Failed to read columns:" + ex.getMessage(), TraceLevel.ERROR);
            // Continue reading.
        }
    }

    /**
     * Read association view.
     *
     * @throws Exception
     */
    public GXDLMSObjectCollection getAssociationView() throws Exception {
        GXReplyData reply = new GXReplyData();
        // Get Association view from the meter.
        readDataBlock(client.getObjectsRequest(), reply);
        GXDLMSObjectCollection objects = client.parseObjects(reply.getData(), true);
        // Get description of the objects.
        GXDLMSConverter converter = new GXDLMSConverter();
        converter.updateOBISCodeInformation(objects);
        return objects;

    }

    /**
     * Read profile generic (Historical) data.
     */
    public void getProfileGenerics(GXDLMSProfileGeneric pg, java.util.Calendar startLP, java.util.Calendar endLp, String jenis,
            String UID, Params p, Boolean isReadAll) throws Exception {
        java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));

        Object[] cells;
        GXDLMSObjectCollection profileGenerics = client.getObjects().getObjects(ObjectType.PROFILE_GENERIC);
//		writeTrace("-------- Reading " + pg.getClass().getSimpleName() + " " + pg.getName().toString() + " "
//				+ pg.getDescription(), TraceLevel.INFO);

        long entriesInUse = ((Number) read(pg, 7)).longValue();
        long entries = ((Number) read(pg, 8)).longValue();
//		writeTrace("Entries: " + String.valueOf(entriesInUse) + "/" + String.valueOf(entries), TraceLevel.INFO);
        if (entriesInUse == 0 || pg.getCaptureObjects().size() == 0) {
            return;
        }

        List<Entry<GXDLMSObject, GXDLMSCaptureObject>> li = pg.getCaptureObjects();
        ///////////////////////////////////////////////////////////////////
        // Read first item.
        // try {
        // cells = readRowsByEntry(pg, 1, 1);
        //
        // if (Trace.ordinal() > TraceLevel.WARNING.ordinal()) {
        // for (Object rows : cells) {
        // int i = 1;
        // for (Object cell : (Object[]) rows) {
        // String obis = li.get(i - 1).getKey().getLogicalName();
        // if (cell instanceof byte[]) {
        // writeTrace(GXCommon.bytesToHex((byte[]) cell) + " | ",
        // TraceLevel.INFO);
        // } else {
        // System.out.println(cell);
        // writeTrace(obis + " ==> " + cell + " | ", TraceLevel.INFO);
        // }
        // }
        // writeTrace("", TraceLevel.INFO);
        // i++;
        // }
        // }
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // writeTrace("Error! Failed to read first row: " + ex.getMessage(),
        // TraceLevel.ERROR);
        // // Continue reading if device returns access denied error.
        // }

        ///////////////////////////////////////////////////////////////////
        // Read last day.
        // try {
        // for (Object rows : cells) {
        // int a = 0;
        // for (Object cell : (Object[]) rows) {
        // System.out.print(pg.getCaptureObjects().get(a).getKey().getLogicalName()
        // + " = ");
        // a++;
        // if (cell instanceof byte[]) {
        // System.out.println(new String((byte[]) cell));
        // } else {
        // System.out.println(cell);
        // }
        // }
        // }
        HashMap<String, String> dataObisList = MongoDB.getDataObis(jenis);
//		if (!p.getMerkMeter().equals("EDMI")) {
        int sukses = 0;
        if (!isReadAll) {
            try {
                if (p.getMerkMeter().equals("EDMI")) {
                    if (p.getTypeMeter().toLowerCase().contains("gen1")) {
                        cells = readRowsByRangeEDMI(pg, startLP.getTime(), endLp.getTime(), 1, li.size());
                    } else {
                        cells = readRowsByRangeEDMIGen2(pg, startLP.getTime(), endLp.getTime(), 1, li.size());
                    }
                } else {
                    cells = readRowsByRange(pg, startLP.getTime(), endLp.getTime());
                }
                for (Object rows : cells) {
                    Document doc = new Document("TGL_MULAI", now.getTime())
                            //						.append("TGL_MULAI", now.getTime())
                            .append("UID", UID)
                            .append("MERK_METER", p.getMerkMeter())
                            .append("TYPE_METER", p.getTypeMeter())
                            .append("NO_METER", p.getNoMeter())
                            .append("KD_PUSAT", p.getKdPusat())
                            .append("KD_UNIT_BISNIS", p.getKdUnitBisnis())
                            .append("KD_PEMBANGKIT", p.getKdPembangkit())
                            .append("KD_AREA", p.getKdArea())
                            .append("JENIS", p.getJenis())
                            .append("OBIS", pg.getLogicalName())
                            .append("DESKRIPSI", pg.getDescription())
                            .append("DATA_METER", p.getDbObject() == null ? null : p.getDbObject().toMap());
                    int i = 1;
                    String tglBaca = "";
                    for (Object cell : (Object[]) rows) {
                        String obis = li.get(i - 1).getKey().getLogicalName();
                        if (cell instanceof byte[]) {
//						System.out.println("<<===" + dataObisList.get(obis) + " : " + (GXCommon.bytesToHex((byte[]) cell)) +" - "+ (GXCommon.bytesToHex((byte[]) cell)).toString()+" - "+new String((byte[]) cell));
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), new String((byte[]) cell));
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), new String((byte[]) cell));
                            }

//						if (dataObisList.containsKey(obis)) {
//							doc.remove(dataObisList.get(obis));
//							doc.append(dataObisList.get(obis), GXCommon.bytesToHex((byte[]) cell));
//						} else {
//							doc.append("Obs" + obis.replace(".", ""), GXCommon.bytesToHex((byte[]) cell));
//						}
                            // System.out.print(GXCommon.bytesToHex((byte[]) cell) +
                            // " |
                            // ");
                        } else if (cell instanceof GXDateTime) {
                            GXDateTime dt = (GXDateTime) cell;
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                            if (p.getMerkMeter().equals("EDMI")) {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
                            } else {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                            }

                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), sdf.format(dt.getValue()));
                                if (dataObisList.get(obis).equals("TGL_BACA")) {
                                    tglBaca = sdf.format(dt.getValue());
                                    doc.append(dataObisList.get(obis) + "_UTC", dt.getValue());
                                }
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), sdf.format(dt.getValue()));
                            }
                            // System.out.print(dt.getValue() + " | ");
                        } else if (cell instanceof Double) {
                            // System.out.println(dataObisList.get(obis)+ " :
                            // "+cell);
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), cell);
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), cell);
                            }
                        } else {
//						System.out.println("<<===" + dataObisList.get(obis) + " : " + cell);
                            if (cell instanceof GXBitString) {
                                cell = cell.toString();
                            }
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));

                                doc.append(dataObisList.get(obis), cell);
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), cell);
                            }
                            // System.out.print(cell + " | ");
                        }
                        i++;
                    }
                    now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    doc.append("TGL_SELESAI", now.getTime());
                    try {
                        MongoDB.deleteOne("DATA_" + jenis,
                                new BasicDBObject().append("NO_METER", p.getNoMeter()).append("TGL_BACA", tglBaca));
                        MongoDB.setData("DATA_" + jenis, doc);
                    } catch (Exception e) {
                    }
//				writeTrace("", TraceLevel.INFO);
                }
                sukses = 1;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (!e.getMessage().equals("Access Error : Device reports scope of access violated.")) {
                    throw new Exception(e.getMessage());
                }
            }
        }
//		} else {
        ///////////////////////////////////////////////////////////////////
        // Read EDMI.
        if (sukses == 0 || isReadAll) {
            try {
                read(pg, 2);
                cells = pg.getBuffer();
                // cells = readRowsByEntry(pg, 1, 1);

                // if (Trace.ordinal() > TraceLevel.WARNING.ordinal()) {
                for (Object rows : cells) {
                    Document doc = new Document("TGL_MULAI", now.getTime())
                            //							.append("TGL_MULAI", now.getTime())
                            .append("UID", UID)
                            .append("MERK_METER", p.getMerkMeter())
                            .append("TYPE_METER", p.getTypeMeter())
                            .append("NO_METER", p.getNoMeter())
                            .append("KD_PUSAT", p.getKdPusat())
                            .append("KD_UNIT_BISNIS", p.getKdUnitBisnis())
                            .append("KD_PEMBANGKIT", p.getKdPembangkit())
                            .append("KD_AREA", p.getKdArea())
                            .append("JENIS", p.getJenis())
                            .append("OBIS", pg.getLogicalName())
                            .append("DESKRIPSI", pg.getDescription())
                            .append("DATA_METER", p.getDbObject() == null ? null : p.getDbObject().toMap());

                    int i = 1;
                    String tglBaca = "";
                    for (Object cell : (Object[]) rows) {
                        String obis = li.get(i - 1).getKey().getLogicalName();
//						System.out.println("<<===" + dataObisList.get(obis) + " : " + cell);
                        if (cell instanceof byte[]) {
//							System.out.println("||===" + dataObisList.get(obis) + " : " + new String((byte[]) cell));
//							if(obis.equals("0.0.1.0.0.255")){
//								GXDateTime dt = (GXDateTime) cell;
//								System.out.println("||===" + dataObisList.get(obis) + " : " + dt.getValue());
//							}
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), new String((byte[]) cell));
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), new String((byte[]) cell));
                            }
                            // System.out.print(GXCommon.bytesToHex((byte[])
                            // cell) +
                            // " |
                            // ");
                        } else if (cell instanceof GXDateTime) {
                            GXDateTime dt = (GXDateTime) cell;
//							dt = dt.toFormatString();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                            if (p.getMerkMeter().equals("EDMI")) {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
                            } else {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                            }

                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), sdf.format(dt.getValue()));
                                if (dataObisList.get(obis).equals("TGL_BACA")) {
                                    tglBaca = sdf.format(dt.getValue());
                                    doc.append(dataObisList.get(obis) + "_UTC", dt.getValue());
                                }
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), sdf.format(dt.getValue()));
                            }
//							 System.out.print(dt.getValue() + " | ");
                        } else if (cell instanceof Double) {
//							 System.out.println(dataObisList.get(obis)+ " :"+cell);
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));
                                doc.append(dataObisList.get(obis), cell);
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), cell);
                            }
                        } else {
                            if (cell instanceof GXBitString) {
                                cell = cell.toString();
                            }
                            if (dataObisList.containsKey(obis)) {
                                doc.remove(dataObisList.get(obis));

                                doc.append(dataObisList.get(obis), cell);
                            } else {
                                doc.append("Obs" + obis.replace(".", ""), cell);
                            }
                            // System.out.print(cell + " | ");
                        }
                        i++;
                    }
                    now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    doc.append("TGL_SELESAI", now.getTime());
                    try {
                        MongoDB.deleteOne("DATA_" + jenis,
                                new BasicDBObject().append("NO_METER", p.getNoMeter()).append("TGL_BACA", tglBaca));
                        MongoDB.setData("DATA_" + jenis, doc);
                    } catch (Exception e) {
                    }
//					writeTrace("", TraceLevel.INFO);
                }
                // }
            } catch (Exception ex) {
                ex.printStackTrace();
//				writeTrace("Error! Failed to read first row: " + ex.getMessage(), TraceLevel.ERROR);
                // Continue reading if device returns access denied error.
            }
        }
//		}
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // writeTrace("Error! Failed to read last day: " + ex.getMessage(),
        // TraceLevel.ERROR);
        // Continue reading if device returns access denied error.
        // }
        // }
    }

    public Object[] readRowsByRangeEDMI(final GXDLMSProfileGeneric pg, final Date start, final Date end, final int startRegister, final int numberOfRegister) throws Exception {
        System.out.println("============readRowsByRangeEDMI==========");
        GXReplyData reply = new GXReplyData();
        byte[][] data = client.readRowsByRangeEDMI(pg, start, end, startRegister, numberOfRegister);
//		byte[][] data = dlms.readRowsByRangeEDMI(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) client.updateValue(pg, 2, reply.getValue());
    }

    public Object[] readRowsByRangeEDMIGen2(final GXDLMSProfileGeneric pg, final Date start, final Date end, final int startRegister, final int numberOfRegister) throws Exception {
        System.out.println("============readRowsByRangeEDMI==========");
        GXReplyData reply = new GXReplyData();
        byte[][] data = client.readRowsByRangeEDMIGen2(pg, start, end, startRegister, numberOfRegister);
//		byte[][] data = dlms.readRowsByRangeEDMI(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) client.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Membaca capture object profile generic dan menambahkan kedalam
     * GXDLMSProfileGeneric
     */
    public void getKolomPG(GXDLMSProfileGeneric generic) {
        try {
            read(generic, 3);
            if (Trace.ordinal() > TraceLevel.WARNING.ordinal()) {
                boolean first = true;
                StringBuilder sb = new StringBuilder();
                for (Entry<GXDLMSObject, GXDLMSCaptureObject> col : generic.getCaptureObjects()) {
                    if (!first) {
                        sb.append(" | ");
                    }
                    sb.append(col.getKey().getName());
                    sb.append(" ");
                    String desc = col.getKey().getDescription();
                    if (desc != null) {
                        sb.append(desc);
                    }
                    first = false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Err! Failed to read columns:" + ex.toString());
        }
    }

    /**
     * Menambahkan capture object profile generic ke dalam Object Collection
     * GXDLMSClient sekaligus scaller dan unitnya. Perhatian : Jangan digunakan
     * bila sudah melakukan pembacaan Association View kecuali meter EDMI
     *
     * @param isEDMI : true jika yang dibaca meter EDMI
     * @param isLoadProfile : true jika membaca Load Profile karena Load Profile
     * EDMI scallernya spesial
     */
    public void addToCollection(GXDLMSProfileGeneric generic, boolean isEDMI, boolean isLoadProfile, int ke) {
        boolean needDoubleList = isEDMI && isLoadProfile;
        List<Double> doubles = new ArrayList<>();
        if (needDoubleList) {
            try {
                doubles = getEDMILPScaleList();
//                System.out.println("addToCollection ::> "+doubles.size());
            } catch (Exception ex) {
                System.out.println("Gagal mengambil skala EDMI karena : " + ex.toString());
            }
        }
        try {
            read(generic, 3);
//            if (Trace.ordinal() > TraceLevel.WARNING.ordinal()) {
            int count = 0;
            for (Entry<GXDLMSObject, GXDLMSCaptureObject> col : generic
                    .getCaptureObjects()) {
                switch (col.getKey().getObjectType()) {
                    case DEMAND_REGISTER: {

                        GXDLMSDemandRegister demandRegister = new GXDLMSDemandRegister(col.getKey().getLogicalName());
                        demandRegister.setVersion(col.getKey().getVersion());
                        Object val = read(demandRegister, 4);
                        SkaladanUnit su = new SkaladanUnit(lihatNilai(4, val));
                        demandRegister.setScaler(getLPScale(needDoubleList, count, doubles, su.getSkala()));
                        demandRegister.setUnit(su.getUnit());
                        client.getObjects().add(demandRegister);
                        break;
                    }
                    case REGISTER: {
//                        	System.out.println(col.getKey().getLogicalName());
                        GXDLMSRegister register = new GXDLMSRegister(col.getKey().getLogicalName());
                        register.setVersion(col.getKey().getVersion());
                        Object val = read(register, 3);
                        SkaladanUnit su = new SkaladanUnit(lihatNilai(3, val));
//                            System.out.println(count +":"+ doubles.size());
                        register.setScaler(getLPScale(needDoubleList, count, doubles, su.getSkala()));
                        register.setUnit(su.getUnit());
                        client.getObjects().add(register);
                        break;
                    }
                    case EXTENDED_REGISTER: {
                        GXDLMSExtendedRegister extendedRegister
                                = new GXDLMSExtendedRegister(col.getKey().getLogicalName());
                        extendedRegister.setVersion(col.getKey().getVersion());
                        Object val = read(extendedRegister, 3);
                        SkaladanUnit su = new SkaladanUnit(lihatNilai(3, val));
                        extendedRegister.setScaler(getLPScale(needDoubleList, count, doubles, su.getSkala()));
                        extendedRegister.setUnit(su.getUnit());
                        client.getObjects().add(extendedRegister);
                        break;
                    }
                    case CLOCK:
                        GXDLMSClock clock = new GXDLMSClock(col.getKey().getLogicalName());
                        clock.setVersion(col.getKey().getVersion());
                        for (int i = 2; i < 10; i++) {
                            read(clock, i);
                        }
                        client.getObjects().add(clock);
                        break;
                    case DATA: {
                        GXDLMSData dt = new GXDLMSData(col.getKey().getLogicalName());
                        client.getObjects().add(dt);
                        break;
                    }
                    default: {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName(col.getKey().getLogicalName());
                        dt.setObjectType(col.getKey().getObjectType());
                        client.getObjects().add(dt);
                        break;
                    }
                }
                count++;
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error adding to collection " + e.toString());
            if (ke == 1 && e.getMessage().equals("Failed to receive reply from the device in given time.")) {
                try {
                    this.initializeConnection();
                    addToCollection(generic, isEDMI, isLoadProfile, 2);
                } catch (InterruptedException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }
    }

// // untuk gurux 2.2.28
//    List<Double> getEDMILPScaleList() throws Exception {
//        List<Double> doubles = new ArrayList<>();
//        GXDLMSData data = new GXDLMSData("0.0.136.0.1.255");
//        data.setVersion(0);
//        data.setObjectType(ObjectType.DATA);
//        read(data, 2);
//
////        System.out.println("getEDMILPScaleList-----<<<");
//        for (int pos2 = 0; pos2 != Array.getLength(data.getValue()); ++pos2) {
//            Object tmp = Array.get(data.getValue(), pos2);
////            System.out.println("Dalam getEDMILPScaleList-----<<<");
//            System.out.println(Array.get(data.getValue(), pos2));
//            for (int pos3 = 0; pos3 != Array.getLength(tmp); ++pos3) {
//                Object tmp2 = Array.get(tmp, pos3);
//                if (pos3 == 1) {
//                    doubles.add(Math.pow(10, Integer.parseInt(tmp2.toString())));
//                }
//            }
//        }
//        System.out.println("=====>"+doubles.size());
//        return doubles;
//    }
    // // Untuk gurux 3.1.16
    List<Double> getEDMILPScaleList() throws Exception {
        List<Double> doubles = new ArrayList<>();
        GXDLMSData data = new GXDLMSData("0.0.136.0.1.255");
        data.setVersion(0);
        data.setObjectType(ObjectType.DATA);
        read(data, 2);
        for (Object tmp : (List<?>) data.getValue()) {
            String tmp2 = "";
            if (tmp instanceof byte[]) {
                tmp2 = new String((byte[]) tmp);
            } else {
                tmp2 = String.valueOf(tmp);
            }
            String[] strings = tmp2.replace("[", "").replace("]", "").split(",");
            doubles.add(Math.pow(10, Integer.parseInt(strings[1].trim())));
        }
        return doubles;
    }

    double getLPScale(boolean needDoubleList, int count, List<Double> doubles, double skalaObject) {
        if (needDoubleList && (count - 2 >= 0)) {
            return doubles.get(count);
        } else {
            return skalaObject;
        }
    }

    public String lihatNilai(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = new String((byte[]) val);
        } else if (val instanceof Double) {
            NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            StringBuilder sb = new StringBuilder();
            for (Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        return String.valueOf(val);
    }

    // public String showValue(final int pos, final Object value) {
    //     Object val = value;
    //     if (val instanceof byte[]) {
    //         val = GXCommon.bytesToHex((byte[]) val);
    //     } else if (val instanceof Double) {
    //         NumberFormat formatter = NumberFormat.getNumberInstance();
    //         val = formatter.format(val);
    //     } else if (val instanceof List) {
    //         StringBuilder sb = new StringBuilder();
    //         for (Object tmp : (List<?>) val) {
    //             if (sb.length() != 0) {
    //                 sb.append(", ");
    //             }
    //             if (tmp instanceof byte[]) {
    //                 sb.append(GXCommon.bytesToHex((byte[]) tmp));
    //             } else {
    //                 sb.append(String.valueOf(tmp));
    //             }
    //         }
    //         val = sb.toString();
    //     } else if (val != null && val.getClass().isArray()) {
    //         StringBuilder sb = new StringBuilder();
    //         for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
    //             if (sb.length() != 0) {
    //                 sb.append(", ");
    //             }
    //             Object tmp = Array.get(val, pos2);
    //             if (tmp instanceof byte[]) {
    //                 sb.append(GXCommon.bytesToHex((byte[]) tmp));
    //             } else {
    //                 sb.append(String.valueOf(tmp));
    //             }
    //         }
    //         val = sb.toString();
    //     }
    //     return String.valueOf(val);
    // }
    public void addCollectionMeter(GXDLMSProfileGeneric generic, String noMeter) {
        log(Level.INFO, "add unit measurement, record unit and scale to collection");
        try {
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> col : generic
                    .getCaptureObjects()) {
                switch (col.getKey().getObjectType()) {
                    case DEMAND_REGISTER: {
                        GXDLMSDemandRegister demandRegister = new GXDLMSDemandRegister(
                                col.getKey().getLogicalName());
                        demandRegister.setVersion(col.getKey().getVersion());
                        demandRegister.setDescription(ObisDescriptor.getDeskripsi(
                                demandRegister.getLogicalName()));
                        Object val = readMeter(demandRegister, 4, noMeter);
                        SkaladanUnit su = new SkaladanUnit(Utils.lihatNilai(4, val));
                        demandRegister.setScaler(su.getSkala());
                        demandRegister.setUnit(su.getUnit());
                        client.getObjects().add(demandRegister);
                        log(Level.INFO, "add " + demandRegister.getLogicalName()
                                + "(" + demandRegister.getDescription() + ")"
                                + " version " + demandRegister.getVersion()
                                + " scaller " + demandRegister.getScaler()
                                + " unit " + demandRegister.getUnit()
                                + " as " + demandRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case REGISTER: {
                        GXDLMSRegister register = new GXDLMSRegister(col.getKey().getLogicalName());
                        register.setVersion(col.getKey().getVersion());
                        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
                        Object val = readMeter(register, 3, noMeter);
                        SkaladanUnit su = new SkaladanUnit(Utils.lihatNilai(3, val));
                        register.setScaler(su.getSkala());
                        register.setUnit(su.getUnit());
                        client.getObjects().add(register);
                        log(Level.INFO, "add " + register.getLogicalName()
                                + "(" + register.getDescription() + ")"
                                + " version " + register.getVersion()
                                + " scaller " + register.getScaler()
                                + " unit " + register.getUnit()
                                + " as " + register.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case EXTENDED_REGISTER: {
                        GXDLMSExtendedRegister extendedRegister
                                = new GXDLMSExtendedRegister(col.getKey().getLogicalName());
                        extendedRegister.setVersion(col.getKey().getVersion());
                        extendedRegister.setDescription(ObisDescriptor.getDeskripsi(
                                extendedRegister.getLogicalName()));
                        Object val = readMeter(extendedRegister, 3, noMeter);
                        SkaladanUnit su = new SkaladanUnit(Utils.lihatNilai(3, val));
                        extendedRegister.setScaler(su.getSkala());
                        extendedRegister.setUnit(su.getUnit());
                        client.getObjects().add(extendedRegister);
                        log(Level.INFO, "add " + extendedRegister.getLogicalName()
                                + "(" + extendedRegister.getDescription() + ")"
                                + " version " + extendedRegister.getVersion()
                                + " scaller " + extendedRegister.getScaler()
                                + " unit " + extendedRegister.getUnit()
                                + " as " + extendedRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case CLOCK:
                        GXDLMSClock clock = new GXDLMSClock(col.getKey().getLogicalName());
                        clock.setDescription(ObisDescriptor.getDeskripsi(clock.getLogicalName()));
                        clock.setVersion(col.getKey().getVersion());
                        for (int i = 2; i < 10; i++) {
                            readMeter(clock, i, noMeter);
                        }
                        client.getObjects().add(clock);
                        log(Level.INFO, "add " + clock.getLogicalName()
                                + "(" + clock.getDescription() + ")"
                                + " version " + clock.getVersion()
                                + " as " + clock.getObjectType()
                                + " to collection ");
                        break;
                    case DATA: {
                        GXDLMSData dt = new GXDLMSData(col.getKey().getLogicalName());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        client.getObjects().add(dt);
                        log(Level.INFO, "add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                    default: {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName(col.getKey().getLogicalName());
                        dt.setObjectType(col.getKey().getObjectType());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        client.getObjects().add(dt);
                        log(Level.INFO, "add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log(Level.SEVERE, "Error adding to collection " + e.toString());
        }
    }

    public void printProfileGenericBuffer(GXDLMSProfileGeneric pg) {
        Object[] cells = pg.getBuffer();
        for (Object rows : cells) {
            log(Level.INFO, "---------------------------------------------------------------------------");
            int a = 0;
            for (Object cell : (Object[]) rows) {
                String output = pg.getCaptureObjects().get(a).getKey().getDescription() + "("
                        + pg.getCaptureObjects().get(a).getKey().getLogicalName() + ") = ";
                String hasil;
                if (cell instanceof byte[]) {
                    hasil = new String((byte[]) cell);
                } else {
                    hasil = cell.toString();
                }
                if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                    if (PhysicalUnit.getPhysicalUnit(
                            ((GXDLMSRegister) client.getObjects().findByLN(ObjectType.REGISTER,
                                    pg.getCaptureObjects().get(a).getKey().
                                            getLogicalName())).getUnit()).getSatuan().equals(
                                    PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                        output += PhysicalUnit.secondToDuration(Integer.parseInt(hasil));
                    } else {
                        output += hasil + " " + PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) client.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan();
                    }
                } else {
                    output += hasil;
                }
                log(Level.INFO, output);
                a++;
            }
        }
    }

    void printClientInfo() {
        String x = String.valueOf((char) 215);
        log(Level.INFO, "Use logical name referencing = " + client.getUseLogicalNameReferencing());
        log(Level.INFO, "Authentication = " + client.getAuthentication().toString());
        log(Level.INFO, "Client address = 0" + x + Integer.toHexString(client.getClientAddress()).toUpperCase()
                + "(" + client.getClientAddress() + ")");
        log(Level.INFO, "Interface type = " + client.getInterfaceType().toString());
        log(Level.INFO, "Server address = 0" + x
                + Integer.toHexString(client.getServerAddress()).toUpperCase()
                + "(" + client.getServerAddress() + ")");
        if (client.getAuthentication().equals(Authentication.LOW)) {
            log(Level.INFO, "Password = " + client.getPassword() + "("
                    + Utils.arrayByteToString(client.getPassword()) + ")");
        }
        if (client.getAuthentication().equals(Authentication.HIGH_GMAC)) {
            log(Level.INFO, "Security = " + client.getCiphering().getSecurity().toString());
            log(Level.INFO, "System title = " + GXCommon.bytesToHex(client.getCiphering().getSystemTitle())
                    + "(" + Utils.arrayByteToString(client.getCiphering().getSystemTitle()) + ")");
            log(Level.INFO, "Block cipher key = " + GXCommon.bytesToHex(client.getCiphering().getBlockCipherKey())
                    + "(" + Utils.arrayByteToString(client.getCiphering().getBlockCipherKey()) + ")");
            log(Level.INFO, "Authentication key = " + GXCommon.bytesToHex(
                    client.getCiphering().getAuthenticationKey()) + "("
                    + Utils.arrayByteToString(client.getCiphering().getAuthenticationKey()) + ")");
            if (client.getCiphering().getInvocationCounter() != 0) {
                log(Level.INFO, "Invocation counter = " + client.getCiphering().getInvocationCounter());
            }
        }
    }

    public void printActivityCalendar(GXDLMSActivityCalendar activityCalendar) {
        LOGGER.log(Level.INFO, "activate_passive_calendar_time = " + activityCalendar.getTime());
        LOGGER.log(Level.INFO, "Calendar_name_active = " + activityCalendar.getCalendarNameActive());
        LOGGER.log(Level.INFO, "Season_profile_active list = ");
        LOGGER.log(Level.INFO, "\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
        for (GXDLMSSeasonProfile seasonProfile : activityCalendar.getSeasonProfileActive()) {
            LOGGER.log(Level.INFO, "\t" + new String(seasonProfile.getName())
                    + "\t" + seasonProfile.getStart().toString()
                    + "\t" + new String(seasonProfile.getWeekName()));
        }
        LOGGER.log(Level.INFO, "Week_profile_table_active list = ");
        LOGGER.log(Level.INFO, "\t" + "Name" + "\t" + "Monday" + "\t" + "Tuesday" + "\t"
                + "Wednesday" + "\t" + "Thursday" + "\t" + "Friday" + "\t" + "Saturday"
                + "\t" + "Monday");
        for (GXDLMSWeekProfile weekProfile : activityCalendar.getWeekProfileTableActive()) {
            LOGGER.log(Level.INFO, "\t" + new String(weekProfile.getName())
                    + "\t" + weekProfile.getMonday() + "\t" + weekProfile.getTuesday()
                    + "\t" + weekProfile.getWednesday() + "\t" + weekProfile.getThursday()
                    + "\t" + weekProfile.getFriday() + "\t" + weekProfile.getSaturday()
                    + "\t" + weekProfile.getMonday());
        }
        LOGGER.log(Level.INFO, "Day_profile_table_active list = ");
        for (GXDLMSDayProfile dayProfile : activityCalendar.getDayProfileTableActive()) {
            LOGGER.log(Level.INFO, "\t" + "Day ID = " + dayProfile.getDayId());
            for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
                LOGGER.log(Level.INFO, "\t\t" + "Start Time = " + profileAction.getStartTime());
                LOGGER.log(Level.INFO, "\t\t\t" + "Script = " + profileAction.getScriptLogicalName());
                LOGGER.log(Level.INFO, "\t\t\t" + "Selector = " + profileAction.getScriptSelector());
            }
        }
        LOGGER.log(Level.INFO, "Calendar_name_passive = " + activityCalendar.getCalendarNamePassive());
        LOGGER.log(Level.INFO, "Season_profile_passive list = ");
        LOGGER.log(Level.INFO, "\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
        for (GXDLMSSeasonProfile seasonProfile : activityCalendar.getSeasonProfilePassive()) {
            LOGGER.log(Level.INFO, "\t" + new String(seasonProfile.getName())
                    + "\t" + seasonProfile.getStart().toString()
                    + "\t" + new String(seasonProfile.getWeekName()));
        }
        LOGGER.log(Level.INFO, "Week_profile_table_passive list = ");
        LOGGER.log(Level.INFO, "\t" + "Name" + "\t" + "Monday" + "\t" + "Tuesday" + "\t"
                + "Wednesday" + "\t" + "Thursday" + "\t" + "Friday" + "\t" + "Saturday"
                + "\t" + "Monday");
        for (GXDLMSWeekProfile weekProfile : activityCalendar.getWeekProfileTablePassive()) {
            LOGGER.log(Level.INFO, "\t" + new String(weekProfile.getName())
                    + "\t" + weekProfile.getMonday() + "\t" + weekProfile.getTuesday()
                    + "\t" + weekProfile.getWednesday() + "\t" + weekProfile.getThursday()
                    + "\t" + weekProfile.getFriday() + "\t" + weekProfile.getSaturday()
                    + "\t" + weekProfile.getMonday());
        }
        LOGGER.log(Level.INFO, "Day_profile_table_passive list = ");
        for (GXDLMSDayProfile dayProfile : activityCalendar.getDayProfileTablePassive()) {
            LOGGER.log(Level.INFO, "\t" + "Day ID = " + dayProfile.getDayId());
            for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
                LOGGER.log(Level.INFO, "\t\t" + "Start Time = " + profileAction.getStartTime());
                LOGGER.log(Level.INFO, "\t\t\t" + "Script = " + profileAction.getScriptLogicalName());
                LOGGER.log(Level.INFO, "\t\t\t" + "Selector = " + profileAction.getScriptSelector());
            }
        }
    }
    
    /**
     * Fungsi untuk mencetak informasi object register.
     *
     * @param register GXDLMSRegister yang akan dicetak informasinya.
     */
    public void printRegister(GXDLMSRegister register) {
        try {
            LOGGER.log(Level.INFO,"Value = " + register.getValue());
            LOGGER.log(Level.INFO,"Skala = " + register.getScaler());
            LOGGER.log(Level.INFO,"Unit = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getUnit().toString()
                    + "(" + PhysicalUnit.getPhysicalUnit(register.getUnit()).getUkuran() + ")");
            LOGGER.log(Level.INFO,"Satuan = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSatuan());
            LOGGER.log(Level.INFO,"Nama satuan = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getNamaSatuan());
            LOGGER.log(Level.INFO,"SI definition = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSIDefinition());
            LOGGER.log(Level.INFO,"---------------------------------------------------------------------------");
            LOGGER.log(Level.INFO,register.getDescription() + " = " + register.getValue() + " "
                    + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSatuan());
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Cetak value error @" + e.toString());
        }
    }
    
    /**
     * Fungsi untuk mencetak informasi object script table.
     *
     * @param scriptTable GXDLMSScriptTable yang akan dicetak informasinya.
     */
    public void printScriptTable(GXDLMSScriptTable scriptTable) {
        try {
            LOGGER.log(Level.INFO,"Script = ");
            for (GXDLMSScript script : scriptTable.getScripts()) {
                LOGGER.log(Level.INFO,"\t" + script.getId() + "{");
                for (GXDLMSScriptAction action : script.getActions()) {
                    LOGGER.log(Level.INFO,"\t\t{\n\t\t Target = " + action.getTarget().getLogicalName()
                            + "(" + ObisDescriptor.getDeskripsi(action.getTarget().getLogicalName()) + "),");
                    LOGGER.log(Level.INFO,"\t\t Index = " + action.getIndex() + ",");
                    LOGGER.log(Level.INFO,"\t\t Action type = " + action.getType());
                    LOGGER.log(Level.INFO,"\t\t Parameter = " + action.getParameter().toString() + ",");
                    LOGGER.log(Level.INFO,"\t\t Parameter type = " + action.getParameterType() + ",");
                    LOGGER.log(Level.INFO,"\t\t}");
                }
                LOGGER.log(Level.INFO,"\t}");
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Cetak value error @" + e.toString());
        }
    }
    
    public void printActionSchedule(GXDLMSActionSchedule actionSchedule) {
        int tglAsliBlling = 0;
        LOGGER.log(Level.INFO, "Executed script logical name = " + actionSchedule.getTarget().getLogicalName()
                + " (" + ObisDescriptor.getDeskripsi(actionSchedule.getTarget().getLogicalName()) + ")");
        LOGGER.log(Level.INFO, "Executed script selector = " + actionSchedule.getExecutedScriptSelector());
        LOGGER.log(Level.INFO, "Script type = " + actionSchedule.getType().getValue());
        GXDateTime executionTime = actionSchedule.getExecutionTime()[0];
        Calendar calendar = executionTime.getLocalCalendar();
        tglAsliBlling = calendar.get(Calendar.DATE);
        String waktuRekamBillings = calendar.get(Calendar.DATE) + "-" + calendar.get(Calendar.DATE)
                + "-" + calendar.get(Calendar.YEAR) + " ";
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            waktuRekamBillings += calendar.get(Calendar.HOUR_OF_DAY) + " :";
        } else {
            waktuRekamBillings += calendar.get(Calendar.HOUR_OF_DAY) + ":";
        }
        if (calendar.get(Calendar.MINUTE) < 10) {
            waktuRekamBillings += calendar.get(Calendar.MINUTE);
        } else {
            waktuRekamBillings += calendar.get(Calendar.MINUTE);
        }
        LOGGER.log(Level.INFO, "Execution time = " + waktuRekamBillings);
        LOGGER.log(Level.INFO, "Day of week = " + executionTime.getDayOfWeek());
        LOGGER.log(Level.INFO, "Deviation = " + executionTime.getDeviation());
        for (DateTimeExtraInfo dateTimeExtraInfo : executionTime.getExtra()) {
            LOGGER.log(Level.INFO, "dateTimeExtraInfo = " + dateTimeExtraInfo.getValue());
        }
        for (DateTimeSkips dateTimeSkips : executionTime.getSkip()) {
            LOGGER.log(Level.INFO, "dateTimeSkips = " + dateTimeSkips.toString());
        }
        for (ClockStatus status : executionTime.getStatus()) {
            LOGGER.log(Level.INFO, "Status = " + status.toString());
        }
        LOGGER.log(Level.INFO, "Meter Calendar = " + executionTime.getMeterCalendar().getTime().toString());
        LOGGER.log(Level.INFO, "Local Calendar = " + executionTime.getLocalCalendar().getTime().toString());
    }
}
