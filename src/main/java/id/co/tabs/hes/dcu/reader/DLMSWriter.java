package id.co.tabs.hes.dcu.reader;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import gurux.dlms.GXDateTime;
import gurux.dlms.GXTime;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import id.co.tabs.hes.DayProfile;
import id.co.tabs.hes.DayProfileAction;
import id.co.tabs.hes.DayProfileList;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.dlms.DLMS;
import id.co.tabs.hes.util.ObisDescriptor;

public class DLMSWriter {
	public static Charset encodingType = StandardCharsets.UTF_8;
    public static void _DLMSwriteMeter(final DLMS r, final String namaField, final String UID, final Params p,
            final String data) throws Exception {
        try {
            final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            final BasicDBObject query = new BasicDBObject();
            String jenisPhasa = "FASE1";
            if (p.getPhasa() == 1) {
                jenisPhasa = "FASE1";
            } else {
                jenisPhasa = "FASE3";
            }
            query.put(jenisPhasa, "Y");
            query.put("NAMA_FIELD", namaField);
            final MongoDB mg = new MongoDB();
            final List<DBObject> cursors = mg.getData("DATA_OBIS", query, null);
            if (cursors.size() == 0) {
                throw new NullPointerException("Obis tidak ditemukan");
            }
            String obis = "";
            String type = "";
            String column = "";
            String deskripsi = "";
            for (final DBObject csr : cursors) {
                obis = csr.get("LOGICAL_NAME").toString();
                type = csr.get("CLASS_ID").toString();
                column = csr.get("NAMA_FIELD").toString();
                deskripsi = csr.get("DESKRIPSI").toString();

                pg.setLogicalName(obis);
                pg.setObjectType(r.getObjectType(type));
                pg.setDescription(deskripsi);
            }
            Object data_ob = null;
            DataType dataType = null;
            if (type.equals("8")) {
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                final java.util.Calendar tgl = java.util.Calendar.getInstance();
                tgl.setTime(sdf.parse(data));
                // System.out.println("Tanggal "+data+ " :: "+tgl.getTime());
                data_ob = tgl.getTime();
                dataType = DataType.OCTET_STRING;
            } else if (type.equals("1")) {
                data_ob = data;
                dataType = DataType.STRING;
                // dataType = DataType.OCTET_STRING;
            } else if (type.equals("3")) {
                data_ob = data;
                dataType = DataType.UINT32;
            }
            if (data_ob != null) {
                r.readDLMSPacket(r.getClient().write(obis, data_ob, dataType, r.getObjectType(type), 2));
            }
        } finally {
            r.close();
        }
    }
    private static final Logger log = Logger.getLogger(DLMSReader.class.getName());

	public static void writeMeter(final DLMS r, String namaField, String UID, Params p, String data) throws Exception {
//		if (r.getClient().getInterfaceType() == InterfaceType.WRAPPER) {
//			r.replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
//		} else {
//			r.replyBuff = java.nio.ByteBuffer.allocate(100);
//		}
		try {
			GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
			BasicDBObject query = new BasicDBObject();
			String jenisPhasa = "FASE1";
			if (p.getPhasa() == 1) {
				jenisPhasa = "FASE1";
			} else {
				jenisPhasa = "FASE3";
			}
			query.put(jenisPhasa, "Y");
			query.put("NAMA_FIELD", namaField);

			if(p.gettypeObis().equals("SPLN")){
				DBObject clause1 = new BasicDBObject("TYPE_OBIS", "SPLN");
				DBObject clause2 = new BasicDBObject("TYPE_OBIS", "");
				DBObject clause3 = new BasicDBObject("TYPE_OBIS", null);
				DBObject clause4 = new BasicDBObject("TYPE_OBIS", new BasicDBObject("$exists", false));
				BasicDBList or = new BasicDBList();
				or.add(clause1);
				or.add(clause2);
				or.add(clause3);
				or.add(clause4);
				query.put("$or", or);
			}else{
				query.put("TYPE_OBIS", p.gettypeObis());
			}

			List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, null);
			if (cursors.size() == 0) {
				throw new NullPointerException("Obis tidak ditemukan");
			}
			String obis = "";
			String type = "";
			String column = "";
			String deskripsi = "";
			String version = "";
			for (DBObject csr : cursors) {
				obis = csr.get("LOGICAL_NAME").toString();
				type = csr.get("CLASS_ID").toString();
				column = csr.get("NAMA_FIELD").toString();
				deskripsi = csr.get("DESKRIPSI").toString();
				version = csr.get("VERSION").toString();

				pg.setLogicalName(obis);
				pg.setObjectType(r.getObjectType(type));
				pg.setDescription(deskripsi);
			}
			GXObisCode skipItem = new GXObisCode();
			skipItem.setObjectType(r.getObjectType(type));
			skipItem.setLogicalName(obis);
			skipItem.setVersion(Integer.parseInt(version));
			skipItem.setDescription(obis);
			GXDLMSObject obj = r.objFromCode(skipItem);
			int countWrite = 0;
			DataType dataType = null;
			List<Integer> listIndex = new ArrayList<Integer>();
			for (int pos = 1; pos<=obj.getAttributeCount();pos++) {
//				System.out.println("index "+pos+ " ==> "+obj.getUIDataType(pos) +" -> "+ obj.getDataType(pos)+" ("+obj.getAccess(pos).toString()+")");
				if(obj.getAccess(pos).toString().contains("WRITE")){
//					System.out.println(pos);
					listIndex.add(pos);
				}
			}
			if(listIndex.size()>0){
			Object data_ob = null;
			java.util.Calendar tgl = java.util.Calendar.getInstance();
			int idx = listIndex.get(0);

			dataType = obj.getDataType(idx);
			if(dataType==DataType.NONE){
			switch(namaField) {
			  case "LOAD_PROFILE_INTERVAL":
			    // code block
				  dataType = DataType.UINT8;
			    break;
			  case "ID_PELANGGAN":
			    // code block
				  dataType = DataType.OCTET_STRING;
			    break;
			  case "KD_UNIT_PLN":
				    // code block
					  dataType = DataType.OCTET_STRING;
				    break;
			  case "TGL_BILL_SETTING":
				    // code block
					  dataType = DataType.DATETIME;
				    break;
			  default:
			    // code block
			}
			}

			if(dataType == DataType.DATETIME){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
				tgl.setTime(sdf.parse(data));
				data_ob = tgl.getTime();
			}else if (namaField.equals("TGL_BILL_SETTING")){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
				tgl.setTime(sdf.parse(data));
				data_ob = tgl.getTime();
			}else{
				if(dataType == DataType.INT16 ||
						dataType == DataType.INT32 ||
								dataType == DataType.INT64||
										dataType == DataType.UINT16 ||
												dataType == DataType.UINT32 ||
														dataType == DataType.UINT64 ||
																dataType == DataType.UINT8)
				{
					data_ob = Integer.parseInt(data);
				}else if(dataType == DataType.FLOAT32 ||
						dataType == DataType.FLOAT64)
				{
					data_ob = Float.parseFloat(data);
				}else if(dataType == DataType.STRUCTURE)
				{
					Object[] datas = {data};
					data_ob = datas;
				}else if(dataType == DataType.OCTET_STRING)
				{
					if(r.getObjectType(type)==ObjectType.CLOCK){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
						tgl.setTime(sdf.parse(data));
						data_ob = tgl.getTime();
					}
					else
						data_ob = textToHex(data.toString());
				}else{
					data_ob = data;
				}
			}


//			if(obj.getDataType(idx).equals(DataType.NONE)){
//				dataType = DataType.UINT32;
//				data_ob = Integer.parseInt(data_ob.toString());
//			}else{

//			}
//			if (type.equals("8")) {
//				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//				tgl.setTime(sdf.parse(data));
////				System.out.println("Tanggal "+data+ " :: "+tgl.getTime());
//				data_ob = tgl.getTime();
//				dataType = DataType.OCTET_STRING;
//			} else if (type.equals("1")) {
//				data_ob = data;
//				dataType = DataType.STRING;
////				dataType = DataType.OCTET_STRING;
//			} else if (type.equals("3")) {
//				data_ob = Integer.parseInt(data);
//				dataType = DataType.UINT32;
//			}
//			System.out.println("OBIS \t"+obis);
//			System.out.println("DATA \t"+data_ob);
//			System.out.println("DATA TYPE \t"+dataType);
//			System.out.println("OBJECT TYPE \t"+r.getObjectType(type));
//			System.out.println("INDEX \t"+idx);
			if (data_ob != null) {
//				r.readDLMSPacket(r.dlms.write("1.0.0.8.4.255", 60, DataType.UINT8, ObjectType.REGISTER, 2));
				r.readDLMSPacket(r.getClient().write(obis, data_ob, dataType, r.getObjectType(type), idx));
			}
			}
		} finally {
			r.close();
		}
	}

	public static void writeMeterActiveCallendar(DLMS r, String namaField, String UID, Params p, String dates,
			DayProfileList dayProfilex) throws Exception {
//		if (r.dlms.getInterfaceType() == InterfaceType.WRAPPER) {
//			r.replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
//		} else {
//			r.replyBuff = java.nio.ByteBuffer.allocate(100);
//		}
		java.util.Calendar tgl = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		GXDateTime tanggal = new GXDateTime();
		tgl.setTime(sdf.parse(dates));
		tanggal.setValue(tgl.getTime());
//		try {
			GXDLMSActivityCalendar gxdlmsac = new GXDLMSActivityCalendar("0.0.13.0.0.255");
            gxdlmsac.setVersion(0);
            gxdlmsac.setDescription("Activity Calendar");
            r.read(gxdlmsac, 9);
            GXDLMSDayProfile[] dayProfiles = gxdlmsac.getDayProfileTablePassive();
            GXTime gXTime = new GXTime(tanggal.getValue().getHours(),tanggal.getValue().getMinutes(),tanggal.getValue().getSeconds(),0);
//            if(jenis.equals("tanggal")){
            	gXTime = new GXTime(tanggal);
//            }
//            GXDLMSDayProfileAction[] actions = {new GXDLMSDayProfileAction(gXTime, "0.0.10.0.100.255", 1)};
            GXDLMSDayProfile[] dayProfilesw = new GXDLMSDayProfile[dayProfilex.getListDayProfile().size()];
            int i = 0;
            for(DayProfile dp : dayProfilex.getListDayProfile()){

	            List<GXDLMSDayProfileAction> actions = new ArrayList<GXDLMSDayProfileAction>();
	            for(DayProfileAction dpa : dp.getItems()){
	            	String tanggalx = "";
	            	if(dpa.getSTART_TIME().length()<=8){
	            		sdf = new SimpleDateFormat("HH:mm:ss");
	            	}
	            	tgl.setTime(sdf.parse(dpa.getSTART_TIME()));
	        		tanggal.setValue(tgl.getTime());
	            	gXTime = new GXTime(tanggal);
	            	actions.add(new GXDLMSDayProfileAction(gXTime,dpa.getSCRIPT().equals("")?"0.0.10.0.100.255":dpa.getSCRIPT(), Integer.parseInt(dpa.getSELECTOR())));
	            }
	            GXDLMSDayProfile dayProfile = new GXDLMSDayProfile(dp.getDAY_ID(), actions.toArray(new GXDLMSDayProfileAction[0]));


//            for (int i = 0; i < dayProfilesw.length; i++) {
            	dayProfilesw[i] = dayProfile;
            	i++;
//            }
            }
            //            GXDLMSDayProfile[] dayProfilesw = new GXDLMSDayProfile[dayProfiles.length + 1];
//            for (int i = 0; i < dayProfilesw.length; i++) {
//                if (i < dayProfiles.length) {
//                    dayProfilesw[i] = dayProfiles[i];
//                }else{
//                    dayProfilesw[i] = dayProfile;
//                }
//            }
            gxdlmsac.setDayProfileTablePassive(dayProfilesw);
            r.writeObject(gxdlmsac, 9);

            //Pasive to Active Callendar
            setActivatePassiveCalendarDate(r, tgl);
//		} finally {
//			r.close();
//		}
	}

	public static void writeMeterLimmiter(DLMS r, String namaField, String UID, Params p,
			String s_originActivationTime, int duration,
			long minOverThresholdDuration, long minUnderThresholdDuration, long emergencythreshold,
			long nemergencythreshold, long aemergencythreshold ) throws Exception {

//		if (r.getClient().getInterfaceType() == InterfaceType.WRAPPER) {
//			r.replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
//		} else {
//			r.replyBuff = java.nio.ByteBuffer.allocate(100);
//		}
		java.util.Calendar tgl = java.util.Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		if(s_originActivationTime!=null && !s_originActivationTime.equals(""))
			tgl.setTime(sdf.parse(s_originActivationTime));

		GXDateTime originActivationTime=new GXDateTime();
		originActivationTime.setValue(tgl.getTime());
//		try {
//            GXDLMSRegister register = new GXDLMSRegister("1.0.32.7.0.255");
//            register.setVersion(0);
//            register.setDescription("Tegangan Instant");
//            r.dlms.getObjects().add(register);
            GXDLMSLimiter limiter = new GXDLMSLimiter("0.0.17.0.0.255");
            limiter.setVersion(0);
            limiter.setDescription("Limiter");

            // 3. threshold_active
            if(aemergencythreshold>-1){
            	try{
	            	System.out.println("Write Active Emergency threshold : "+aemergencythreshold);
	//            	limiter.setThresholdEmergency(aemergencythreshold);
	//            	r.writeObject(limiter, 3);
	//            	long val = Long.parseLong("20");
	//                r.readDLMSPacket(r.dlms.write(limiter.getLogicalName(), val,
	//                        DataType.UINT32, ObjectType.LIMITER, 3));
	            	r.readDLMSPacket(r.getClient().write("0.0.17.0.0.255", (long)aemergencythreshold, DataType.UINT32, ObjectType.LIMITER, 3));
            	}catch(Exception e){
           		log.log(Level.SEVERE, "Write Active Emergency threshold : "+e.getMessage());
            	}

            }

            // 4. threshold_normal
            if(nemergencythreshold>-1){
            	try{
	            	System.out.println("Write Normal Emergency threshold : "+nemergencythreshold);
	//            	limiter.setThresholdEmergency(nemergencythreshold);
	            	r.readDLMSPacket(r.getClient().write("0.0.17.0.0.255", (long)nemergencythreshold, DataType.UINT32, ObjectType.LIMITER, 4));
	//            	r.writeObject(limiter, 4);
            	}catch(Exception e){
                    log.log(Level.SEVERE,"Write Normal Emergency threshold : "+e.getMessage());
            	}
            }

            // 5. threshold_emergency
            if(emergencythreshold>-1){
            	try{
            	System.out.println("Write Emergency threshold : "+emergencythreshold);
            	limiter.setThresholdEmergency(emergencythreshold);
            	r.readDLMSPacket(r.getClient().write("0.0.17.0.0.255", (long)emergencythreshold, DataType.UINT32, ObjectType.LIMITER, 5));
//            	r.writeObject(limiter, 5);
            	}catch(Exception e){
                    log.log(Level.SEVERE,"Write Emergency threshold : "+e.getMessage());
            	}
            }

            // 6 min_over_ threshold_duration
            if(minOverThresholdDuration>-1){
            	try{
            	System.out.println("Write Min Over threshold Duration : "+minOverThresholdDuration);
            	limiter.setMinOverThresholdDuration(minOverThresholdDuration);
            	r.writeObject(limiter, 6);
            	}catch(Exception e){
                    log.log(Level.SEVERE,"Write Min Over threshold Duration : "+e.getMessage());
            	}
            }

            // 7 min_under_threshold_duration
            if(minUnderThresholdDuration>-1){
            	try{
            	System.out.println("Write Min Under threshold Duration : "+minUnderThresholdDuration);
            	limiter.setMinUnderThresholdDuration(minUnderThresholdDuration);
            	r.writeObject(limiter, 7);
            	}catch(Exception e){
                    log.log(Level.SEVERE,"Write Min Under threshold Duration : "+e.getMessage());
            	}
            }
             // 8 Emergency Profile
            if(duration>-1){
            	try{
            	System.out.println("Write Duration : "+duration);
            	System.out.println("Activation Time : "+originActivationTime);
            	limiter.getEmergencyProfile().setDuration(duration);
            	limiter.getEmergencyProfile().setActivationTime(originActivationTime);
            	r.writeObject(limiter, 8);
            	}catch(Exception e){
                    log.log(Level.SEVERE,"Write Emergency Profile : "+e.getMessage());
            	}
            }


//		} finally {
//			r.close();
//		}
	}

	/**
     * Menambahkan COSEM object ke capture object load profile meter SMI.
     *
     * @param logicalName Logical name dari COSEM object yang akan ditambahkan.
     * @param objectType Object type dari COSEM object yang akan ditambahkan.
     * @param version Versi dari COSEM object yang akan ditambahkan.
     * @param attributeIndex Attribute index (tempat menyimpan nilai) dari COSEM
     * object yang akan ditambahkan.
     * @param dataIndex Data index dari COSEM object yang akan ditambahkan.
	 * @throws Exception
     */
    public void addLPCaptureObject(
    		DLMS r,
    		Params p,
    		String jenis,
    		String namaField,
    		String logicalName,
    		ObjectType objectType,
    		int version,
    		int attributeIndex,
            int dataIndex) throws Exception {

    	GXDLMSProfileGeneric loadProfile = new GXDLMSProfileGeneric();
    	loadProfile.setLogicalName("1.0.99.1.0.255");
    	loadProfile.setObjectType(ObjectType.PROFILE_GENERIC);
    	loadProfile.setDescription("LoadProfile");

    	BasicDBObject query = new BasicDBObject();
		String jenisPhasa = "FASE1";
		if (p.getPhasa() == 1) {
			jenisPhasa = "FASE1";
		} else {
			jenisPhasa = "FASE3";
		}


		query.put(jenisPhasa, "Y");
		query.put("CLASS_ID", 7);
		String _jenis = "GENERIC";
		if (jenis.toUpperCase().equals("PROFILE")) {
			_jenis = "GENERIC";
			query.put("NAMA_FIELD", namaField);
		} else if (jenis.toUpperCase().equals("BILLING")) {
			_jenis = "GENERIC";
			query.put("NAMA_FIELD", namaField);
		} else {
			_jenis = jenis;
			query.put("NAMA_FIELD", namaField);
		}

		query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));
		List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, _jenis);
		if (cursors.size() == 0) {
			throw new NullPointerException("Obis tidak ditemukan");
		}

		DBObject csr = cursors.get(0);
		String obis = csr.get("LOGICAL_NAME").toString();
		String type = csr.get("CLASS_ID").toString();
		String deskripsi = csr.get("DESKRIPSI").toString();

		loadProfile.setLogicalName(obis);
		loadProfile.setObjectType(r.getObjectType(type));
		loadProfile.setDescription(deskripsi);

//        if (Meter.meterType.equals(MeterType.SMI_1_Phase)) {
            try {
                r.read(loadProfile, 3);
                int sizeAwal = loadProfile.getCaptureObjects().size();
                for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : loadProfile.getCaptureObjects()) {
                    if (captureObject.getKey().getLogicalName().equals(logicalName)) {
//                        log.info("Object {0}({1}) sudah ada dalam capture object",
//                                new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
//                        System.out.println("===========================================================================");
                        return;
                    }
                }
                GXDLMSObject object = new GXDLMSObject();
                object.setLogicalName(logicalName);
                object.setDescription(ObisDescriptor.getDeskripsi(logicalName));
                object.setObjectType(objectType);
                object.setVersion(version);
                loadProfile.addCaptureObject(object, attributeIndex, dataIndex);
                r.writeObject(loadProfile, 3);
                r.read(loadProfile, 3);
                int sizeAkhir = loadProfile.getCaptureObjects().size();
                if (sizeAkhir > sizeAwal) {
                	DLMSAction.resetMeter(r);
//                    log.info( "adding {0}({1}) version {2} with attribute index {3} and "
//                            + "data index {4} object into captured object success",
//                            new Object[]{object.getLogicalName(), object.getDescription(), object.getVersion(),
//                                attributeIndex, dataIndex});
                    System.out.println("===========================================================================");
                } else {
//                    log.info("Gagal menambah capture object");
                    System.out.println("===========================================================================");
                }
            } catch (Exception ex) {
//                log.error( "Gagal membaca capture object cause {0}", ex.toString());
//                System.out.println("===========================================================================");
                throw new Exception("Gagal membaca capture object cause {0}"+ex.toString());
            }
//        } else {
//            log.info("Bukan meter SMI");
//            System.out.println("===========================================================================");
//        }
    }

    /**
     * Menghapus COSEM object dari capture object load profile meter SMI.
     *
     * @param logicalName Logical name dari COSEM object yang akan dihapus.
     * @throws Exception
     */
    public void removeCaptureObject(DLMS r, Params p,String jenis, String namaField, String logicalName) throws Exception {
    	GXDLMSProfileGeneric loadProfile = new GXDLMSProfileGeneric();
    	loadProfile.setLogicalName("1.0.99.1.0.255");
    	loadProfile.setObjectType(ObjectType.PROFILE_GENERIC);
    	loadProfile.setDescription("LoadProfile");

    	BasicDBObject query = new BasicDBObject();
		String jenisPhasa = "FASE1";
		if (p.getPhasa() == 1) {
			jenisPhasa = "FASE1";
		} else {
			jenisPhasa = "FASE3";
		}

		query.put(jenisPhasa, "Y");
		query.put("CLASS_ID", 7);
		String _jenis = "GENERIC";
		if (jenis.toUpperCase().equals("PROFILE")) {
			_jenis = "GENERIC";
			query.put("NAMA_FIELD", namaField);
		} else if (jenis.toUpperCase().equals("BILLING")) {
			_jenis = "GENERIC";
			query.put("NAMA_FIELD", namaField);
		} else {
			_jenis = jenis;
			query.put("NAMA_FIELD", namaField);
		}

		query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));
		List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, _jenis);
		if (cursors.size() == 0) {
			throw new NullPointerException("Obis tidak ditemukan");
		}

		DBObject csr = cursors.get(0);
		String obis = csr.get("LOGICAL_NAME").toString();
		String type = csr.get("CLASS_ID").toString();
		String deskripsi = csr.get("DESKRIPSI").toString();

		loadProfile.setLogicalName(obis);
		loadProfile.setObjectType(r.getObjectType(type));
		loadProfile.setDescription(deskripsi);

//        if (Meter.meterType.equals(MeterType.SMI_1_Phase)) {
//            GXDLMSProfileGeneric loadProfile = Meter.getLoadProfileObject();
            try {
                r.read(loadProfile, 3);
                boolean ada = false;
                int indexObject = 0;
                for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : loadProfile.getCaptureObjects()) {
                    if (captureObject.getKey().getLogicalName().equals(logicalName)) {
//                        log.info("Object {0}({1}) will be deleted",
//                                new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
                        indexObject = loadProfile.getCaptureObjects().indexOf(captureObject);
                        ada = true;
                        break;
                    }
                }
                if (ada) {
                    loadProfile.getCaptureObjects().remove(indexObject);
                    r.writeObject(loadProfile, 3);
                    DLMSAction.resetMeter(r);
//                    log.info("deleting {0}({1}) from captured object success",
//                            new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
//                    System.out.println("===========================================================================");
                } else {
//                    log.info("Object {0}({1}) tidak ada di captured object",
//                            new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
//                    System.out.println("===========================================================================");
                }
            } catch (Exception ex) {
//                log.error(ex.toString());
//                System.out.println("===========================================================================");
                throw new Exception(ex.toString());
            }
//        } else {
//            log.info("Bukan meter SMI");
//            System.out.println("===========================================================================");
//        }
    }

    /**
     * Mereset buffer dari load profile SMI.
     * @throws Exception
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
//    void resetLPSMI(GXDLMSReader r) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, Exception {
////        try {
//        	log.info("Resetting meter load profile buffer");
//            GXDLMSScriptTable scriptTable = new GXDLMSScriptTable("0.0.10.0.0.255");
//            scriptTable.setVersion(0);
//            scriptTable.setDescription(ObisDescriptor.getDeskripsi("0.0.10.0.0.255"));
//            r.readDLMSPacket(scriptTable.execute(r.dlms, 6));
////        } catch (Exception ex) {
////            log.error(ex.toString());
////        }
//    }

    /**
     * Mengatur waktu pengaktifan passive calendar.Cosem object yang digunakan
     * adalah standard blue book yaitu "0.0.13.0.0.255"
     *
     * @param calendar Waktu pengaktifan dalam bentuk java.util.Calendar
     * @throws Exception
     */
    public static void setActivatePassiveCalendarDate(DLMS r, Calendar calendar) throws Exception {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar("0.0.13.0.0.255");
        activityCalendar.setVersion(0);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendar.getLogicalName()));
        setActivatePassiveCalendarDate(r, calendar, activityCalendar);
    }

    /**
     * Mengatur waktu pengaktifan passive calendar.
     *
     * @param calendar Waktu pengaktifan dalam bentuk java.util.Calendar
     * @param activityCalendar Cosem object activity calendar
     * @throws Exception
     */
    public static  void setActivatePassiveCalendarDate(DLMS r, Calendar calendar, GXDLMSActivityCalendar activityCalendar) throws Exception {
//        log.info("setActivatePassiveCalendarDate ", new Object[]{activityCalendar.getObjectType(),
//            activityCalendar.getLogicalName(), activityCalendar.getDescription()});
//        try {
            GXDateTime dateTime = new GXDateTime(calendar);
            activityCalendar.setTime(dateTime);
            r.writeObject(activityCalendar, 10);
//            log.info("Passive calendar will active at {0}", activityCalendar.getTime());
//        } catch (Exception ex) {
//            log.error(PrintUtil.FAIL_LOG, ex.toString());
//        }
//        System.out.println("===========================================================================");
    }

	public static String textToHex(String text)
	{
	    byte[] buf = null;
	    buf = text.getBytes(encodingType);
	    char[] HEX_CHARS = "0123456789abcdef".toCharArray();
	    char[] chars = new char[2 * buf.length];
	    for (int i = 0; i < buf.length; ++i)
	    {
	        chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
	        chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
	    }
	    return new String(chars);
	}
}
