package id.co.tabs.hes.dcu.reader;

import java.util.List;

import com.google.protobuf.ByteString;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import gurux.dlms.GXReplyData;
import gurux.dlms.enums.DataType;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSImageActivateInfo;
import gurux.dlms.objects.GXDLMSImageTransfer;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.enums.ImageTransferStatus;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.dlms.DLMS;
import id.co.tabs.hes.util.ObisDescriptor;

public class DLMSAction {
    public static void _DLMSdisconnectMeter(final DLMS r, final int value) throws Exception {
        System.out.println("Start Disconnect");
        final GXDLMSDisconnectControl dc = new GXDLMSDisconnectControl("0.0.96.3.10.255");
        // dc.setOutputState(true);
        final int ic = 1 + 0xff;
        r.readDLMSPacket(r.getClient().method(dc, value + 1, ic, DataType.INT8));
        System.out.println("Finish Disconnect");
    }

    public	static void clearTamper(final DLMS r) throws Exception {
//		log.info("Start Clear Tamper");
		GXDLMSScriptTable dc = new GXDLMSScriptTable("0.0.10.0.128.255");
		int ic = 2;
		r.readDLMSPacket(r.getClient().method(dc, 1, ic, DataType.UINT16));
//		log.info("Finish Clear Tamper");
	}

    public	static void resetMeter(final DLMS r) throws Exception {
//		log.info("Start Reset LP Meter");
		GXDLMSScriptTable dc = new GXDLMSScriptTable("0.0.10.0.0.255");
		dc.setVersion(0);
        dc.setDescription(ObisDescriptor.getDeskripsi("0.0.10.0.0.255"));
		r.readDLMSPacket(dc.execute(r.getClient(), 6));
//		log.info("Finish Reset LP Meter");
	}

	public static void trigerTest(final DLMS r) throws Exception {
//		log.info("Start Triger Test");
		GXDLMSScriptTable dc = new GXDLMSScriptTable("0.0.10.0.129.255");
		int ic = 3;
		r.readDLMSPacket(r.getClient().method(dc, 1, ic, DataType.UINT16));
//		log.info("Finish Triger Test");
	}

    public	static void eventPush(final DLMS r, int type) throws Exception {
		// 0x0004 Mengkonfirmasi bahwa push event/alarm/tamper telah diterima
		// oleh aplikasi AMI
		// 0xFFFF Menonaktifkan (disable) aktifitas event push
		// 0x1111 Mengaktifkan (enable) aktifitas event push
//		log.info("Start Event Push");
		GXDLMSScriptTable dc = new GXDLMSScriptTable("0.0.10.0.130.255");
		// int ic = 3;
		r.readDLMSPacket(r.getClient().method(dc, 1, type, DataType.UINT16));
//		log.info("Finish Event Push");
	}

    public	static void pushScriptTable(final DLMS r, int type) throws Exception {
		// 0x0001 (2) 40 0.1.25.9.0.255 1 0
		// 0x0002 (2) 40 0.2.25.9.0.255 1 0
		// 0x0003 (2) 40 0.3.25.9.0.255 1 0
		// 0x0004 (2) 40 0.4.25.9.0.255 1 0
//		log.info("Start Push Script Table");
		GXDLMSScriptTable dc = new GXDLMSScriptTable("0.0.10.0.108.255");
		// int ic = 3;
		r.readDLMSPacket(r.getClient().method(dc, 1, type, DataType.UINT16));
//		log.info("Finish Push Script Table");
	}

    public	static void imageTransfer(final DLMS r, String Identification, ByteString image) throws Exception {
		byte[] data = image.toByteArray();
		GXDLMSImageTransfer target = new GXDLMSImageTransfer("0.0.44.0.0.255");
		GXReplyData reply = new GXReplyData();
		// Check that image transfer ia enabled.
		r.readDataBlock(r.getClient().read(target, 5), reply);
		r.getClient().updateValue(target, 5, reply.getValue());
		if (!target.getImageTransferEnabled()) {
			throw new Exception("Image transfer is not enabled");
		}

		// Step 1: Read image block size.
		r.readDataBlock(r.getClient().read(target, 2), reply);
		r.getClient().updateValue(target, 2, reply.getValue());
//		System.out.println("============" + Identification);
//		System.out.println(data.length);

		// Step 2: Initiate the Image transfer process.
		r.readDataBlock(target.imageTransferInitiate(r.getClient(), Identification, data.length), reply);

		// Step 3: Transfers ImageBlocks.
		int[] imageBlockCount = new int[1];
		r.readDataBlock(target.imageBlockTransfer(r.getClient(), data, imageBlockCount), reply);

		// Step 4: Check the completeness of the Image.
		r.readDataBlock(r.getClient().read(target, 3), reply);

		r.getClient().updateValue(target, 3, reply.getValue());

		// Step 5: The Image is verified;
		r.readDataBlock(target.imageVerify(r.getClient()), reply);
		//
		// // Step 6: Before activation, the Image is checked;
		// //Get list to imaages to activate.
		r.readDataBlock(r.getClient().read(target, 7), reply);
		boolean bFound = false;
		try {
			r.getClient().updateValue(target, 7, reply);
			for (GXDLMSImageActivateInfo it : target.getImageActivateInfo()) {
				if (it.getIdentification().equals(Identification)) {
					bFound = true;
					break;
				}
			}
		} catch (Exception e) {

		}

		// Read image transfer status.
		r.readDataBlock(r.getClient().read(target, 6), reply);
		try {
			r.getClient().updateValue(target, 6, reply);
			if (target.getImageTransferStatus() != ImageTransferStatus.IMAGE_VERIFICATION_SUCCESSFUL) {
				throw new RuntimeException("Image transfer status is " + target.getImageTransferStatus().toString());
			}
			if (!bFound) {
				throw new RuntimeException("Image not found.");
			}
		} catch (Exception e) {

		}
		// Step 7: Activate image.
		r.readDataBlock(target.imageActivate(r.getClient()), reply);
	}

    public	static void clearProfileGeneric(final DLMS r, String namaField) throws Exception {
//		log.info("Start Clear Profile");
		BasicDBObject query = new BasicDBObject();
		query.put("NAMA_FIELD", namaField);
		query.put("CLASS_ID", 7);
		List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, null);
		for (DBObject csr : cursors) {
			// System.out.println(csr.get("LOGICAL_NAME").toString());
			String obis = csr.get("LOGICAL_NAME").toString();
			GXDLMSProfileGeneric it = new GXDLMSProfileGeneric(obis);
			r.readDLMSPacket(r.getClient().method(it, 1, 0, DataType.INT8));
		}
//		log.info("Finish Clear Profile");
	}

}
