package id.co.tabs.hes.dcu.reader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import gurux.common.GXCommon;
import gurux.dlms.GXDateTime;
import gurux.dlms.GXTime;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSEmergencyProfile;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSObjectCollection;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSSeasonProfile;
import gurux.dlms.objects.GXDLMSWeekProfile;
import gurux.dlms.objects.IGXDLMSBase;
import gurux.dlms.objects.enums.ControlMode;
import gurux.dlms.objects.enums.ControlState;

import com.mongodb.BasicDBList;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.ResultRead;
import id.co.tabs.hes.dlms.DLMS;

public class DLMSReader {
    private static final Logger LOGGER = Logger.getLogger(DLMSReader.class.getName());
    private String getLogPrefix(final String serialNumber, final String currentMeter) {
        if (currentMeter == null)
            return "DCU@" + serialNumber;
        else
            return "DCU@" + serialNumber + " Meter@" + currentMeter;
    }
    private static void log(final Level level, final String message) {
        // LOGGER.log(level, getLogPrefix(serialNumber, currentMeter) + " " + message);
        LOGGER.log(level, message);
    }
	public static String _DLMSreadProfileGeneric(DLMS r, String jenis, java.util.Calendar startLP, java.util.Calendar endLp,
			String UID, Params p, Boolean isReadAll) throws Exception {
		String status = "";
		try {
			// if (r.dlms.getInterfaceType() == InterfaceType.WRAPPER) {
			// r.replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
			// } else {
//			r.replyBuff = java.nio.ByteBuffer.allocate(100);
			// }

			GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
			pg.setLogicalName("1.0.99.1.0.255");
			pg.setObjectType(ObjectType.PROFILE_GENERIC);
			pg.setDescription("LoadProfile");

			GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
			pg2.setLogicalName("1.0.99.1.0.255");
			pg2.setObjectType(ObjectType.PROFILE_GENERIC);
			pg2.setDescription("LoadProfile");

			BasicDBObject query = new BasicDBObject();
			String jenisPhasa = "FASE1";
			if (p.getPhasa() == 1) {
				jenisPhasa = "FASE1";
			} else {
				jenisPhasa = "FASE3";
			}
			// System.out.println(jenis);
			query.put(jenisPhasa, "Y");
			query.put("CLASS_ID", 7);
			String _jenis = "GENERIC";
			Boolean isLoadProfile = false;
			if (jenis.toUpperCase().equals("PROFILE")) {
				_jenis = "GENERIC";
				query.put("NAMA_FIELD", "LOAD_PROFILE");
				isLoadProfile = true;
			} else if (jenis.toUpperCase().equals("BILLING")) {
				_jenis = "GENERIC";
				query.put("NAMA_FIELD", "EOB");
			} else {
				_jenis = jenis;
			}
			query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));

			if (p.gettypeObis().equals("SPLN")) {
				DBObject clause1 = new BasicDBObject("TYPE_OBIS", "SPLN");
				DBObject clause2 = new BasicDBObject("TYPE_OBIS", "");
				DBObject clause3 = new BasicDBObject("TYPE_OBIS", null);
				DBObject clause4 = new BasicDBObject("TYPE_OBIS", new BasicDBObject("$exists", false));
				BasicDBList or = new BasicDBList();
				or.add(clause1);
				or.add(clause2);
				or.add(clause3);
				or.add(clause4);
				query.put("$or", or);
			} else {
				query.put("TYPE_OBIS", p.gettypeObis());
			}

//			System.out.println(query.toJson().toString());
			List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, _jenis);
			if (cursors.size() == 0) {
				// System.out.println();
				throw new NullPointerException("Obis tidak ditemukan");
			}
			for (DBObject csr : cursors) {
				try {
//					System.out.println(csr.get("LOGICAL_NAME").toString());
					String obis = csr.get("LOGICAL_NAME").toString();
					String type = csr.get("CLASS_ID").toString();
					String column = csr.get("NAMA_FIELD").toString();
					String deskripsi = csr.get("DESKRIPSI").toString();

					pg.setLogicalName(obis);
					pg.setObjectType(r.getObjectType(type));
					pg.setDescription(deskripsi);

					pg2.setLogicalName(obis);
					pg2.setObjectType(r.getObjectType(type));
					pg2.setDescription(deskripsi);

//					System.out.println("======== addToCollection =======");
					r.addToCollection(pg, p.getMerkMeter().equals("EDMI"), isLoadProfile, 1);
//					System.out.println("======== End addToCollection =======");
//					System.out.println("======== getKolomPG =======");
					r.getKolomPG(pg);
//					System.out.println("======== End getKolomPG =======");

					// r.getProfileGenericColumns(pg);

					if (pg.getCaptureObjects().size() > 0) {
						Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

						if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
							GXDLMSObject dt = new GXDLMSObject();
							dt.setLogicalName("0.0.1.0.0.255");
							dt.setObjectType(ObjectType.DATA);
							dt.setUIDataType(2, DataType.DATETIME);
							pg2.addCaptureObject(dt, 2, 0);

							dt = new GXDLMSObject();
							dt.setLogicalName("1.0.0.8.4.255");
							dt.setObjectType(ObjectType.DATA);
							pg2.addCaptureObject(dt, 2, 0);

							for (Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
								pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
										it.getValue().getDataIndex());
							}
						} else {
							pg2 = pg;
						}

						r.getProfileGenerics(pg2, startLP, endLp, jenis, UID, p, isReadAll);
					}
					status = status + csr.get("NAMA_FIELD").toString() + "=> SUKSES; ";
				} catch (Exception e) {
					status = status + csr.get("NAMA_FIELD").toString() + "=> Err : " + e.getMessage() + "; ";
					e.printStackTrace();
				}
			}
		} finally {
			r.close();
		}
		return status;
	}

    public static void _DLMSreadOneData(final DLMS r, final String logicalName, final String classId, final String namaField,
            final String UID, final Params p)throws Exception {
    	// try {
    			java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    			Document doc = new Document("TGL_MULAI", now.getTime())
    					// .append("TGL_MULAI", now.getTime())
    					.append("UID", UID).append("MERK_METER", p.getMerkMeter()).append("TYPE_METER", p.getTypeMeter())
    					.append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
    					.append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT", p.getKdPembangkit())
    					.append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis())
    					.append("DATA_METER", p.getDbObject() == null ? null : p.getDbObject().toMap());

//    			String obis = logicalName;
//    			String type = classId;
//    			String column = namaField;
    			if (namaField.equals("TOU")) {
    				_readTOU(r, logicalName, classId, namaField, doc, UID, p);
    			} else {
    				if (classId.equals("70")) {
    					_readDisconnectControl(r, logicalName, classId, namaField, doc, UID, p);
    				} else if (classId.equals("20")) {
    					_readActiveCallendar(r, logicalName, classId, namaField, doc, UID, p);
    				} else if (classId.equals("71")) {
    					_readLimmiter(r, logicalName, classId, namaField, doc, UID, p);
    				} else if (!classId.equals("7")) {
    					GXObisCode skipItem = new GXObisCode();
    					skipItem.setObjectType(r.getObjectType(classId));
    					skipItem.setLogicalName(logicalName);
    					skipItem.setVersion(0);
    					skipItem.setDescription(logicalName);
    					List<ResultRead> ldt = new ArrayList<>();
    					ldt = r.getRegister(skipItem);
    					doc.append("NAMA_FIELD", namaField);
    					for (int j = 0; j < ldt.size(); j++) {
    						// System.out.println("DATA" +
    						// r.ifNull(ldt.get(j).getAlias()) + " : " +
    						// r.convertData(ldt.get(j))
    						// + " : " + ldt.get(j).getType());
    						doc.append("DATA" + r.ifNull(ldt.get(j).getAlias()),
    								r.convertData(ldt.get(j), p.getMerkMeter(), classId));
    						// doc.append("DATA"+ r.ifNull(ldt.get(j).getAlias()),
    						// "xxxx");
    					}
    					now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    					doc.append("TGL_SELESAI", now.getTime());
    					MongoDB.setData("DATA_GLOBAL", doc);
    				} else {
    					GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    					pg.setLogicalName(logicalName);
    					pg.setObjectType(ObjectType.PROFILE_GENERIC);
    					pg.setDescription(logicalName);

    					GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
    					pg2.setLogicalName(logicalName);
    					pg2.setObjectType(ObjectType.PROFILE_GENERIC);
    					pg2.setDescription(logicalName);

    					r.getProfileGenericColumns(pg);

    					if (pg.getCaptureObjects().size() > 0) {
    						Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

    						if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
    							GXDLMSObject dt = new GXDLMSObject();
    							dt.setLogicalName("0.0.1.0.0.255");
    							dt.setObjectType(ObjectType.DATA);
    							dt.setUIDataType(2, DataType.DATETIME);
    							pg2.addCaptureObject(dt, 2, 0);

    							dt = new GXDLMSObject();
    							dt.setLogicalName("1.0.0.8.4.255");
    							dt.setObjectType(ObjectType.DATA);
    							pg2.addCaptureObject(dt, 2, 0);

    							for (Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
    								pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
    										it.getValue().getDataIndex());
    							}
    						} else {
    							pg2 = pg;
    						}
    						java.util.Calendar startLP = java.util.Calendar
    								.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    						startLP.add(java.util.Calendar.DATE, -1);
    						java.util.Calendar endLp = java.util.Calendar
    								.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    						r.getProfileGenerics(pg2, startLP, endLp, "GLOBAL", UID, p, false);
    					}
    				}
    			}

    			// } catch (Exception e) {
    			// e.printStackTrace();
    			// }
    }

    public static String _DLMSreadData(final DLMS r, final String jenis, final String UID, final Params p) throws Exception {
    	String status = "";
		BasicDBObject query = new BasicDBObject();
		String jenisPhasa = "FASE1";
		if (p.getPhasa() == 1) {
			jenisPhasa = "FASE1";
		} else {
			jenisPhasa = "FASE3";
		}
		query.put(jenisPhasa, "Y");
		query.put(jenis, new BasicDBObject("$ne", 0).append("$exists", true));
		if (jenis.equals("PROFILE")) {
			query.put("PSTATUS", "A");
		} else if (jenis.equals("BILLING")) {
			query.put("BSTATUS", "A");
		}
		if (p.gettypeObis().equals("SPLN")) {
			DBObject clause1 = new BasicDBObject("TYPE_OBIS", "SPLN");
			DBObject clause2 = new BasicDBObject("TYPE_OBIS", "");
			DBObject clause3 = new BasicDBObject("TYPE_OBIS", null);
			DBObject clause4 = new BasicDBObject("TYPE_OBIS", new BasicDBObject("$exists", false));
			BasicDBList or = new BasicDBList();
			or.add(clause1);
			or.add(clause2);
			or.add(clause3);
			or.add(clause4);
			query.put("$or", or);
		} else {
			query.put("TYPE_OBIS", p.gettypeObis());
		}
//		System.out.println(query.toJson().toString());
		/* Step 5 : Get all documents */
		List<DBObject> cursors = MongoDB.getData("DATA_OBIS", query, jenis);
		if (cursors.size() == 0) {
			throw new NullPointerException("Obis tidak ditemukan");
		}
		/* Step 6 : Print all documents */
		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		// System.out.println("Now " + now.getTime());
		Document doc = new Document("TGL_MULAI", now.getTime())
				// .append("TGL_MULAI", now.getTime())
				.append("UID", UID).append("MERK_METER", p.getMerkMeter()).append("TYPE_METER", p.getTypeMeter())
				.append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
				.append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT", p.getKdPembangkit())
				.append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis())
				.append("DATA_METER", p.getDbObject() == null ? null : p.getDbObject().toMap());

		int i = 1;
		int ke = 1;
//		System.out.println(cursors.size());
		for (int c = 0; c < cursors.size(); c++) {
			DBObject csr = cursors.get(c);
			try {
				String obis = csr.get("LOGICAL_NAME").toString();
				String type = csr.get("CLASS_ID").toString();
				String column = csr.get("NAMA_FIELD").toString();
				String version = csr.get("VERSION").toString();
//				System.out.println(c + " : " + obis + "; " + type + "; " + column + "; " + version);
				if(type.equals("7")){
					_DLMSreadProfileGeneric(r, jenis, null, null, UID, p, true);
				}else{

				GXObisCode skipItem = new GXObisCode();
				skipItem.setObjectType(r.getObjectType(type));
				skipItem.setLogicalName(obis);
				skipItem.setVersion(Integer.parseInt(version));
				skipItem.setDescription(obis);
				List<ResultRead> ldt = new ArrayList<>();
				String dt = null;
				ldt = r.getRegister(skipItem);
				String msg = "";
				for (int j = 0; j < ldt.size(); j++) {
					doc.remove(column + r.ifNull(ldt.get(j).getAlias()));
					doc.append(column + r.ifNull(ldt.get(j).getAlias()),
							r.convertData(ldt.get(j), p.getMerkMeter(), type));
					if (column.equals("TGL_BACA") && r.ifNull(ldt.get(j).getAlias()).equals("")) {
						doc.remove(column + r.ifNull(ldt.get(j).getAlias()) + "_UTC");
						doc.append(column + r.ifNull(ldt.get(j).getAlias()) + "_UTC", ldt.get(j).getResult());
					}
					msg = r.ifNull(ldt.get(j).getStatus());
					if (!msg.equals(""))
						status = status + column + " -> " + msg + "; ";
				}
				if (msg.equals(""))
					status = status + column + " -> SUKSES" + "; ";
				}
				Thread.sleep(1000);
				i++;
			} catch (Exception e) {
				if (ke == 2) {
					throw new Exception(e.getMessage());
				}
				if (ke == 1 && e.getMessage().equals("Failed to receive reply from the device in given time.")) {
					ke = 2;
					try {
						r.initializeConnection();
						ke = 1;
						c--;
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					status = status + csr.get("NAMA_FIELD").toString() + " -> " + e.getMessage() + "; ";
				}
			}
		}
		now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		doc.append("TGL_SELESAI", now.getTime());
		MongoDB.setData("DATA_" + jenis, doc);
		return status;
    }

	public static void readAssociationView(DLMS r, String jenis, String UID, Params p) throws Exception {

		/* Step 6 : Print all documents */
		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		// System.out.println("Now " + now.getTime());
		Document doc = new Document("TGL_MULAI", now.getTime()).append("UID", UID)
				.append("MERK_METER", p.getMerkMeter()).append("TYPE_METER", p.getTypeMeter())
				.append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
				.append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT", p.getKdPembangkit())
				.append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis())
				.append("DATA_METER", p.getDbObject() == null ? null : p.getDbObject().toMap());
		;

		GXDLMSObjectCollection objects = r.getAssociationView();
		now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		for (GXDLMSObject obj : objects) {
			try {
				Document docx = new Document();
				docx.putAll(doc);
				docx.append("LOGICAL_NAME", obj.getLogicalName());
				docx.append("DESCRIPTION", obj.getDescription() == null ? "" : obj.getDescription().toString());
				docx.append("NAME", obj.getName());
				docx.append("SHORT_NAME", obj.getShortName());
				docx.append("VERSION", obj.getVersion());
				docx.append("OBJECT_TYPE", obj.getObjectType().toString());
				docx.append("ATTRIBUTE_COUNT", obj.getAttributeCount());
				for (int pos : ((IGXDLMSBase) obj).getAttributeIndexToRead(true)) {
					docx.append("ATTRIBUTE_" + pos, obj.getDataType(pos) + "[" + obj.getUIDataType(pos) + "]" + " ("
							+ obj.getAccess(pos).toString() + ")");
				}
				docx.append("METHOD_COUNT", obj.getMethodCount());
				docx.append("TGL_SELESAI", now.getTime());
				MongoDB.setData("DATA_" + jenis, docx);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public static void _readDisconnectControl(final DLMS r, final String logicalName, final String classId,
			final String namaField, Document doc, final String UID, final Params p) throws Exception {
		GXDLMSDisconnectControl control = new GXDLMSDisconnectControl(logicalName);
		control.setDescription("Disconnect Control");
		GXDLMSObject object = control;
		for (int pos : control.getAttributeIndexToRead(true)) {
			// try {
			Object val = r.read(object, pos);
			switch (pos) {
			case 1:
				control.setLogicalName(r.showValue(pos, val));
				break;
			case 2:
				if (r.showValue(pos, val).equals("true")) {
					control.setOutputState(true);
				} else {
					control.setOutputState(false);
				}
				break;
			case 3:
				if (r.showValue(pos, val).equals("Connected")) {
					control.setControlState(ControlState.CONNECTED);
				} else {
					if (r.showValue(pos, val).equals("Disconnected")) {
						control.setControlState(ControlState.DISCONNECTED);
					} else {
						control.setControlState(ControlState.READY_FOR_RECONNECTION);
					}
				}
				break;
			case 4:
				switch (r.showValue(pos, val)) {
				case "None":
					control.setControlMode(ControlMode.NONE);
					break;
				case "Mode1":
					control.setControlMode(ControlMode.MODE_1);
					break;
				case "Mode2":
					control.setControlMode(ControlMode.MODE_2);
					break;
				case "Mode3":
					control.setControlMode(ControlMode.MODE_3);
					break;
				case "Mode4":
					control.setControlMode(ControlMode.MODE_4);
					break;
				case "Mode5":
					control.setControlMode(ControlMode.MODE_5);
					break;
				case "Mode6":
					control.setControlMode(ControlMode.MODE_6);
					break;
				default:
					control.setControlMode(ControlMode.NONE);
					break;
				}
				break;
			}
			// } catch (Exception ex) {
			// log.error("error : " + ex.toString());
			// }
		}
		// System.out.println("Logical Name = " + control.getLogicalName());
		// System.out.println("Output State = " + control.getOutputState());
		// System.out.println("Control State = " + control.getControlState());
		// System.out.println("Control Mode = " + control.getControlMode());
		doc.append("LOGICAL_NAME", control.getLogicalName().toString());
		doc.append("OUTPUT_STATE", control.getOutputState());
		doc.append("DATA", control.getControlState().toString());
		doc.append("CONTROL_MODE", control.getControlMode().toString());
		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		doc.append("TGL_SELESAI", now.getTime());
		try {
			MongoDB.setData("DATA_" + "GLOBAL", doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
//			log.error(e.getMessage());
		}
	}

	public static void _readLimmiter(final DLMS r, final String logicalName, final String classId,
			final String namaField, Document doc, final String UID, final Params p) throws Exception {
		GXDLMSLimiter limiter = new GXDLMSLimiter("0.0.17.0.0.255");
		limiter.setVersion(0);
		limiter.setDescription("Limiter");
		r.getClient().getObjects().add(limiter);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
		for (int pos : limiter.getAttributeIndexToRead(true)) {
			System.out.print("Pos " + pos + " : ");
			// try{
			Object val = r.read(limiter, pos);
			switch (pos) {
			case 1:
				doc.append("LOGICAL_NAME", r.showValue(pos, val));
				break;
			case 2:
				doc.append("MONITORED_VALUE", r.showValue(pos, val));
				break;
			case 3:
				doc.append("THRESHOLD_ACTIVE", r.showValue(pos, val));
				break;
			case 4:
				doc.append("THRESHOLD_NORMAL", r.showValue(pos, val));
				break;
			case 5:
				doc.append("THRESHOLD_EMERGENCY", r.showValue(pos, val));
				break;
			case 6:
				doc.append("MIN_OVER_THRESHOLD_DURATION", r.showValue(pos, val));
				break;
			case 7:
				doc.append("MIN_UNDER_THRESHOLD_DURATION", r.showValue(pos, val));
				break;
			case 8:
				JSONObject obj = new JSONObject();
				obj.put("ACTIVATION_TIME", sdf.format(((GXDLMSEmergencyProfile) val).getActivationTime().getValue()));
				obj.put("DURATION", ((GXDLMSEmergencyProfile) val).getDuration());
				doc.append("EMERGENCY_PROFILE", obj);
				break;
			case 9:
				doc.append("EMERGENCY_PROFILE_GROUP_ID_LIST", r.showValue(pos, val));
				break;
			case 10:

				doc.append("EMERGENCY_PROFILE_ACTIVE", r.showValue(pos, val));
				break;
			case 11:
				doc.append("ACTIONS", r.showValue(pos, val));
				break;
			}
			// }catch(Exception e){
			// e.printStackTrace();
			// }
			// System.out.println("");
		}
		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		doc.append("TGL_SELESAI", now.getTime());
		try {
			MongoDB.setData("DATA_" + "GLOBAL", doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
//			log.error(e.getMessage());
		}
	}

	public static void _readActiveCallendar(final DLMS r, final String logicalName, final String classId,
			final String namaField, Document doc, final String UID, final Params p) throws Exception {
		GXDLMSActivityCalendar gxdlmsac = new GXDLMSActivityCalendar("0.0.13.0.0.255");
		gxdlmsac.setVersion(0);
		gxdlmsac.setDescription("Activity Calendar");
		for (int pos : gxdlmsac.getAttributeIndexToRead(true)) {
			Object val = r.read(gxdlmsac, pos);
			switch (pos) {
			case 1:
				doc.append("LOGICAL_NAME", r.showValue(pos, val));
				break;
			case 2:
				doc.append("CALENDAR_NAME_ACTIVE", r.showValue(pos, val));
				break;
			// case 3:
			// doc.append("SEASON_PROFILE_ACTIVE", r.showValue(pos, val));
			// break;
			// case 4:
			// doc.append("WEEK_PROFILE_ACTIVE", r.showValue(pos, val));
			// break;
			// case 5:
			// doc.append("DAY_PROFILE_ACTIVE", r.showValue(pos, val));
			// break;
			case 6:
				doc.append("CALENDAR_NAME_PASSIVE", r.showValue(pos, val));
				break;
			// case 7:
			// doc.append("SEASON_PROFILE_PASSIVE", r.showValue(pos, val));
			// break;
			// case 8:
			// doc.append("WEEK_PROFILE_PASSIVE", r.showValue(pos, val));
			// break;
			// case 9:
			// doc.append("DAY_PROFILE_PASSIVE", r.showValue(pos, val));
			// break;
			// case 10:
			// doc.append("ACTIVATE_PASSIVE_CALENDAR_TIME", r.showValue(pos,
			// val));
			// break;
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));

		SimpleDateFormat sdfH = new SimpleDateFormat("HH:mm:ss");
		sdfH.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));

		System.out.println("calendar_name_active = " + gxdlmsac.getCalendarNameActive());
		System.out.println("season_profile_active list = ");
		System.out.println("\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
		JSONArray j_season = new JSONArray();
		for (GXDLMSSeasonProfile seasonProfile : gxdlmsac.getSeasonProfileActive()) {
			JSONObject tmp = new JSONObject();
			tmp.put("NAME", GXCommon.bytesToHex(seasonProfile.getName()));
			tmp.put("START", sdf.format(((GXDateTime) seasonProfile.getStart()).getValue()));
			tmp.put("WEEK_NAME", GXCommon.bytesToHex(seasonProfile.getWeekName()));
			j_season.add(tmp);
			System.out.println("\t" + GXCommon.bytesToHex(seasonProfile.getName()) + "\t"
					+ seasonProfile.getStart().toString() + "\t" + GXCommon.bytesToHex(seasonProfile.getWeekName()));
		}
		doc.append("SEASON_PROFILE_ACTIVE", j_season);
		System.out.println("week_profile_table_active list = ");
		System.out.println("\t" + "Name" + "\t" + "Monday" + "\t" + "Tuesday" + "\t" + "Wednesday" + "\t" + "Thursday"
				+ "\t" + "Friday" + "\t" + "Saturday" + "\t" + "Monday");

		j_season = new JSONArray();
		for (GXDLMSSeasonProfile seasonProfile : gxdlmsac.getSeasonProfilePassive()) {
			JSONObject tmp = new JSONObject();
			tmp.put("NAME", GXCommon.bytesToHex(seasonProfile.getName()));
			tmp.put("START", sdf.format(((GXDateTime) seasonProfile.getStart()).getValue()));
			tmp.put("WEEK_NAME", GXCommon.bytesToHex(seasonProfile.getWeekName()));
			j_season.add(tmp);
		}
		doc.append("SEASON_PROFILE_PASIVE", j_season);

		j_season = new JSONArray();
		for (GXDLMSWeekProfile weekProfile : gxdlmsac.getWeekProfileTableActive()) {
			JSONObject tmp = new JSONObject();
			tmp.put("NAME", GXCommon.bytesToHex(weekProfile.getName()));
			tmp.put("MONDAY", weekProfile.getMonday());
			tmp.put("TUESDAY", weekProfile.getTuesday());
			tmp.put("WEDNESDAY", weekProfile.getWednesday());
			tmp.put("THURSDAY", weekProfile.getThursday());
			tmp.put("FRIDAY", weekProfile.getFriday());
			tmp.put("SATURDAY", weekProfile.getSaturday());
			j_season.add(tmp);
			System.out.println("\t" + GXCommon.bytesToHex(weekProfile.getName()) + "\t" + weekProfile.getMonday() + "\t"
					+ weekProfile.getTuesday() + "\t" + weekProfile.getWednesday() + "\t" + weekProfile.getThursday()
					+ "\t" + weekProfile.getFriday() + "\t" + weekProfile.getSaturday() + "\t"
					+ weekProfile.getMonday());
		}
		doc.append("WEEK_PROFILE_ACTIVE", j_season);

		j_season = new JSONArray();
		for (GXDLMSWeekProfile weekProfile : gxdlmsac.getWeekProfileTablePassive()) {
			JSONObject tmp = new JSONObject();
			tmp.put("NAME", GXCommon.bytesToHex(weekProfile.getName()));
			tmp.put("MONDAY", weekProfile.getMonday());
			tmp.put("TUESDAY", weekProfile.getTuesday());
			tmp.put("WEDNESDAY", weekProfile.getWednesday());
			tmp.put("THURSDAY", weekProfile.getThursday());
			tmp.put("FRIDAY", weekProfile.getFriday());
			tmp.put("SATURDAY", weekProfile.getSaturday());
			j_season.add(tmp);
		}
		doc.append("WEEK_PROFILE_PASIVE", j_season);

		System.out.println("day_profile_table_active list = ");

		j_season = new JSONArray();
		for (GXDLMSDayProfile dayProfile : gxdlmsac.getDayProfileTableActive()) {
			System.out.println("\t" + "Day ID = " + dayProfile.getDayId());
			JSONObject tmp = new JSONObject();
			tmp.put("DAY_ID", dayProfile.getDayId());
			JSONArray j_temp = new JSONArray();
			for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
				JSONObject tmp1 = new JSONObject();
				tmp1.put("START_TIME", sdfH.format(((GXTime) profileAction.getStartTime()).getValue()));
				tmp1.put("SCRIPT", profileAction.getScriptLogicalName());
				tmp1.put("SELECTOR", profileAction.getScriptSelector());
				j_temp.add(tmp1);
				System.out.print("\t\t" + "Start Time : " + profileAction.getStartTime());
				System.out.print("\t\t" + "Script : " + profileAction.getScriptLogicalName());
				System.out.println("\t\t" + "Selector : " + profileAction.getScriptSelector());
			}
			tmp.put("DETAIL", j_temp);
			j_season.add(tmp);
		}
		doc.append("DAY_PROFILE_ACTIVE", j_season);

		j_season = new JSONArray();
		for (GXDLMSDayProfile dayProfile : gxdlmsac.getDayProfileTablePassive()) {
			System.out.println("\t" + "Day ID = " + dayProfile.getDayId());
			JSONObject tmp = new JSONObject();
			tmp.put("DAY_ID", dayProfile.getDayId());
			JSONArray j_temp = new JSONArray();
			for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
				JSONObject tmp1 = new JSONObject();
				tmp1.put("START_TIME", sdfH.format(((GXTime) profileAction.getStartTime()).getValue()));
				tmp1.put("SCRIPT", profileAction.getScriptLogicalName());
				tmp1.put("SELECTOR", profileAction.getScriptSelector());
				j_temp.add(tmp1);
			}
			tmp.put("DETAIL", j_temp);
			j_season.add(tmp);
		}
		doc.append("DAY_PROFILE_PASIVE", j_season);

		doc.append("ACTIVATE_PASSIVE_CALENDAR_TIME", sdf.format(gxdlmsac.getTime().getValue()));
		System.out.println("activate_passive_calendar_time = " + gxdlmsac.getTime());
		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		doc.append("TGL_SELESAI", now.getTime());
		try {
			MongoDB.setData("DATA_" + "GLOBAL", doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			log.error(e.getMessage());
		}
	}

	public static void _readTOU(final DLMS r, final String logicalName, final String classId, final String namaField,
			Document doc, final String UID, final Params p) throws Exception {
		GXDLMSActivityCalendar gxdlmsac = new GXDLMSActivityCalendar("0.0.13.0.0.255");
		gxdlmsac.setVersion(0);
		gxdlmsac.setDescription("TOU");
		Object val = r.read(gxdlmsac, 5);
		JSONObject json = printTOU(gxdlmsac);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//		sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
//
//		SimpleDateFormat sdfH = new SimpleDateFormat("HH:mm:ss");
//		sdfH.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
//
//		System.out.println("calendar_name_active = " + gxdlmsac.getCalendarNameActive());
//		System.out.println("season_profile_active list = ");
//		System.out.println("\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
//		JSONArray j_season = new JSONArray();
//
//		System.out.println("day_profile_table_active list = ");
//
//		j_season = new JSONArray();
//		for (GXDLMSDayProfile dayProfile : gxdlmsac.getDayProfileTableActive()) {
//			System.out.println("\t" + "Day ID = " + dayProfile.getDayId());
//			JSONObject tmp = new JSONObject();
//			tmp.put("DAY_ID", dayProfile.getDayId());
//			JSONArray j_temp = new JSONArray();
//			for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
//				JSONObject tmp1 = new JSONObject();
//				tmp1.put("START_TIME", sdfH.format(((GXTime) profileAction.getStartTime()).getValue()));
//				tmp1.put("SCRIPT", profileAction.getScriptLogicalName());
//				tmp1.put("SELECTOR", profileAction.getScriptSelector());
//				j_temp.add(tmp1);
//				System.out.print("\t\t" + "Start Time : " + profileAction.getStartTime());
//				System.out.print("\t\t" + "Script : " + profileAction.getScriptLogicalName());
//				System.out.println("\t\t" + "Selector : " + profileAction.getScriptSelector());
//			}
//			tmp.put("DETAIL", j_temp);
//			j_season.add(tmp);
//		}
		doc.append("DATA", json);

		java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
		doc.append("TGL_SELESAI", now.getTime());
		try {
			MongoDB.setData("DATA_" + "GLOBAL", doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			log.error(e.getMessage());
		}
	}

	/**
     * Fungsi untuk mencetak informasi TOU.
     *
     * @param activityCalendar GXDLMSActivityCalendar yang akan dicetak
     * informasinya.
     */
    public static JSONObject printTOU(GXDLMSActivityCalendar activityCalendar) {
//        printObjectDefinition(activityCalendar);
        try {
            String timeString;
            HashMap<String, String> daftarTarif = new HashMap<>();
            JSONObject json = new JSONObject();
            for (int i = 0; i < activityCalendar.getDayProfileTableActive()[0].getDaySchedules().length; i++) {
                if (i == activityCalendar.getDayProfileTableActive()[0].getDaySchedules().length - 1) {
                    Calendar startTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i].getStartTime().getLocalCalendar();
                    Calendar endTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[0].getStartTime().getLocalCalendar();
                    endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
                    timeString = startTime.get(Calendar.HOUR) + ":"
                            + (startTime.get(Calendar.MINUTE) < 10
                            ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
                            + (startTime.get(Calendar.SECOND) < 10
                            ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM") + " s/d "
                            + endTime.get(Calendar.HOUR) + ":"
                            + (endTime.get(Calendar.MINUTE) < 10
                            ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
                            + (endTime.get(Calendar.SECOND) < 10
                            ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM"));
                } else {
                    Calendar startTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i].getStartTime().getLocalCalendar();
                    Calendar endTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i + 1].getStartTime().getLocalCalendar();
                    endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
                    timeString = startTime.get(Calendar.HOUR) + ":"
                            + (startTime.get(Calendar.MINUTE) < 10
                            ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
                            + (startTime.get(Calendar.SECOND) < 10
                            ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM") + " s/d "
                            + endTime.get(Calendar.HOUR) + ":"
                            + (endTime.get(Calendar.MINUTE) < 10
                            ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
                            + (endTime.get(Calendar.SECOND) < 10
                            ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM"));
                }
                if (activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].getScriptSelector() == 1
                        || activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].
                                getScriptSelector() == 2) {
                    String wbp = "";
                    if (activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].getScriptSelector() == 1) {
                        wbp = "WBP";
                    } else {
                        wbp = "LWBP";
                    }
                    if (daftarTarif.containsKey(wbp)) {
                        daftarTarif.replace(wbp, daftarTarif.get(wbp) + ", " + timeString);
                    } else {
                        daftarTarif.put(wbp, timeString);
                    }
                }
            }
            for (Map.Entry<String, String> entry : daftarTarif.entrySet()) {
//                System.out.println(entry.getKey() + " : " + entry.getValue());
                json.put(entry.getKey(), entry.getValue());
            }
            return json;
        } catch (Exception e) {
//        	log.error("Cetak value error @" + e.toString());
        }
        return new JSONObject();
    }

}
