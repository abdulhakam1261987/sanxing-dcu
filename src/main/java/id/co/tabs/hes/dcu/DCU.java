package id.co.tabs.hes.dcu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import id.co.tabs.hes.dlms.Aes128Ecb;

import java.util.Map.Entry;

import gurux.common.AutoResetEvent;
import gurux.dlms.GXArray;
import gurux.dlms.GXDLMSGateway;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXStructure;
import gurux.common.GXCommon;
import gurux.dlms.GXDLMSException;
import gurux.dlms.GXDateTime;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.GXTime;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.ClockStatus;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.DateTimeExtraInfo;
import gurux.dlms.enums.DateTimeSkips;
import gurux.dlms.enums.ErrorCode;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Security;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSeasonProfile;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import gurux.dlms.objects.GXDLMSWeekProfile;
import gurux.dlms.objects.enums.GlobalKeyType;
import gurux.dlms.secure.GXDLMSSecureClient;
import gurux.dlms.secure.GXSecure;
import gurux.net.GXNet;
import id.co.tabs.hes.DataLogicalName;
import id.co.tabs.hes.DayProfileList;
import id.co.tabs.hes.ListLogicalName;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.ResultRead;
import id.co.tabs.hes.Utils;
import id.co.tabs.hes.api.GetDataResponse;
import id.co.tabs.hes.dcu.reader.DLMSAction;
import id.co.tabs.hes.dcu.reader.DLMSReader;
import id.co.tabs.hes.dcu.reader.DLMSWriter;
import id.co.tabs.hes.dlms.DLMS;
import id.co.tabs.hes.dlms.IDLMSMedia;
import id.co.tabs.hes.obis.descriptor.ObisDescriptor;
import id.co.tabs.hes.object.Meter;
import id.co.tabs.hes.object.MeterArchiveObj;
import java.time.ZoneId;
import java.util.Random;

public class DCU implements IDLMSMedia {

    private static final Logger LOGGER = Logger.getLogger(DCU.class.getName());
    private final byte[] heartbeatResponse = {0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00};

    private final int WaitTime = 40000;
    private final DLMS dlms = new DLMS(this);

    private GXNet media = null;
    GXDLMSGateway gateway = new GXDLMSGateway();
    private String ipAddress;
    private final String serialNumber;
    private String jenis;
    private byte[] snByte;

    private Boolean online;
    private Boolean waitingData = false;
    private String currentMeter = null;

    /**
     * Received event.
     */
    private final AutoResetEvent receivedEvent = new AutoResetEvent(false);
    private byte[] receivedData;

    public DCU(final GXNet media, final String ipAddress, final String serialNumber, final byte[] snByte,
            final String jenis) {
        this.media = media;
        this.ipAddress = ipAddress;
        this.serialNumber = serialNumber;
        this.jenis = jenis;
        this.snByte = snByte;
        this.dlms.setLogPrefix(getLogPrefix());
    }

    public String getJenis() {
        return jenis;
    }

    public byte[] getSNByte() {
        return snByte;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public Boolean getIsOnline() {
        return online;
    }

    public String getIPAddress() {
        return ipAddress;
    }

    public void setIPAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setIsOnline(final Boolean online) {
        this.online = online;
    }

    private String getLogPrefix() {
        if (currentMeter == null) {
            return "DCU@" + this.serialNumber;
        } else {
            return "DCU@" + this.serialNumber + " Meter@" + currentMeter;
        }
    }

    private void log(final Level level, final String message) {
        LOGGER.log(level, getLogPrefix() + " " + message);
    }

    public Boolean isWaitingData() {
        return this.waitingData;
    }

    public void packetReceived(final byte[] packet) {
        receivedData = Arrays.copyOf(packet, packet.length);
        receivedEvent.set();
        waitingData = false;
    }

    @Override
    public void send(final Object data) {
        try {
            media.send(data, ipAddress);
        } catch (final Exception e) {
            log(Level.WARNING, e.getMessage());
            this.online = false;
        }
    }

    @Override
    public boolean receive() {
        try {
            waitingData = true;
            return receivedEvent.waitOne(WaitTime);
        } catch (final Exception ex) {
        }
        return false;
    }

    @Override
    public byte[] getReceivedData() {
        return receivedData;
    }

    public Boolean processHeartbeat(final byte[] packet) {
        if ((packet.length > 8) && (packet[7] == 3) && (packet[8] == 1)) {
            log(Level.INFO, "Incoming heartbeat, replying...");
            send(heartbeatResponse);
            return true;
        }
        return false;
    }

    public void setTargetMeter(final String serialNumber) {
        final GXDLMSSecureClient client = dlms.getClient();
        if ((serialNumber == null) || (serialNumber.isEmpty())) {
            log(Level.INFO, "Select DCU as target.");
            client.setPassword("Aa12345.");
            client.setGateway(null);
            client.setClientAddress(1);
            currentMeter = null;
        } else {
            log(Level.INFO, "Select meter " + serialNumber + " as target.");
            gateway.setNetworkId((short) 0);
            gateway.setPhysicalDeviceAddress(serialNumber.getBytes());
            client.setPassword("12345678");
            client.setGateway(gateway);
            client.setClientAddress(5);
            currentMeter = serialNumber;
        }
        dlms.setLogPrefix(getLogPrefix());
    }

    public void setTargetMeter(final String serialNumber, final String AuthMeter, final boolean isDCU) throws Exception {
        final HashMap<String, String> MapAuthMeter = MappingAuthMeter(AuthMeter);
        final GXDLMSSecureClient client = dlms.getClient();

        if (isDCU) {
            log(Level.INFO, "Select DCU as target.");
            // client.setPassword("00000000");
            if (serialNumber == null || serialNumber.equals("")) {
                setParamDCU(client, this.serialNumber, MapAuthMeter, isDCU);
            } else {
                setParam(client, serialNumber, MapAuthMeter, isDCU);
            }
            // client =
            client.setGateway(null);
            // client.setClientAddress(1);
            currentMeter = null;
        } else {
            log(Level.INFO, "Select meter " + serialNumber + " as target.");
            gateway.setNetworkId((short) 0);
            gateway.setPhysicalDeviceAddress(serialNumber.getBytes());
            setParam(client, serialNumber, MapAuthMeter, isDCU);
            // client.setPassword("00000000");
            client.setGateway(gateway);
            // client.setClientAddress(5);
            currentMeter = serialNumber;
        }
        dlms.setLogPrefix(getLogPrefix());
    }

    public String[] getMeterList() throws Exception {
        // final GXDLMSObject obj = dlms.getObject("0.1.96.1.130.255");
        GXDLMSObject obj = new GXDLMSObject();

        GXObisCode skipItem = new GXObisCode();
        skipItem.setObjectType(ObjectType.forValue(1));
        skipItem.setLogicalName("0.1.96.1.130.255");
        skipItem.setVersion(0);
        skipItem.setDescription("0.1.96.1.130.255");

        obj = objFromCode(skipItem);
        dlms.read(obj, 0x2);
        int i = 0;
        final GXArray ga = (GXArray) obj.getValues()[1];
        final String[] result = new String[ga.size()];
        for (i = 0; i < result.length; i++) {
            result[i] = ((String) ((GXStructure) ga.get(i)).get(1)).replaceFirst("^0+(?!$)", "");
        }
        return result;
    }

    public void handleNotifyMessages(final GXReplyData reply) throws Exception {
        dlms.handleNotifyMessages(reply);
    }

    private boolean testReadExecuted = false;

    private void dumpObject(final String caption, final String ln, final int attributeIndex) throws Exception {
        log(Level.INFO, "Getting " + caption + "...");
        Object rest = dlms.read(dlms.getObject(ln), attributeIndex);
        log(Level.INFO, caption + ": " + rest == null ? "Null" : rest.toString());
    }

    public void testRead() {
        if (testReadExecuted) {
            return;
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU association view");
            try {
                dlms.getAssociationViewNoCache();
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "DCU association view operation error : " + exception.toString());
            }
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU COSEM Logical Device Name");
            GXDLMSData dcuCLDN = new GXDLMSData("0.0.42.0.0.255");
            dcuCLDN.setVersion(0);
            try {
                dlms.read(dcuCLDN, 2);
                LOGGER.log(Level.INFO, "DCU COSEM logical device name = " + Utils.lihatNilai(2, dcuCLDN.getValue()));
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "Read DCU COSEM logical device name error : " + exception.toString());
            }
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU Serial Number");
            GXDLMSData dcuSN = new GXDLMSData("0.0.96.1.0.255");
            dcuSN.setVersion(0);
            try {
                dlms.read(dcuSN, 2);
                LOGGER.log(Level.INFO, "DCU serial number = " + Utils.lihatNilai(2, dcuSN.getValue()));
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "Read DCU serial number error : " + exception.toString());
            }
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU Firmware Version");
            GXDLMSData dcuFWV = new GXDLMSData("1.0.0.2.0.255");
            dcuFWV.setVersion(0);
            try {
                dlms.read(dcuFWV, 2);
                LOGGER.log(Level.INFO, "DCU firmware version = " + Utils.lihatNilai(2, dcuFWV.getValue()));
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "Read DCU firmware version error : " + exception.toString());
            }
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU Time");
            GXDLMSClock waktuDCU = new GXDLMSClock("0.0.1.0.0.255");
            try {
                waktuDCU.setVersion(0);
                dlms.read(waktuDCU, 2);
                LOGGER.log(Level.INFO, "Waktu DCU = " + waktuDCU.getTime());
//                LOGGER.log(Level.INFO, "=======================================================");
//                log(Level.INFO, "                         Adjusting DCU time 1 year ahead");
//                Calendar calendar = waktuDCU.getTime().getLocalCalendar();
//                calendar.add(Calendar.YEAR, 1);
//                waktuDCU.setTime(calendar);
//                dlms.write(waktuDCU, 2);
//                LOGGER.log(Level.INFO, "Adjusting DCU time finished");
//                LOGGER.log(Level.INFO, "=======================================================");
//                LOGGER.log(Level.INFO, "                   Read DCU Time");
//                dlms.read(waktuDCU, 2);
//                LOGGER.log(Level.INFO, "DCU time after adjusting : " + waktuDCU.getTime());
//                LOGGER.log(Level.INFO, "=======================================================");
//                LOGGER.log(Level.INFO, "                         Restoring DCU clock");
//                calendar = waktuDCU.getTime().getLocalCalendar();
//                calendar.add(Calendar.YEAR, -1);
//                waktuDCU.setTime(calendar);
//                dlms.write(waktuDCU, 2);
//                LOGGER.log(Level.INFO, "Restoring DCU time finished");
//                LOGGER.log(Level.INFO, "=======================================================");
//                LOGGER.log(Level.INFO, "                         Read DCU Time");
//                dlms.read(waktuDCU, 2);
//                LOGGER.log(Level.INFO, "DCU time after restoring : " + waktuDCU.getTime());
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "DCU time operation error : " + exception.toString());
            }
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU Meter List");
            GXDLMSData mList = new GXDLMSData("0.0.96.54.1.255");
            mList.setVersion(0);
            mList.setDataType(2, DataType.STRUCTURE);
            try {
                List<MeterArchiveObj> meterArchiveObjs = new ArrayList<>();
//            MeterArchiveObj mao = new MeterArchiveObj("1708194510103", 1, 2, 1, 255 , 1);
//            meterArchiveObjs.add(mao);
//            mao = new MeterArchiveObj("1708194510104", 1, 2, 1, 255 , 1, "1708194510104");
//            meterArchiveObjs.add(mao);
//            mao = new MeterArchiveObj("1708194510105", 1, 2, 1, 255 , 1, "1708194510105");
//            meterArchiveObjs.add(mao);
//            mao = new MeterArchiveObj("888819090948", 1, 2, 1, 255 , 1, "22222222");
//            meterArchiveObjs.add(mao);
//            dlms.writeMeterList(mList, 2, meterArchiveObjs);
                dlms.read(mList, 2);
//            List<MeterArchiveObj> meterArchiveObjs = MeterArchiveObj.parseMeterList(mList.getValue());
                meterArchiveObjs = MeterArchiveObj.parseMeterList(mList.getValue());
                for (MeterArchiveObj meterArchiveObj : meterArchiveObjs) {
                    LOGGER.log(Level.INFO, meterArchiveObj.toString());
                }
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "DCU meter list operation error : " + exception.toString());
            }
//            List<MeterArchiveObj> meterArchiveObjs = new ArrayList<>();
//            MeterArchiveObj mao = new MeterArchiveObj("1708194510103", 1, 2, 1, 255 , 1);
//            meterArchiveObjs.add(mao);
//            mao = new MeterArchiveObj("1708194510104", 1, 2, 1, 255 , 1, "1708194510104");
//            meterArchiveObjs.add(mao);
//            mao = new MeterArchiveObj("19090948", 1, 2, 1, 255 , 1, "22222222");
//            meterArchiveObjs.add(mao);
//            dlms.writeMeterList(mList, 2, meterArchiveObjs);
//            dlms.read(mList, 2);
//            LOGGER.log(Level.INFO, "DCU meter list = " + Utils.lihatNilai(2, mList.getValue()));
////            String[] meterList = getMeterList();
//            log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            LOGGER.log(Level.INFO, "==============================================================================================================");
            LOGGER.log(Level.INFO, "                  Read DCU Profile");
            GXDLMSProfileGeneric dcuProfile = new GXDLMSProfileGeneric("1.0.99.1.0.255");
            dcuProfile.setVersion(1);
            try {
                dlms.read(dcuProfile, 7);
                LOGGER.log(Level.INFO, "DCU profile entries in use : " + dcuProfile.getEntriesInUse());
//                dlms.read(dcuProfile, 201);
//                LOGGER.log(Level.INFO, "DCU profile entries : " + dcuProfile.getProfileEntries());
//                dlms.read(dcuProfile, 3);
//                LOGGER.log(Level.INFO, "DCU profile channel size : " + dcuProfile.getCaptureObjects().size());
                int index = 1;
                for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : dcuProfile.getCaptureObjects()) {
                    LOGGER.log(Level.INFO, "\t" + index + ".\t" + captureObject.getKey().getLogicalName() + "("
                            + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + ")");
                    index++;
                }
                dlms.read(dcuProfile, 2);
                dlms.printProfileGenericBuffer(dcuProfile);
            } catch (Exception exception) {
                LOGGER.log(Level.SEVERE, "Read DCU profile error : " + exception.toString());
            }
            client.disconnectRequest(true);
//            for (String string : meterList) {
//                client.disconnectRequest(true);
            byte[] blockCipherKey = {(byte) 0x11, (byte) 0x11, (byte) 0x11, (byte) 0x11,
                (byte) 0x11, (byte) 0x11, (byte) 0x11, (byte) 0x11,
                (byte) 0x11, (byte) 0x11, (byte) 0x11, (byte) 0x11,
                (byte) 0x11, (byte) 0x11, (byte) 0x11, (byte) 0x11};
            byte[] kek = {(byte) 0x00, (byte) 0x11, (byte) 0x22, (byte) 0x33, (byte) 0x44, (byte) 0x55,
                (byte) 0x66, (byte) 0x77, (byte) 0x88, (byte) 0x99, (byte) 0xAA, (byte) 0xBB,
                (byte) 0xCC, (byte) 0xDD, (byte) 0xEE, (byte) 0xFF};
            Meter meter = new Meter("888819090948", true, Authentication.LOW, InterfaceType.WRAPPER, 1, 0,
                    1, 4, "22222222", Security.AUTHENTICATION_ENCRYPTION, "AUX00000", new String(blockCipherKey),
                    "3333333333333333", 150);
            try {
                Random rand = new Random();
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Perform public login with no security");
                meter.setAuthentication(Authentication.NONE);
                meter.setClientAddress(16);
                meter.printMeterInfo();
                setTargetMeter(meter.getNoMeter());
                dlms.setClient(meter);
                dlms.initializeConnection();
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter IC (0.0.43.1.0.255)");
                GXDLMSData meterIC = new GXDLMSData("0.0.43.1.0.255");
                meterIC.setVersion(0);
                try {
                    dlms.read(meterIC, 2);
                    LOGGER.log(Level.INFO, "Meter IC : " + Utils.lihatNilai(2, meterIC.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read meter IC error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
//====================================================LLS LOGIN===========================================
                meter.setAuthentication(Authentication.LOW);
                meter.setClientAddress(1);
                LOGGER.log(Level.INFO, "                         Perform " + meter.getAuthentication().toString()
                        + "-level security login");
                meter.printMeterInfo();
                setTargetMeter(meter.getNoMeter());
                dlms.setClient(meter);
                dlms.initializeConnection();
//====================================================HLS LOGIN===========================================
//                int IC = Integer.parseInt(meterIC.getValue().toString());
//                meter.setAuthentication(Authentication.HIGH_GMAC);
//                meter.setInvocation_counter(IC + 1);
//                meter.setClientAddress(1);
//                LOGGER.log(Level.INFO, "                         Perform " + meter.getAuthentication().toString()
//                        + "-level security login");
//                setTargetMeter(meter.getNoMeter());
//                dlms.setClient(meter);
//                meter.printMeterInfo();
//                dlms.initializeMeterConnection(meter.getNoMeter());
//                dlms.getAssociationViewNoCache();
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter Clock (0.0.1.0.0.255)");
                GXDLMSClock waktuMeter = new GXDLMSClock("0.0.1.0.0.255");
                try {
                    dlms.readMeter(waktuMeter, 0x2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter time : " + waktuMeter.getTime());
                    LOGGER.log(Level.INFO, "Meter time zone : " + waktuMeter.getTime().getMeterCalendar().
                            getTimeZone());
                    TimeZone timeZone = waktuMeter.getTime().getMeterCalendar().getTimeZone();
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    log(Level.INFO, "                         Adjusting Meter Time 1 Year Ahead");
//                    Calendar calendar = waktuMeter.getTime().getMeterCalendar();
//                    calendar.add(Calendar.YEAR, 1);
//                    calendar.setTimeZone(timeZone);
//                    waktuMeter.setTime(calendar);
//                    waktuMeter.getTime().getLocalCalendar().setTimeZone(timeZone);
//                    waktuMeter.getTime().getMeterCalendar().setTimeZone(timeZone);
//                    waktuMeter.getTime().getSkip().add(DateTimeSkips.MILLISECOND);
//                    dlms.writeMeter(waktuMeter, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Adjusting meter time finished");
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                   Read Meter Time");
//                    dlms.readMeter(waktuMeter, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter time after adjusting : " + waktuMeter.getTime());
//                    LOGGER.log(Level.INFO, "Meter time zone after adjusting : " + waktuMeter.getTime().
//                            getMeterCalendar().getTimeZone());
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Restoring Meter Time");
//                    calendar = waktuMeter.getTime().getLocalCalendar();
//                    calendar.add(Calendar.YEAR, -1);
//                    waktuMeter.setTime(calendar);
//                    dlms.writeMeter(waktuMeter, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Restoring meter time finished");
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Meter Time");
//                    dlms.readMeter(waktuMeter, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter time after restoring : " + waktuMeter.getTime());
//                    LOGGER.log(Level.INFO, "Meter time zone after restoring : " + waktuMeter.getTime().
//                            getMeterCalendar().getTimeZone());
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Meter clock operation error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter Type (0.0.96.1.1.255)");
                GXDLMSData meterType = new GXDLMSData("0.0.96.1.1.255");
                meterType.setVersion(0);
                try {
                    dlms.readMeter(meterType, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter type : " + Utils.lihatNilai(2, meterType.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read meter type error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter ID (0.0.96.1.0.255)");
                GXDLMSData nomorMeter = new GXDLMSData("0.0.96.1.0.255");
                nomorMeter.setVersion(0);
                try {
                    dlms.readMeter(nomorMeter, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter ID : " + Utils.lihatNilai(2, nomorMeter.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read meter ID error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Hardware Version (0.0.96.96.0.255)");
                GXDLMSData hWVersion = new GXDLMSData("0.0.96.96.0.255");
                hWVersion.setVersion(0);
                try {
                    dlms.readMeter(hWVersion, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Hardware version : " + Utils.lihatNilai(2, hWVersion.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read hardware version error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Software Version (1.0.0.2.0.255)");
                GXDLMSData sWVersion = new GXDLMSData("1.0.0.2.0.255");
                sWVersion.setVersion(0);
                try {
                    dlms.readMeter(sWVersion, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Software version : " + Utils.lihatNilai(2, sWVersion.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read software version error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Firmware Checksum (1.0.0.2.8.255)");
                GXDLMSData fwChecksum = new GXDLMSData("1.0.0.2.8.255");
                fwChecksum.setVersion(0);
                try {
                    dlms.readMeter(fwChecksum, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Firmware checksum : " + Utils.lihatNilai(2, fwChecksum.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read firmware checksum error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Customer ID (0.0.96.1.2.255)");
                GXDLMSData custID = new GXDLMSData("0.0.96.1.2.255");
                custID.setVersion(0);
                try {
                    dlms.readMeter(custID, 0x2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Customer ID : " + Utils.lihatNilai(2, custID.getValue()));
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Write Customer ID (0.0.96.1.2.255)");
//                    String nomor = "15000" + rand.nextInt(99999999);
//                    custID.setValue(nomor.getBytes());
//                    dlms.writeMeter(custID, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Set Customer ID to " + nomor);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Customer ID after write (0.0.96.1.2.255)");
//                    custID.setVersion(0);
//                    dlms.readMeter(custID, 0x2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Customer ID : " + Utils.lihatNilai(2, custID.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Customer ID operation error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Unit PLN ID (0.0.96.1.3.255)");
                GXDLMSData unitPLNID = new GXDLMSData("0.0.96.1.3.255");
                unitPLNID.setVersion(0);
                try {
                    dlms.readMeter(unitPLNID, 0x2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Unit PLN ID : " + Utils.lihatNilai(2, unitPLNID.getValue()));
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Write Unit PLN ID (0.0.96.1.3.255)");
//                    String nomor = rand.nextInt(15500) + "";
//                    unitPLNID.setValue(nomor.getBytes());
//                    dlms.writeMeter(unitPLNID, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Set unit PLN ID to " + nomor);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Unit PLN ID after write (0.0.96.1.3.255)");
//                    dlms.readMeter(unitPLNID, 0x2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Unit PLN ID : " + Utils.lihatNilai(2, unitPLNID.getValue()));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Unit PLN ID operation error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter Work Mode (0.0.96.5.96.255)");
                GXDLMSData meterWorkMode = new GXDLMSData("0.0.96.5.96.255");
                meterWorkMode.setVersion(0);
                try {
                    dlms.readMeter(meterWorkMode, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter work mode : "
                            + (Utils.lihatNilai(2, meterWorkMode.getValue()).equals("16") ? "Prepaid"
                            : (Utils.lihatNilai(2, meterWorkMode.getValue()).equals("18") ? "Postpaid"
                            : Utils.lihatNilai(2, meterWorkMode.getValue()).equals("19")
                            ? "Postpaid export-import" : "Unknown")));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read meter work mode error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Previous Meter Work Mode (0.0.96.5.97.255)");
                GXDLMSData prevMeterWorkMode = new GXDLMSData("0.0.96.5.97.255");
                prevMeterWorkMode.setVersion(0);
                try {
                    dlms.readMeter(prevMeterWorkMode, 2, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Previous meter work mode : "
                            + (Utils.lihatNilai(2, meterWorkMode.getValue()).equals("16") ? "Prepaid"
                            : (Utils.lihatNilai(2, meterWorkMode.getValue()).equals("18") ? "Postpaid"
                            : Utils.lihatNilai(2, meterWorkMode.getValue()).equals("19")
                            ? "Postpaid export-import" : "Unknown")));
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read previous meter work mode error : " + exception.toString());
                }

                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Meter Konstanta (1.0.0.3.0.255)");
                GXDLMSRegister meterKonstanta = new GXDLMSRegister("1.0.0.3.0.255");
                meterKonstanta.setVersion(0);
                try {
                    for (int pos : meterKonstanta.getAttributeIndexToRead(true)) {
                        dlms.readMeter(meterKonstanta, pos, meter.getNoMeter());
                    }
                    dlms.printRegister(meterKonstanta);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read meter konstanta error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Kapasitas Baterai (0.0.96.6.3.255)");
                GXDLMSRegister kapasitasBaterai = new GXDLMSRegister("0.0.96.6.3.255");
                kapasitasBaterai.setVersion(0);
                try {
                    for (int pos : kapasitasBaterai.getAttributeIndexToRead(true)) {
                        dlms.readMeter(kapasitasBaterai, pos, meter.getNoMeter());
                    }
                    dlms.printRegister(kapasitasBaterai);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read kapasitas baterai error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Kapasitas Superkapasitor (0.0.96.6.13.255)");
                GXDLMSRegister kapasitasSuperkapasitor = new GXDLMSRegister("0.0.96.6.13.255");
                kapasitasSuperkapasitor.setVersion(0);
                try {
                    for (int pos : kapasitasSuperkapasitor.getAttributeIndexToRead(true)) {
                        dlms.readMeter(kapasitasSuperkapasitor, pos, meter.getNoMeter());
                    }
                    dlms.printRegister(kapasitasSuperkapasitor);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read kapasitas superkapasitor error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Frekuensi (1.0.14.7.0.255)");
                GXDLMSRegister frekuensi = new GXDLMSRegister("1.0.14.7.0.255");
                frekuensi.setVersion(0);
                try {
                    for (int pos : frekuensi.getAttributeIndexToRead(true)) {
                        dlms.readMeter(frekuensi, pos, meter.getNoMeter());
                    }
                    dlms.printRegister(frekuensi);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read frekuensi error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Reset Meter (0.0.10.0.0.255)");
                GXDLMSScriptTable resetMeter = new GXDLMSScriptTable("0.0.10.0.0.255");
                resetMeter.setVersion(0);
                try {
                    for (int pos : resetMeter.getAttributeIndexToRead(true)) {
                        dlms.readMeter(resetMeter, pos, meter.getNoMeter());
                    }
                    dlms.printScriptTable(resetMeter);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read reset meter error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Clear All Tamper (0.0.10.0.128.255)");
                GXDLMSScriptTable clearAllTamper = new GXDLMSScriptTable("0.0.10.0.128.255");
                clearAllTamper.setVersion(0);
                try {
                    for (int pos : clearAllTamper.getAttributeIndexToRead(true)) {
                        dlms.readMeter(clearAllTamper, pos, meter.getNoMeter());
                    }
                    dlms.printScriptTable(clearAllTamper);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read clear all tamper error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Trigger Test (0.0.10.0.129.255)");
                GXDLMSScriptTable triggerTest = new GXDLMSScriptTable("0.0.10.0.129.255");
                triggerTest.setVersion(0);
                try {
                    for (int pos : triggerTest.getAttributeIndexToRead(true)) {
                        dlms.readMeter(triggerTest, pos, meter.getNoMeter());
                    }
                    dlms.printScriptTable(triggerTest);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read trigger test  error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read Event Push Script (0.0.10.0.130.255)");
                GXDLMSScriptTable eventPushScript = new GXDLMSScriptTable("0.0.10.0.130.255");
                eventPushScript.setVersion(0);
                try {
                    for (int pos : eventPushScript.getAttributeIndexToRead(true)) {
                        dlms.readMeter(eventPushScript, pos, meter.getNoMeter());
                    }
                    dlms.printScriptTable(eventPushScript);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Read event push script  error : " + exception.toString());
                }

//                LOGGER.log(Level.INFO, "==============================================================================================================");
//                LOGGER.log(Level.INFO, "                         Read Waktu Rekam Billing (0.0.15.0.0.255)");
//                int tglAsliBlling = 0;
//                GXDLMSActionSchedule actionSchedule = new GXDLMSActionSchedule("0.0.15.0.0.255");
//                actionSchedule.setDescription(ObisDescriptor.getDeskripsi("0.0.15.0.0.255"));
//                actionSchedule.setVersion(0);
//                try {
//                    for (int pos : actionSchedule.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(actionSchedule, pos, meter.getNoMeter());
//                    }
//                    GXDateTime executionTime = actionSchedule.getExecutionTime()[0];
//                    Calendar calendar = executionTime.getLocalCalendar();
//                    tglAsliBlling = calendar.get(Calendar.DATE);
//                    dlms.printActionSchedule(actionSchedule);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Write Waktu Rekam Billing (0.0.15.0.0.255)");
//                    GXDateTime waktuRekamBilling = actionSchedule.getExecutionTime()[0];
//                    int tanggal = rand.nextInt(28);
//                    waktuRekamBilling.getSkip().remove(DateTimeSkips.MILLISECOND);
//                    waktuRekamBilling.getMeterCalendar().set(Calendar.DATE, tanggal);
//                    waktuRekamBilling.getMeterCalendar().set(Calendar.MILLISECOND, 0);
//                    waktuRekamBilling.getLocalCalendar().set(Calendar.DATE, tanggal);
//                    waktuRekamBilling.getLocalCalendar().set(Calendar.MILLISECOND, 0);
//                    GXDateTime[] dateTimes = {waktuRekamBilling};
//                    actionSchedule.setExecutionTime(dateTimes);
//                    dlms.writeMeter(actionSchedule, 4, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Set date to " + tanggal);
//                    LOGGER.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
//                            new Object[]{"4", actionSchedule.getLogicalName(), actionSchedule.getDescription()});
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Waktu Rekam Billing (0.0.15.0.0.255) After Write");
//                    for (int pos : actionSchedule.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(actionSchedule, pos, meter.getNoMeter());
//                    }
//                    dlms.printActionSchedule(actionSchedule);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Set Back Waktu Rekam Billing (0.0.15.0.0.255)");
//                    waktuRekamBilling = actionSchedule.getExecutionTime()[0];
//                    waktuRekamBilling.getSkip().remove(DateTimeSkips.MILLISECOND);
//                    waktuRekamBilling.getMeterCalendar().set(Calendar.DATE, tglAsliBlling);
//                    waktuRekamBilling.getMeterCalendar().set(Calendar.MILLISECOND, 0);
//                    waktuRekamBilling.getLocalCalendar().set(Calendar.DATE, tglAsliBlling);
//                    waktuRekamBilling.getLocalCalendar().set(Calendar.MILLISECOND, 0);
//                    GXDateTime[] dateTimess = {waktuRekamBilling};
//                    actionSchedule.setExecutionTime(dateTimess);
//                    dlms.writeMeter(actionSchedule, 4, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Set date to " + tglAsliBlling);
//                    LOGGER.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
//                            new Object[]{"4", actionSchedule.getLogicalName(), actionSchedule.getDescription()});
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Waktu Rekam Billing (0.0.15.0.0.255) After Set Back");
//                    for (int pos : actionSchedule.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(actionSchedule, pos, meter.getNoMeter());
//                    }
//                    dlms.printActionSchedule(actionSchedule);
//                } catch (Exception exception) {
//                    LOGGER.log(Level.SEVERE, "Waktu rekam billing operation error : " + exception.toString());
//                }
//                LOGGER.log(Level.INFO, "==============================================================================================================");
//                LOGGER.log(Level.INFO, "                         Read Load Profile Interval (1.0.0.8.4.255)");
//                GXDLMSRegister lPInterval = new GXDLMSRegister("1.0.0.8.4.255");
//                lPInterval.setVersion(0);
//                try {
//                    for (int pos : lPInterval.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(lPInterval, pos, meter.getNoMeter());
//                    }
//                    dlms.printRegister(lPInterval);
//                    int loadProfileInterval = 15;
//                    if ((int) lPInterval.getValue() == 15) {
//                        loadProfileInterval = 30;
//                    }
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Write Load Profile Interval (1.0.0.8.4.255)");
//                    String nomor = loadProfileInterval + "";
//                    lPInterval.setValue(loadProfileInterval);
//                    dlms.writeMeter(lPInterval, 2, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Set load profile interval to " + nomor);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read Load Profile Interval (1.0.0.8.4.255) after write");
//                    for (int pos : lPInterval.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(lPInterval, pos, meter.getNoMeter());
//                    }
//                    dlms.printRegister(lPInterval);
//                } catch (Exception exception) {
//                    LOGGER.log(Level.SEVERE, "Load profile interval operation error : " + exception.toString());
//                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Limiter (0.0.17.0.0.255)");
                LOGGER.log(Level.INFO, "=======================================================");
                LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Active");
                GXDLMSLimiter limiter = new GXDLMSLimiter("0.0.17.0.0.255");
                try {
                    dlms.readMeter(limiter, 0x3, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold active : " + limiter.getThresholdActive().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Normal");
                    dlms.readMeter(limiter, 0x4, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold normal : " + limiter.getThresholdNormal().toString());
                    int thresholdNormal = Integer.parseInt(limiter.getThresholdNormal().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Limiter (0.0.17.0.0.255) Threshold Normal to 1000");
                    limiter.setThresholdNormal(1000);
                    dlms.writeMeter(limiter, 4, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter threshold normal finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Normal After Changed");
                    dlms.readMeter(limiter, 0x4, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold normal : " + limiter.getThresholdNormal().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Back Limiter (0.0.17.0.0.255) Threshold Normal to its original value");
                    limiter.setThresholdNormal(thresholdNormal);
                    dlms.writeMeter(limiter, 4, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter threshold normal finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Normal After Changed");
                    dlms.readMeter(limiter, 0x4, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold normal : " + limiter.getThresholdNormal().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Emergency");
                    dlms.readMeter(limiter, 0x5, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold emergency : " + limiter.getThresholdEmergency().toString());
                    int thresholdEmergency = Integer.parseInt(limiter.getThresholdEmergency().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Limiter (0.0.17.0.0.255) Threshold emergency to 1000");
                    limiter.setThresholdEmergency(1000);
                    dlms.writeMeter(limiter, 5, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter threshold emergency finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Emergency After Changed");
                    dlms.readMeter(limiter, 0x5, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold emergency : " + limiter.getThresholdEmergency().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Back Limiter (0.0.17.0.0.255) Threshold Emergency to its original value");
                    limiter.setThresholdEmergency(thresholdEmergency);
                    dlms.writeMeter(limiter, 5, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter threshold emergency finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Threshold Emergency After Changed");
                    dlms.readMeter(limiter, 0x5, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter threshold emergency : " + limiter.getThresholdEmergency().toString());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Over ThresholdDuration");
                    dlms.readMeter(limiter, 0x6, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min over threshold duration : " + limiter.getMinOverThresholdDuration() + " s");
                    int minOverThresholdDuration = Integer.parseInt(limiter.getMinOverThresholdDuration() + "");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Limiter (0.0.17.0.0.255) Min Over ThresholdDuration to 1000");
                    limiter.setMinOverThresholdDuration(1000);
                    dlms.writeMeter(limiter, 6, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write min over threshold duration finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Over ThresholdDuration After Changed");
                    dlms.readMeter(limiter, 0x6, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min over threshold duration : " + limiter.getMinOverThresholdDuration() + " s");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Back Limiter (0.0.17.0.0.255) Min Over ThresholdDuration to its original value");
                    limiter.setMinOverThresholdDuration(minOverThresholdDuration);
                    dlms.writeMeter(limiter, 6, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter min over threshold duration finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Over ThresholdDuration After Changed");
                    dlms.readMeter(limiter, 0x6, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min over threshold duration : " + limiter.getMinOverThresholdDuration() + " s");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Under ThresholdDuration");
                    dlms.readMeter(limiter, 0x7, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min under threshold duration : " + limiter.getMinUnderThresholdDuration() + " s");
                    int minUnderThresholdDuration = Integer.parseInt(limiter.getMinUnderThresholdDuration() + "");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Limiter (0.0.17.0.0.255) Min Under ThresholdDuration to 1000");
                    limiter.setMinUnderThresholdDuration(1000);
                    dlms.writeMeter(limiter, 7, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write min under threshold duration finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Under ThresholdDuration After Changed");
                    dlms.readMeter(limiter, 0x7, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min under threshold duration : " + limiter.getMinUnderThresholdDuration() + " s");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Back Limiter (0.0.17.0.0.255) Min Under ThresholdDuration to its original value");
                    limiter.setMinUnderThresholdDuration(minUnderThresholdDuration);
                    dlms.writeMeter(limiter, 7, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter min under threshold duration finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Min Under ThresholdDuration After Changed");
                    dlms.readMeter(limiter, 0x7, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter min under threshold duration : " + limiter.getMinUnderThresholdDuration() + " s");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Emergency Profile");
                    dlms.readMeter(limiter, 0x8, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile ID : "
                            + limiter.getEmergencyProfile().getID());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile activation time : "
                            + limiter.getEmergencyProfile().getActivationTime().toString());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile duration : "
                            + limiter.getEmergencyProfile().getDuration());
                    GXDateTime activationTimeDate = limiter.getEmergencyProfile().getActivationTime();
                    long emergencyProfileDuration = limiter.getEmergencyProfile().getDuration();
                    int emergencyProfileID = limiter.getEmergencyProfile().getID();
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Write Limiter (0.0.17.0.0.255) Emergency Profile");
                    limiter.getEmergencyProfile().setID(emergencyProfileID);
                    limiter.getEmergencyProfile().setDuration(Long.parseLong("60"));
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.YEAR, 5);
                    GXDateTime dateTime = new GXDateTime(calendar);
                    limiter.getEmergencyProfile().setActivationTime(dateTime);
                    dlms.writeMeter(limiter, 8, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter emergency profile finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Emergency Profile After Changed");
                    dlms.readMeter(limiter, 0x8, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile ID : "
                            + limiter.getEmergencyProfile().getID());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile activation time : "
                            + limiter.getEmergencyProfile().getActivationTime().toString());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile duration : "
                            + limiter.getEmergencyProfile().getDuration());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Set Back Limiter (0.0.17.0.0.255) Emergency Profile");
                    limiter.getEmergencyProfile().setID(emergencyProfileID);
                    limiter.getEmergencyProfile().setDuration(emergencyProfileDuration);
                    limiter.getEmergencyProfile().setActivationTime(activationTimeDate);
                    dlms.writeMeter(limiter, 8, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter write limiter emergency profile finished");
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read Limiter (0.0.17.0.0.255) Emergency Profile After Set Back");
                    dlms.readMeter(limiter, 0x8, meter.getNoMeter());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile ID : "
                            + limiter.getEmergencyProfile().getID());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile activation time : "
                            + limiter.getEmergencyProfile().getActivationTime().toString());
                    LOGGER.log(Level.INFO, "Meter limiter emergency profile duration : "
                            + limiter.getEmergencyProfile().getDuration());
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Limiter operation error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Read TOU (0.0.13.0.0.255)");
                GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar();
                activityCalendar.setLogicalName("0.0.13.0.0.255");
                activityCalendar.setVersion(0);
                activityCalendar.setDescription("TOU");
                try {
                    for (int pos : activityCalendar.getAttributeIndexToRead(true)) {
                        dlms.readMeter(activityCalendar, pos, meter.getNoMeter());
                    }
                    dlms.printActivityCalendar(activityCalendar);
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Write TOU (0.0.13.0.0.255)");
//                    GXTime gXTime = new GXTime(22, 0, 0, 0);
//                    GXDLMSDayProfileAction dayProfileAction = new GXDLMSDayProfileAction();
//                    dayProfileAction.setScriptLogicalName("0.0.10.0.100.255");
//                    dayProfileAction.setScriptSelector(1);
//                    dayProfileAction.setStartTime(gXTime);
//                    boolean ada = false;
//                    for (int i = 0; i < activityCalendar.getDayProfileTablePassive().length; i++) {
//                        for (int j = 0; j < activityCalendar.getDayProfileTablePassive()[i].
//                                getDaySchedules().length; j++) {
//                            activityCalendar.getDayProfileTablePassive()[i].
//                                    getDaySchedules()[j].getStartTime().getSkip().
//                                    remove(DateTimeSkips.MILLISECOND);
//                            activityCalendar.getDayProfileTablePassive()[i].
//                                    getDaySchedules()[j].getStartTime().getLocalCalendar().
//                                    set(Calendar.MILLISECOND, 0);
//                            activityCalendar.getDayProfileTablePassive()[i].
//                                    getDaySchedules()[j].getStartTime().getMeterCalendar().
//                                    set(Calendar.MILLISECOND, 0);
//                        }
//                        if (activityCalendar.getDayProfileTablePassive()[i].getDayId() == 2) {
//                            ada = true;
//                            GXDLMSDayProfileAction[] dayProfileActions = new GXDLMSDayProfileAction[activityCalendar.getDayProfileTablePassive()[i].getDaySchedules().length + 1];
//                            for (int j = 0; j < dayProfileActions.length; j++) {
//                                if (j == activityCalendar.getDayProfileTablePassive()[i].getDaySchedules().length) {
//                                    dayProfileAction.getStartTime().getSkip().remove(DateTimeSkips.MILLISECOND);
//                                    dayProfileAction.getStartTime().getLocalCalendar().
//                                            set(Calendar.MILLISECOND, 0);
//                                    dayProfileAction.getStartTime().getMeterCalendar().
//                                            set(Calendar.MILLISECOND, 0);
//                                    dayProfileActions[j] = dayProfileAction;
//                                } else {
//                                    activityCalendar.getDayProfileTablePassive()[i].
//                                            getDaySchedules()[j].getStartTime().getSkip().
//                                            remove(DateTimeSkips.MILLISECOND);
//                                    activityCalendar.getDayProfileTablePassive()[i].
//                                            getDaySchedules()[j].getStartTime().getLocalCalendar().
//                                            set(Calendar.MILLISECOND, 0);
//                                    activityCalendar.getDayProfileTablePassive()[i].
//                                            getDaySchedules()[j].getStartTime().getMeterCalendar().
//                                            set(Calendar.MILLISECOND, 0);
//                                    dayProfileActions[j] = activityCalendar.getDayProfileTablePassive()[i].
//                                            getDaySchedules()[j];
//                                }
//                            }
//                            activityCalendar.getDayProfileTablePassive()[i].setDaySchedules(dayProfileActions);
//                        }
//                    }
//                    if (ada) {
//                        LOGGER.log(Level.INFO, "TOU set :");
//                        dlms.printActivityCalendar(activityCalendar);
//                        dlms.writeMeter(activityCalendar, 9, meter.getNoMeter());
//                        LOGGER.log(Level.INFO, "Meter write TOU finished");
//                    } else {
//                        LOGGER.log(Level.SEVERE, "Day ID tidak ada");
//                    }
//                    LOGGER.log(Level.INFO, "=======================================================");
//                    LOGGER.log(Level.INFO, "                         Read TOU (0.0.13.0.0.255)");
//                    for (int pos : activityCalendar.getAttributeIndexToRead(true)) {
//                        dlms.readMeter(activityCalendar, pos, meter.getNoMeter());
//                    }
//                    dlms.printActivityCalendar(activityCalendar);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "TOU operation error : " + exception.toString());
                }
                LOGGER.log(Level.INFO, "==============================================================================================================");
                LOGGER.log(Level.INFO, "                         Change security setup (0.0.43.0.0.255)");
                GXDLMSSecuritySetup dLMSSecuritySetup = new GXDLMSSecuritySetup("0.0.43.0.0.255");
                dLMSSecuritySetup.setVersion(1);
                try {
                    List<GXSimpleEntry<GlobalKeyType, byte[]>> entrys = new ArrayList<>();
                    GXSimpleEntry<GlobalKeyType, byte[]> entry = new GXSimpleEntry(GlobalKeyType.AUTHENTICATION,
                            "1111111111111111".getBytes());
                    entrys.add(entry);
//                    entry = new GXSimpleEntry(GlobalKeyType.UNICAST_ENCRYPTION, 
//                            "1111111111111111".getBytes());
//                    entrys.add(entry);
                    dlms.readDLMSPacketMeter(dLMSSecuritySetup.globalKeyTransfer(dlms.getClient(), kek, entrys),
                            meter.getNoMeter());
                    LOGGER.log(Level.SEVERE, "change securitykey success, new Authentication key is 1111111111111111");
                    LOGGER.log(Level.INFO, "Disconnecting " + meter.getNoMeter());

                    //====================================================HLS LOGIN===========================================
                    try {
                        int IC = Integer.parseInt(meterIC.getValue().toString());
                        meter.setAuthentication(Authentication.HIGH_GMAC);
                        meter.setInvocation_counter(IC + 1);
                        meter.setClientAddress(1);
                        LOGGER.log(Level.INFO, "Perform " + meter.getAuthentication().toString()
                                + "-level security login with old setup");
                        setTargetMeter(meter.getNoMeter());
                        dlms.setClient(meter);
                        meter.printMeterInfo();
                        dlms.initializeMeterConnection(meter.getNoMeter());
                    } catch (Exception ex) {
                        LOGGER.log(Level.INFO, "Perform " + meter.getAuthentication().toString()
                                + "-level security login with old setup failed : " + ex.toString());
                        try {
                            int IC = Integer.parseInt(meterIC.getValue().toString());
                            meter.setAuthentication(Authentication.HIGH_GMAC);
                            meter.setInvocation_counter(IC + 1);
                            meter.setClientAddress(1);
                            meter.setAuthenticationKey("1111111111111111");
                            LOGGER.log(Level.INFO, "Perform " + meter.getAuthentication().toString()
                                    + "-level security login with new setup");
                            setTargetMeter(meter.getNoMeter());
                            dlms.setClient(meter);
                            meter.printMeterInfo();
                            dlms.initializeMeterConnection(meter.getNoMeter());
                            LOGGER.log(Level.INFO, "==============================================================================================================");
                            LOGGER.log(Level.INFO, "Read Meter Clock (0.0.1.0.0.255) with new security");
                            try {
                                dlms.readMeter(waktuMeter, 0x2, meter.getNoMeter());
                                LOGGER.log(Level.INFO, "Meter time : " + waktuMeter.getTime());
                                LOGGER.log(Level.INFO, "Meter time zone : " + waktuMeter.getTime().getMeterCalendar().
                                        getTimeZone());
                            } catch (Exception exc) {
                                LOGGER.log(Level.SEVERE, "Read Meter Clock (0.0.1.0.0.255) with new security failed : " +
                                        exc.toString());
                            }
                        } catch (Exception e) {
                            LOGGER.log(Level.INFO, "Perform " + meter.getAuthentication().toString()
                                    + "-level security login with new setup failed : " + e.toString());
                        }
                    }
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, "Change security setup : " + exception.toString());
                }
//                LOGGER.log(Level.INFO, "==============================================================================================================");
//                LOGGER.log(Level.INFO, "                         Read Load Profile (1.0.99.1.0.255)");
//                GXDLMSProfileGeneric lP = new GXDLMSProfileGeneric("1.0.99.1.0.255");
//                lP.setVersion(1);
//                try {
//                    dlms.readMeter(lP, 7, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " load profile entries in use : "
//                            + lP.getEntriesInUse());
//                    dlms.readMeter(lP, 8, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " profile entries : " + lP.getProfileEntries());
//                    dlms.readMeter(lP, 3, meter.getNoMeter());
//                    dlms.addCollectionMeter(lP, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " load profile channel size : "
//                            + lP.getCaptureObjects().size());
//                    int index = 1;
//                    for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : lP.getCaptureObjects()) {
//                        LOGGER.log(Level.INFO, "\t" + index + ".\t" + captureObject.getKey().getLogicalName() + "("
//                                + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + ")");
//                        index++;
//                    }
//                    dlms.readMeter(waktuMeter, 2, meter.getNoMeter());
//                    Calendar start = waktuMeter.getTime().getLocalCalendar();
//                    start.set(Calendar.HOUR_OF_DAY, 0);
//                    start.set(Calendar.MINUTE, 0);
//                    start.set(Calendar.SECOND, 0);
//                    Calendar end = waktuMeter.getTime().getMeterCalendar();
//                    dlms.readRowsByRangeMeter(lP, start, end, meter.getNoMeter());
////                    dlms.readMeter(lP, 2, meter.getNoMeter());
//                    dlms.printProfileGenericBuffer(lP);
//                } catch (Exception exception) {
//                    LOGGER.log(Level.SEVERE, "                         Read load profile error : " + exception.toString());
//                }
//                LOGGER.log(Level.INFO, "==============================================================================================================");
//                LOGGER.log(Level.INFO, "Read End of Billing (0.0.98.1.0.255)");
//                GXDLMSProfileGeneric eob = new GXDLMSProfileGeneric("0.0.98.1.0.255");
//                eob.setVersion(1);
//                try {
//                    dlms.readMeter(eob, 7, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " end of billing entries in use : " + eob.getEntriesInUse());
//                    dlms.readMeter(eob, 8, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " end of billing entries : " + eob.getProfileEntries());
//                    dlms.readMeter(eob, 3, meter.getNoMeter());
//                    LOGGER.log(Level.INFO, "Meter " + meter.getNoMeter() + " end of billing channel size : " + eob.getCaptureObjects().size());
//                    int index = 1;
//                    dlms.addCollectionMeter(eob, meter.getNoMeter());
//                    for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : eob.getCaptureObjects()) {
//                        LOGGER.log(Level.INFO, "\t" + index + ".\t" + captureObject.getKey().getLogicalName() + "("
//                                + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + ")");
//                        index++;
//                    }
//                    dlms.readMeter(eob, 2, meter.getNoMeter());
//                    dlms.printProfileGenericBuffer(eob);
//                } catch (Exception exception) {
//                    LOGGER.log(Level.SEVERE, "Read end of billing error : " + exception.toString());
//                }
                client.disconnectRequest();
            } catch (Exception exception) {
                client.disconnectRequest();
                LOGGER.log(Level.SEVERE, "Disconnecting " + meter.getNoMeter() + " : "
                        + exception.toString());
            }
//            }
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
        }
    }

    public List<String> ReadListMeter(final String AuthMeter) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeter, true);

            // setTargetMeter("24915104533", AuthMeter);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
                if (e.getMessage().equals("Failed to receive reply from the device in given time.")
                        || e.getMessage().equals("Connection is permanently rejected")) {
                    testReadExecuted = false;
                    return result(e.getMessage(), "", 0);
                }
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXObisCode skipItem = new GXObisCode();
            // skipItem.setObjectType(ObjectType.CLOCK);
            // skipItem.setLogicalName("0.0.1.0.0.255");
            // skipItem.setVersion(0);
            // skipItem.setDescription("0.0.1.0.0.255");
            // GXDLMSClock obj1 = new GXDLMSClock();
            // obj1.setObjectType((ObjectType) skipItem.getObjectType());
            // obj1.setLogicalName(skipItem.getLogicalName());
            // obj1.setDescription(skipItem.getDescription());
            //     GXDLMSObject obj = objFromCode(skipItem);
            // Object rest = dlms.read(obj, 2);
            // log(Level.INFO, "Clock..." + ": " + rest==null?"Null":rest.toString());
            // GXDLMSObject  obj = new GXDLMSObject();
            // GXObisCode skipItem = new GXObisCode();
            // 	skipItem.setObjectType(ObjectType.forValue(3));
            // 	skipItem.setLogicalName("1.0.73.25.0.255");
            // 	skipItem.setVersion(0);
            //     skipItem.setDescription("1.0.73.25.0.255");
            //     obj = objFromCode(skipItem);
            //     // GXDLMSObject obj = obj1;
            // Object rest = dlms.read(obj, 2);
            // log(Level.INFO, "1.0.73.25.0.255..." + ": " + rest==null?"Null":rest.toString());
            //List Meter
            //   obj = new GXDLMSObject();
            //  skipItem = new GXObisCode();
            // 	skipItem.setObjectType(ObjectType.forValue(1));
            // 	skipItem.setLogicalName("0.0.96.81.0.255");
            // 	skipItem.setVersion(0);
            //     skipItem.setDescription("0.0.96.81.0.255");
            //     obj = objFromCode(skipItem);
            //     // GXDLMSObject obj = obj1;
            // rest = dlms.read(obj, 2);
            // log(Level.INFO, "0.0.96.81.0.255..." + ": " + rest==null?"Null":rest.toString());
//            dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // final String[] meterList = new String[1];//getMeterList();
            final String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));

            // log(Level.INFO, "======= M E T E R T E S T =======");
            client.disconnectRequest(true);
            testReadExecuted = false;
            return result("Sukses", Arrays.toString(meterList), 1);
// return result("Sukses", "", 1);
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadTestDCUGlobal(final String AuthMeter, String obis) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        String[] obisx = obis.split("-");
        String classId = obisx[0];
        String logicalName = obisx[1];
        String version = obisx[2];
        String attribut = obisx[3];
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeter, true);

            // setTargetMeter("24915104533", AuthMeter);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
                if (e.getMessage().equals("Failed to receive reply from the device in given time.")
                        || e.getMessage().equals("Connection is permanently rejected")) {
                    testReadExecuted = false;
                    return result(e.getMessage(), "", 0);
                }
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            if (!classId.equals("7")) {
                GXDLMSObject obj = new GXDLMSObject();
                String datax = "";
                log(Level.INFO, "Get Obis ...." + logicalName);

                GXObisCode skipItem = new GXObisCode();
                skipItem.setObjectType(ObjectType.forValue(Integer.parseInt(classId)));
                skipItem.setLogicalName(logicalName);
                skipItem.setVersion(Integer.parseInt(version));
                skipItem.setDescription(logicalName);

                obj = objFromCode(skipItem);

                // GXDLMSObject obj = obj1;
                List<ResultRead> rest = dlms.getRegister(skipItem);

                for (int j = 0; j < rest.size(); j++) {
                    log(Level.INFO, logicalName + "..." + ">> " + dlms.ifNull(rest.get(j).getAlias()) + " : " + dlms.convertData(rest.get(j)));
                    datax = datax + " Data ke-" + (j + 1) + " : " + dlms.ifNull(rest.get(j).getAlias()) + " : " + dlms.convertData(rest.get(j)) + "; ";
                }
                client.disconnectRequest(true);
                testReadExecuted = false;
                return result("Sukses", datax, 1);
            } else {
                List<JSONObject> Ljson = new ArrayList<JSONObject>();
                log(Level.INFO, "Getting load Profile...");
                final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
                pg.setLogicalName(logicalName);
                pg.setObjectType(ObjectType.PROFILE_GENERIC);
                pg.setDescription(logicalName);
                pg.setVersion(Integer.parseInt(version));

                GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
                pg2.setLogicalName(logicalName);
                pg2.setObjectType(ObjectType.PROFILE_GENERIC);
                pg2.setDescription(logicalName);
                pg2.setVersion(Integer.parseInt(version));

                dlms.getProfileGenericColumnsById(pg);
                if (pg.getCaptureObjects().size() > 0) {
                    final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                    if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName("0.0.1.0.0.255");
                        dt.setObjectType(ObjectType.DATA);
                        dt.setUIDataType(2, DataType.DATETIME);
                        pg2.addCaptureObject(dt, 2, 0);

                        dt = new GXDLMSObject();
                        dt.setLogicalName("1.0.0.8.4.255");
                        dt.setObjectType(ObjectType.DATA);
                        pg2.addCaptureObject(dt, 2, 0);

                        for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                            pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                    it.getValue().getDataIndex());
                        }
                    } else {
                        pg2 = pg;
                    }

                    final Calendar startLP = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    Calendar endLp = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    startLP.add(java.util.Calendar.DATE, -1);
//                    startLP.setTime(sdf.parse(tanggalAwal));
//                    endLp.setTime(sdf.parse(tanggalAkhir));
                    Ljson = dlms.getProfileGenericsByDates(pg2, startLP, endLp);
                    client.disconnectRequest(true);
                    testReadExecuted = false;

                    return result("Sukses", Ljson.toArray(), 1);
                }
                client.disconnectRequest(true);
                testReadExecuted = false;

                return result("Gagal", Ljson.toArray(), 1);
            }
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> WriteTestDCUGlobal(final String AuthMeter, String obis, String tanggal) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        String[] obisx = obis.split("-");
        String classId = obisx[0];
        String logicalName = obisx[1];
        String version = obisx[2];
        String attribut = obisx[3];
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeter, true);

            // setTargetMeter("24915104533", AuthMeter);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
                if (e.getMessage().equals("Failed to receive reply from the device in given time.")
                        || e.getMessage().equals("Connection is permanently rejected")) {
                    testReadExecuted = false;
                    return result(e.getMessage(), "", 0);
                }
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            String rslt = "";
            try {
                GXDLMSObject obj = new GXDLMSObject();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "Getting meter clock...");

                final Calendar calendar = Calendar.getInstance();
                final SimpleDateFormat sdfN = new SimpleDateFormat("yyyyMMddHHmmss");

                final java.util.Calendar tgl = java.util.Calendar.getInstance();
                calendar.setTime(sdfN.parse(tanggal));
                Object data_ob = calendar.getTime();
                DataType dataType = DataType.OCTET_STRING;
                System.out.println("Start Write Date.. " + calendar.getTime().toString());
                dlms.readDLMSPacket(dlms.getClient().write("0.0.1.0.0.255", data_ob, dataType, dlms.getObjectType("8"), 2));
                System.out.println("End Write Date.. ");
                rslt = "";
            } catch (Exception exc) {
                System.out.println(exc.getMessage());
            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;

                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> WriteTestMeterGlobal(final String noMeter, final String AuthMeter, String obis, String tanggal) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        String[] obisx = obis.split("-");
        String classId = obisx[0];
        String logicalName = obisx[1];
        String version = obisx[2];
        String attribut = obisx[3];
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeter, true);

            // setTargetMeter("24915104533", AuthMeter);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
                if (e.getMessage().equals("Failed to receive reply from the device in given time.")
                        || e.getMessage().equals("Connection is permanently rejected")) {
                    testReadExecuted = false;
                    return result(e.getMessage(), "", 0);
                }
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            client.disconnectRequest(true);

            setTargetMeter(noMeter, AuthMeter, false);
            client.disconnectRequest(true);

            String rslt = "";
            try {
                GXDLMSObject obj = new GXDLMSObject();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "Getting meter clock...");

                final Calendar calendar = Calendar.getInstance();
                final SimpleDateFormat sdfN = new SimpleDateFormat("yyyyMMddHHmmss");

                final java.util.Calendar tgl = java.util.Calendar.getInstance();
                calendar.setTime(sdfN.parse(tanggal));
                Object data_ob = calendar.getTime();
                DataType dataType = DataType.OCTET_STRING;
                System.out.println("Start Write Date.. " + calendar.getTime().toString());
                dlms.readDLMSPacket(dlms.getClient().write("0.0.1.0.0.255", data_ob, dataType, dlms.getObjectType("8"), 2));
                System.out.println("End Write Date.. ");
                rslt = "";
            } catch (Exception exc) {
                System.out.println(exc.getMessage());
            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;

                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadTestMeterGlobal(final String noMeter, final String AuthMeter, String obis) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        String[] obisx = obis.split("-");
        String classId = obisx[0];
        String logicalName = obisx[1];
        String version = obisx[2];
        String attribut = obisx[3];
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, AuthMeter, true);

            // setTargetMeter("24915104533", AuthMeter);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
                if (e.getMessage().equals("Failed to receive reply from the device in given time.")
                        || e.getMessage().equals("Connection is permanently rejected")) {
                    testReadExecuted = false;
                    return result(e.getMessage(), "", 0);
                }
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            client.disconnectRequest(true);

            // GXDLMSObject obj;
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, AuthMeter, false);
            client.disconnectRequest(true);

            if (!classId.equals("7")) {
                GXDLMSObject obj = new GXDLMSObject();
                String datax = "";
                log(Level.INFO, "Get Obis ...." + logicalName);

                GXObisCode skipItem = new GXObisCode();
                skipItem.setObjectType(ObjectType.forValue(Integer.parseInt(classId)));
                skipItem.setLogicalName(logicalName);
                skipItem.setVersion(Integer.parseInt(version));
                skipItem.setDescription(logicalName);

                obj = objFromCode(skipItem);

                // GXDLMSObject obj = obj1;
                List<ResultRead> rest = dlms.getRegister(skipItem);

                for (int j = 0; j < rest.size(); j++) {
                    log(Level.INFO, logicalName + "..." + ">> " + dlms.ifNull(rest.get(j).getAlias()) + " : " + dlms.convertData(rest.get(j)));
                    datax = datax + " Data ke-" + (j + 1) + " : " + dlms.ifNull(rest.get(j).getAlias()) + " : " + dlms.convertData(rest.get(j)) + "; ";
                }
                client.disconnectRequest(true);
                testReadExecuted = false;
                return result("Sukses", datax, 1);
            } else {
                List<JSONObject> Ljson = new ArrayList<JSONObject>();
                log(Level.INFO, "Getting load Profile...");
                final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
                pg.setLogicalName(logicalName);
                pg.setObjectType(ObjectType.PROFILE_GENERIC);
                pg.setDescription(logicalName);

                GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
                pg2.setLogicalName(logicalName);
                pg2.setObjectType(ObjectType.PROFILE_GENERIC);
                pg2.setDescription(logicalName);

                dlms.getProfileGenericColumnsById(pg);
                if (pg.getCaptureObjects().size() > 0) {
                    final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                    if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName("0.0.1.0.0.255");
                        dt.setObjectType(ObjectType.DATA);
                        dt.setUIDataType(2, DataType.DATETIME);
                        pg2.addCaptureObject(dt, 2, 0);

                        dt = new GXDLMSObject();
                        dt.setLogicalName("1.0.0.8.4.255");
                        dt.setObjectType(ObjectType.DATA);
                        pg2.addCaptureObject(dt, 2, 0);

                        for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                            pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                    it.getValue().getDataIndex());
                        }
                    } else {
                        pg2 = pg;
                    }

                    final Calendar startLP = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    Calendar endLp = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    startLP.add(java.util.Calendar.DATE, -1);
//                    startLP.setTime(sdf.parse(tanggalAwal));
//                    endLp.setTime(sdf.parse(tanggalAkhir));
                    Ljson = dlms.getProfileGenericsByDates(pg2, startLP, endLp);
                    client.disconnectRequest(true);
                    testReadExecuted = false;

                    return result("Sukses", Ljson.toArray(), 1);
                }
                client.disconnectRequest(true);
                testReadExecuted = false;

                return result("Gagal", Ljson.toArray(), 1);
            }
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadClock(final String noMeter, final String AuthMeterMeter, final String AuthMeterDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, AuthMeterDCU, true);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();
            try {
                dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            // ..readScalerAndUnits();
            // GXDLMSObject obj;

            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            dlms.sendListMeter();

            // dlms.sendMeter();
            System.out.println("==============");
            client.disconnectRequest(true);

            GXDLMSObject obj;
            // setTargetMeter(noMeter);

            setTargetMeter(noMeter, AuthMeterMeter, false);
            client.disconnectRequest(true);

            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                // log(Level.INFO, "Getting meter clock...");
                obj = client.getObjects().findByLN(ObjectType.CLOCK, "0.0.1.0.0.255");
                if (obj != null) {
                    final GXDLMSClock clock = (GXDLMSClock) obj;
                    log(Level.INFO, "=======  M E T E R   R E A D   C L O C K  =======");
                    dlms.read(obj, 0x2);
                    log(Level.INFO, "Initial meter clock: " + clock.getTime().toString());
                    rslt = clock.getTime().toString();
                } else {
                    GXObisCode skipItem = new GXObisCode();
                    skipItem.setObjectType(ObjectType.CLOCK);
                    skipItem.setLogicalName("0.0.1.0.0.255");
                    skipItem.setVersion(0);
                    skipItem.setDescription("0.0.1.0.0.255");

                    GXDLMSClock obj1 = new GXDLMSClock();
                    obj1.setObjectType((ObjectType) skipItem.getObjectType());
                    obj1.setLogicalName(skipItem.getLogicalName());
                    obj1.setDescription(skipItem.getDescription());

                    obj = objFromCode(skipItem);
                    Object rest = dlms.read(obj, 2);
                    log(Level.INFO, "Clock..." + ": " + rest == null ? "Null" : rest.toString());
                    rslt = rest.toString();
                }

                // rslt = sdf.format(clock.getTime().getLocalCalendar());
                //
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                // client.disconnectRequest(true);
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> writeClock(final String noMeter, final String tanggal, final String AuthMeterMeter,
            final String AuthMeterDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(noMeter, AuthMeterDCU, true);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;
            dlms.sendListMeter();
            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);

            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            log(Level.INFO, "=======  M E T E R    W R I T E  =======");
            log(Level.INFO, "Write Clock " + tanggal);
            client.disconnectRequest(true);

            GXDLMSObject obj;
            setTargetMeter(noMeter, AuthMeterMeter, false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                dlms.initializeConnection();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "Getting meter clock...");
                obj = client.getObjects().findByLN(ObjectType.CLOCK, "0.0.1.0.0.255");
                final GXDLMSClock clock = (GXDLMSClock) obj;

                log(Level.INFO, "Adjusting meter clock...");
                final Calendar calendar = clock.getTime().getLocalCalendar();
                final SimpleDateFormat sdfN = new SimpleDateFormat("yyyyMMddHHmmss");
                calendar.setTime(sdfN.parse(tanggal));
                clock.setTime(calendar);
                dlms.write(clock, 0x2);
                log(Level.INFO, "Requery meter clock...");
                dlms.read(obj, 0x2);
                log(Level.INFO, "Adjusted meter clock: " + clock.getTime().toString());
                // rslt = sdf.format(clock.getTime().getLocalCalendar());
                rslt = clock.getTime().toString();
            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadLoadProfile(final String noMeter, final String tanggalAwal, final String tanggalAkhir,
            final String AuthMeter, final String AuthDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        List<JSONObject> Ljson = new ArrayList<JSONObject>();
        try {
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            setTargetMeter(noMeter, AuthDCU, true);
            client.disconnectRequest(true);

            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;
            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            log(Level.INFO, "=======  M E T E R    R E A D  =======");
            log(Level.INFO, "Read Load Profile from " + tanggalAwal + " to " + tanggalAkhir);
            client.disconnectRequest(true);

            // GXDLMSObject obj;
            setTargetMeter(noMeter, AuthMeter, true);
            client.disconnectRequest(true);
            try {
                dlms.initializeConnection();

                log(Level.INFO, "Getting load Profile...");
                final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
                pg.setLogicalName("1.0.99.1.0.255");
                pg.setObjectType(ObjectType.PROFILE_GENERIC);
                pg.setDescription("LoadProfile");

                GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
                pg2.setLogicalName("1.0.99.1.0.255");
                pg2.setObjectType(ObjectType.PROFILE_GENERIC);
                pg2.setDescription("LoadProfile");

                dlms.getProfileGenericColumnsById(pg);
                if (pg.getCaptureObjects().size() > 0) {
                    final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                    if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName("0.0.1.0.0.255");
                        dt.setObjectType(ObjectType.DATA);
                        dt.setUIDataType(2, DataType.DATETIME);
                        pg2.addCaptureObject(dt, 2, 0);

                        dt = new GXDLMSObject();
                        dt.setLogicalName("1.0.0.8.4.255");
                        dt.setObjectType(ObjectType.DATA);
                        pg2.addCaptureObject(dt, 2, 0);

                        for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                            pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                    it.getValue().getDataIndex());
                        }
                    } else {
                        pg2 = pg;
                    }

                    final Calendar startLP = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    final Calendar endLp = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    startLP.setTime(sdf.parse(tanggalAwal));
                    endLp.setTime(sdf.parse(tanggalAkhir));
                    Ljson = dlms.getProfileGenericsByDates(pg2, startLP, endLp);
                }
            } finally {
                client.disconnectRequest(true);
                // testReadExecuted = false;
                dlms.close();
                return result("Sukses", Ljson, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> disconnectReconnect(final String noMeter, final int status, final String AuthMeter,
            final String AuthDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(noMeter, AuthDCU, true);

            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;
            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));

            // log(Level.INFO, "======= M E T E R T E S T =======");
            client.disconnectRequest(true);

            setTargetMeter(noMeter, AuthMeter, false);
            client.disconnectRequest(true);
            try {
                dlms.initializeConnection();
                log(Level.INFO, "Getting Disconnect Reconnect...");
                final GXDLMSDisconnectControl dc = new GXDLMSDisconnectControl("0.0.96.3.10.255");
                final byte[][] b = client.method(dc, status + 1, 1 + 0xff, DataType.INT8);
                dlms.readDLMSPacket(b);
                log(Level.INFO, "Finish Disconnect Reconnect...");

            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                return result("Sukses", "", 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    private List<String> result(final String msg, final Object data, final int out) {
        // JSONObject json = new JSONObject();
        // json.put("msg", msg);
        // json.put("data", data);
        // json.put("out", out);
        final List<String> result = new ArrayList<String>();
        result.add(msg);
        result.add(data.toString());
        result.add(Integer.toString(out));
        return result;
    }

    private GetDataResponse result(final String msg, final int out, final String UID, final String jenis) {
        // JSONObject json = new JSONObject();
        // json.put("msg", msg);
        // json.put("data", data);
        // json.put("out", out);
        List<DBObject> l_rslt = new ArrayList<DBObject>();
        String rslt = "";
        if (msg.equals("Sukses")) {
            BasicDBObject query = new BasicDBObject();
            query = new BasicDBObject();
            query.put("UID", UID);
            final MongoDB mg = new MongoDB();
            try {
                l_rslt = mg.getData("DATA_" + jenis, query, "");
            } catch (final Exception e) {
                e.printStackTrace();
            }

        }

        final Gson gson = new Gson();
        rslt = gson.toJson(l_rslt);
        // System.out.println("=================== R E S U L T M O N G O - D B");
        // System.out.println(l_rslt);
        final GetDataResponse resp = GetDataResponse.newBuilder().setMessages(msg).setResult(rslt.toString())
                .setOut(Integer.toString(out)).build();
        return resp;

        // result.add(msg);
        // result.add(data.toString());
        // result.add(Integer.toString(out));
        // return result;
    }

    // -------------------------
    public GetDataResponse readOneData(final String noMeter, final String logicalName, final String classId,
            final String fieldName, final String UID, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            try {
                dlms.sendListMeter();
            } catch (Exception e) {
            }
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D    O N E   D A T A  =======");
                DLMSReader._DLMSreadOneData(dlms, logicalName, classId, fieldName, UID, p);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");

                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GetDataResponse readData(final String noMeter, final String jenis, final String UID, final Params p) {
        System.out.println("readData ..... ");
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        System.out.println("readData ..... ");
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociatioxnView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();

            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                DLMSReader._DLMSreadData(dlms, jenis, UID, p);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse readDataProfile(final String noMeter, final String jenis, final java.util.Calendar startLP,
            final java.util.Calendar endLp, final String UID, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                // final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                // sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   P R O F I L E  =======");
                log(Level.INFO, " From : " + startLP.getTime() + " To : " + endLp.getTime());
                DLMSReader._DLMSreadProfileGeneric(dlms, jenis, startLP, endLp, UID, p, false);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse writeData(final String noMeter, final String jenis, final String UID, final Params p,
            final String value) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   W R I T E   D A T A  =======");
                DLMSWriter._DLMSwriteMeter(dlms, jenis, UID, p, value);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   W R I T E   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse actionData(final String noMeter, final String jenis, final String UID, final Params p,
            final String value) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   A C T I O N  =======");
                DLMSAction._DLMSdisconnectMeter(dlms, Integer.parseInt(value));
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   A C T I O N   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    // ---------------
    // public void _DLMSreadData(final DLMS r, final String jenis, final String UID,
    // final Params p) throws Exception {
    // log(Level.INFO, "readData..." + jenis);
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put(jenis, new BasicDBObject("$ne", 0).append("$exists", true));
    // log(Level.INFO, "DATA_OBIS..." + jenis + ":" + jenisPhasa + " ==> ");
    // /* Step 5 : Get all documents */
    // final MongoDB db = new MongoDB();
    // final List<DBObject> cursors = db.getData("DATA_OBIS", query, jenis);
    // log(Level.INFO, "DATA_OBIS..." + jenis + " : " + cursors.size());
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // /* Step 6 : Print all documents */
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // System.out.println("Now " + now.getTime());
    // final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID",
    // UID)
    // .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER",
    // p.getTypeMeter())
    // .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
    // .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT",
    // p.getKdPembangkit())
    // .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());
    // for (final DBObject csr : cursors) {
    // final String obis = csr.get("LOGICAL_NAME").toString();
    // final String type = csr.get("CLASS_ID").toString();
    // final String column = csr.get("NAMA_FIELD").toString();
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // final String dt = null;
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.remove(column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append(column + r.ifNull(ldt.get(j).getAlias()),
    // r.convertData(ldt.get(j)));
    // }
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_" + jenis, doc);
    // }
    // void _DLMSreadOneData(final DLMS r, final String logicalName, final String
    // classId, final String namaField,
    // final String UID, final Params p) {
    // try {
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID",
    // UID)
    // .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER",
    // p.getTypeMeter())
    // .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
    // .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT",
    // p.getKdPembangkit())
    // .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());
    // final String obis = logicalName;
    // final String type = classId;
    // final String column = namaField;
    // if(classId.equals("70")){
    // _readDisconnectControl(r,logicalName, classId, namaField, doc, UID, p);
    // }else if (!classId.equals("8")) {
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append("DATA", r.convertData(ldt.get(j)));
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_GLOBAL", doc);
    // }
    // else if (!classId.equals("7")) {
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append("DATA", r.convertData(ldt.get(j)));
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_GLOBAL", doc);
    // } else {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // pg.setLogicalName(obis);
    // pg.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg.setDescription(obis);
    // GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
    // pg2.setLogicalName(obis);
    // pg2.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg2.setDescription(obis);
    // r.getProfileGenericColumnsById(pg);
    // if (pg.getCaptureObjects().size() > 0) {
    // final Entry<GXDLMSObject, GXDLMSCaptureObject> itz =
    // pg.getCaptureObjects().get(0);
    // if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
    // GXDLMSObject dt = new GXDLMSObject();
    // dt.setLogicalName("0.0.1.0.0.255");
    // dt.setObjectType(ObjectType.DATA);
    // dt.setUIDataType(2, DataType.DATETIME);
    // pg2.addCaptureObject(dt, 2, 0);
    // dt = new GXDLMSObject();
    // dt.setLogicalName("1.0.0.8.4.255");
    // dt.setObjectType(ObjectType.DATA);
    // pg2.addCaptureObject(dt, 2, 0);
    // for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it :
    // pg.getCaptureObjects()) {
    // pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
    // it.getValue().getDataIndex());
    // }
    // } else {
    // pg2 = pg;
    // }
    // final java.util.Calendar startLP = java.util.Calendar
    // .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // startLP.add(java.util.Calendar.DATE, -1);
    // final java.util.Calendar endLp = java.util.Calendar
    // .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // final List<Document> docs = r.getProfileGenericsMongo(pg2, column, startLP,
    // endLp, UID, p);
    // MongoDB.setMultiData("DATA_" + "GLOBAL", docs);
    // }
    // }
    // } catch (final Exception e) {
    // }
    // }
    // void _DLMSwriteMeter(final DLMS r, final String namaField, final String UID,
    // final Params p, final String data)
    // throws Exception {
    // try {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put("NAMA_FIELD", namaField);
    // final MongoDB mg = new MongoDB();
    // final List<DBObject> cursors = mg.getData("DATA_OBIS", query, null);
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // String obis = "";
    // String type = "";
    // String column = "";
    // String deskripsi = "";
    // for (final DBObject csr : cursors) {
    // obis = csr.get("LOGICAL_NAME").toString();
    // type = csr.get("CLASS_ID").toString();
    // column = csr.get("NAMA_FIELD").toString();
    // deskripsi = csr.get("DESKRIPSI").toString();
    // pg.setLogicalName(obis);
    // pg.setObjectType(r.getObjectType(type));
    // pg.setDescription(deskripsi);
    // }
    // Object data_ob = null;
    // DataType dataType = null;
    // if (type.equals("8")) {
    // final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    // final java.util.Calendar tgl = java.util.Calendar.getInstance();
    // tgl.setTime(sdf.parse(data));
    // // System.out.println("Tanggal "+data+ " :: "+tgl.getTime());
    // data_ob = tgl.getTime();
    // dataType = DataType.OCTET_STRING;
    // } else if (type.equals("1")) {
    // data_ob = data;
    // dataType = DataType.STRING;
    // // dataType = DataType.OCTET_STRING;
    // } else if (type.equals("3")) {
    // data_ob = data;
    // dataType = DataType.UINT32;
    // }
    // if (data_ob != null) {
    // r.readDLMSPacket(r.getClient().write(obis, data_ob, dataType,
    // r.getObjectType(type), 2));
    // }
    // } finally {
    // r.close();
    // }
    // }
    // public void _DLMSreadProfileGeneric(final DLMS r, final String jenis, final
    // java.util.Calendar startLP,
    // final java.util.Calendar endLp, final String UID, final Params p) throws
    // Exception {
    // try {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // pg.setLogicalName("1.0.99.1.0.255");
    // pg.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg.setDescription("LoadProfile");
    // GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
    // pg2.setLogicalName("1.0.99.1.0.255");
    // pg2.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg2.setDescription("LoadProfile");
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put("CLASS_ID", 7);
    // String _jenis = "PROFILE_GENERIC";
    // if (jenis.toUpperCase().equals("PROFILE")) {
    // _jenis = "PROFILE_GENERIC";
    // } else if (jenis.toUpperCase().equals("BILLING")) {
    // _jenis = "BILLING_GENERIC";
    // } else {
    // _jenis = jenis;
    // }
    // query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));
    // final MongoDB mg = new MongoDB();
    // final List<DBObject> cursors = mg.getData("DATA_OBIS", query, _jenis);
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // for (final DBObject csr : cursors) {
    // final String obis = csr.get("LOGICAL_NAME").toString();
    // final String type = csr.get("CLASS_ID").toString();
    // final String column = csr.get("NAMA_FIELD").toString();
    // final String deskripsi = csr.get("DESKRIPSI").toString();
    // pg.setLogicalName(obis);
    // pg.setObjectType(r.getObjectType(type));
    // pg.setDescription(deskripsi);
    // pg2.setLogicalName(obis);
    // pg2.setObjectType(r.getObjectType(type));
    // pg2.setDescription(deskripsi);
    // }
    // r.getProfileGenericColumnsById(pg);
    // if (pg.getCaptureObjects().size() > 0) {
    // final Entry<GXDLMSObject, GXDLMSCaptureObject> itz =
    // pg.getCaptureObjects().get(0);
    // if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
    // GXDLMSObject dt = new GXDLMSObject();
    // dt.setLogicalName("0.0.1.0.0.255");
    // dt.setObjectType(ObjectType.DATA);
    // dt.setUIDataType(2, DataType.DATETIME);
    // pg2.addCaptureObject(dt, 2, 0);
    // dt = new GXDLMSObject();
    // dt.setLogicalName("1.0.0.8.4.255");
    // dt.setObjectType(ObjectType.DATA);
    // pg2.addCaptureObject(dt, 2, 0);
    // for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it :
    // pg.getCaptureObjects()) {
    // pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
    // it.getValue().getDataIndex());
    // }
    // } else {
    // pg2 = pg;
    // }
    // final List<Document> docs = r.getProfileGenericsMongo(pg2, jenis, startLP,
    // endLp, UID, p);
    // MongoDB.setMultiData("DATA_" + jenis, docs);
    // }
    // } finally {
    // r.close();
    // }
    // }
    // void _DLMSdisconnectMeter(final DLMS r, final int value) throws Exception {
    // System.out.println("Start Disconnect");
    // final GXDLMSDisconnectControl dc = new
    // GXDLMSDisconnectControl("0.0.96.3.10.255");
    // // dc.setOutputState(true);
    // final int ic = 1 + 0xff;
    // r.readDLMSPacket(r.getClient().method(dc, value + 1, ic, DataType.INT8));
    // System.out.println("Finish Disconnect");
    // }
    // public void _readDisconnectControl(final DLMS r, final String logicalName,
    // final String classId, final String namaField,Document doc, final String UID,
    // final Params p) {
    // GXDLMSDisconnectControl control = new GXDLMSDisconnectControl(logicalName);
    // control.setDescription("Disconnect Control");
    // GXDLMSObject object = control;
    // for (int pos : control.getAttributeIndexToRead(true)) {
    // try {
    // Object val = r.read(object, pos);
    // switch (pos) {
    // case 1:
    // control.setLogicalName(r.showValue(pos, val));
    // break;
    // case 2:
    // if (r.showValue(pos, val).equals("true")) {
    // control.setOutputState(true);
    // } else {
    // control.setOutputState(false);
    // }
    // break;
    // case 3:
    // if (r.showValue(pos, val).equals("Connected")) {
    // control.setControlState(ControlState.CONNECTED);
    // } else {
    // if (r.showValue(pos, val).equals("Disconnected")) {
    // control.setControlState(ControlState.DISCONNECTED);
    // } else {
    // control.setControlState(ControlState.READY_FOR_RECONNECTION);
    // }
    // }
    // break;
    // case 4:
    // switch (r.showValue(pos, val)) {
    // case "None":
    // control.setControlMode(ControlMode.NONE);
    // break;
    // case "Mode1":
    // control.setControlMode(ControlMode.MODE_1);
    // break;
    // case "Mode2":
    // control.setControlMode(ControlMode.MODE_2);
    // break;
    // case "Mode3":
    // control.setControlMode(ControlMode.MODE_3);
    // break;
    // case "Mode4":
    // control.setControlMode(ControlMode.MODE_4);
    // break;
    // case "Mode5":
    // control.setControlMode(ControlMode.MODE_5);
    // break;
    // case "Mode6":
    // control.setControlMode(ControlMode.MODE_6);
    // break;
    // default:
    // control.setControlMode(ControlMode.NONE);
    // break;
    // }
    // break;
    // }
    // } catch (Exception ex) {
    // System.out.println("error : " + ex.toString());
    // }
    // }
    // // System.out.println("Logical Name = " + control.getLogicalName());
    // // System.out.println("Output State = " + control.getOutputState());
    // // System.out.println("Control State = " + control.getControlState());
    // // System.out.println("Control Mode = " + control.getControlMode());
    // doc.append("LOGICAL_NAME", control.getLogicalName().toString());
    // doc.append("OUTPUT_STATE", control.getOutputState());
    // doc.append("DATA", control.getControlState().toString());
    // doc.append("CONTROL_MODE", control.getControlMode().toString());
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // try {
    // MongoDB.setData("DATA_" + "GLOBAL", doc);
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }
    public GetDataResponse getMultiLogicalName(final ListLogicalName l_logicalName, final String UID,
            final String noMeter, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                for (final DataLogicalName ln : l_logicalName.getListObis()) {
                    DLMSReader._DLMSreadOneData(dlms, ln.getLogicalName(), ln.getClassId(), ln.getLn(), UID, p);
                }
            } catch (final Exception e) {
                e.printStackTrace();
                // rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GetDataResponse setMultiLogicalName(final ListLogicalName l_logicalName, final String UID,
            final String noMeter, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                for (final DataLogicalName ln : l_logicalName.getListObis()) {
                    DLMSWriter._DLMSwriteMeter(dlms, ln.getLn(), UID, p, ln.getVal());
                }
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GetDataResponse hesImageTransfer(String userId, ByteString images, String noMeter, String data, String authMeter, final String UID,
            final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        JSONObject json = new JSONObject();
        try {

            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);

            BasicDBObject query = new BasicDBObject();
//            String msg =
            DLMSAction.imageTransfer(dlms, data, images);;
//				System.out.println(msg);
//				if (msg.contains("SUKSES") || msg.equals("")) {
            query = new BasicDBObject();
            query.put("UID", UID);
            List<DBObject> l_rslt2 = MongoDB.getLamaWaktu("DATA_HISTORY_PENARIKAN", query, "");
//					System.out.println(l_rslt2.size());
            Gson gson = new Gson();
            String rslt2 = gson.toJson(l_rslt2);

            log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
            dlms.close();
            testReadExecuted = false;
            log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");

            return result("Sukses", null, 1, rslt2);
//				} else {
//					return result(msg, null, 0, null);
//				}
//			return result("Sukses", null, 1, null);
        } catch (Exception e) {
            testReadExecuted = false;
            return result(e.getMessage(), null, 0, null);
        }
    }

    public GetDataResponse hesWriteLimmiter(String userId, String logicalName, String originActivationTime, int duration,
            long minOverThresholdDuration, long minUnderThresholdDuration, long emergencythreshold,
            long nemergencythreshold, long aemergencythreshold, String noMeter, String authMeter, final String UID,
            final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;

        JSONObject json = new JSONObject();
        try {

            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            log(Level.INFO, "=======  M E T E R    C O N N E C T I O N  =======");
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            // meter status, mesermeunt status,

            BasicDBObject query = new BasicDBObject();

            DLMSWriter.writeMeterLimmiter(dlms, logicalName, UID, p, originActivationTime, duration, minOverThresholdDuration, minUnderThresholdDuration, emergencythreshold, nemergencythreshold, aemergencythreshold);
//				System.out.println("------> "+msg);
//				if (msg.toUpperCase().contains("SUKSES") || msg.equals("")) {
            query = new BasicDBObject();
            query.put("UID", UID);
            List<DBObject> l_rslt2 = MongoDB.getLamaWaktu("DATA_HISTORY_PENARIKAN", query, "");
            List<DBObject> l_rslt = MongoDB.getData("DATA_GLOBAL", query, "");
            Gson gson = new Gson();
            String rslt2 = gson.toJson(l_rslt2);
//					System.out.println("=====> "+rslt2);
            log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
            dlms.close();
            testReadExecuted = false;
            log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
            return result("Sukses", l_rslt, 1, rslt2);
//				} else {
//					return result(msg, null, 0, null);
//				}
        } catch (Exception e) {
            testReadExecuted = false;
            return result(e.getMessage(), null, 0, null);
        }
    }

    public GetDataResponse hesAssociation(String userid, String noMeter, String AuthMeter, final String UID,
            final Params p) {

        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;

        JSONObject json = new JSONObject();
        try {

            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            log(Level.INFO, "=======  M E T E R    C O N N E C T I O N  =======");
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            // meter status, mesermeunt status,

            BasicDBObject query = new BasicDBObject();

            DLMSReader.readAssociationView(dlms, "ASSOCIATION_VIEW", UID, p);
//				System.out.println("------> "+msg);
//				if (msg.toUpperCase().contains("SUKSES") || msg.equals("")) {
            query = new BasicDBObject();
            query.put("UID", UID);
            List<DBObject> l_rslt2 = MongoDB.getLamaWaktu("DATA_HISTORY_PENARIKAN", query, "");
            List<DBObject> l_rslt = MongoDB.getData("DATA_GLOBAL", query, "");
            Gson gson = new Gson();
            String rslt2 = gson.toJson(l_rslt2);
//					System.out.println("=====> "+rslt2);
            log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
            dlms.close();
            testReadExecuted = false;
            log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
            return result("Sukses", l_rslt, 1, rslt2);
//				} else {
//					return result(msg, null, 0, null);
//				}
        } catch (Exception e) {
            testReadExecuted = false;
            return result(e.getMessage(), null, 0, null);
        }
    }

    public GetDataResponse hesWriteActiveCallendar(String userId, String logicalName, String tanggal, String jenisActiveCallendar, String noMeter, String authMeter, final String UID,
            final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;

        JSONObject json = new JSONObject();
        try {
            // setTargetMeter(null);
            setTargetMeter(noMeter, "", true);
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.sendListMeter();
            client.disconnectRequest(true);
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, "", false);
            client.disconnectRequest(true);
            // meter status, mesermeunt status,

            // meter status, mesermeunt status,
            BasicDBObject query = new BasicDBObject();
            Gson gson = new Gson();
            DayProfileList dayProfile = gson.fromJson(jenisActiveCallendar, DayProfileList.class);
            DLMSWriter.writeMeterActiveCallendar(dlms, logicalName, UID, p, tanggal, dayProfile);
            // .writeActiveCallendar(logicalName, UID, tanggal, jenisActiveCallendar, noMeter, MapAuthMeter);
//				if (msg.contains("SUKSES") || msg.equals("")) {
            query = new BasicDBObject();
            query.put("UID", UID);
            List<DBObject> l_rslt2 = MongoDB.getLamaWaktu("DATA_HISTORY_PENARIKAN", query, "");
            gson = new Gson();
            String rslt2 = gson.toJson(l_rslt2);
            log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
            dlms.close();
            testReadExecuted = false;
            log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
            return result("Sukses", null, 1, rslt2);
//				} else {
//					return result(msg, null, 0, null);
//				}
        } catch (Exception e) {
            testReadExecuted = false;
            return result(e.getMessage(), null, 0, null);
        }
    }

    private boolean cekMeterOnline(String noMeter) throws Exception {
        BasicDBObject query = new BasicDBObject();
        query.put("noNeter", noMeter);
        query.put("actionStatus", 1);
        List<DBObject> ldb = MongoDB.getData("DATA_METER", query, "");
        if (ldb.size() > 0) {
            for (DBObject csr : ldb) {
                Date now = new Date();
                Date runOn = (Date) csr.get("LAST_ACTION");
                long diffInMillies = Math.abs(now.getTime() - runOn.getTime());
                long diff = diffInMillies / (1000 * 60);
                if (diff < 5) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private GetDataResponse result(String msg, Object data, int out, Object waktuPenarikan) {
        GetDataResponse reply = GetDataResponse.newBuilder()
                .setMessages(msg)
                .setResult(data == null ? "" : data.toString())
                .setOut(Integer.toString(out))
                .setWaktuPenarikan(data == null ? "" : waktuPenarikan.toString())
                .build();
        return reply;
    }

    //----------------------
    public GXDLMSObject objFromCode(GXObisCode code) {
        if (null != code.getObjectType()) {
            switch (code.getObjectType()) {
                case CLOCK: {
                    GXDLMSClock obj = new GXDLMSClock();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case DATA: {
                    GXDLMSData obj = new GXDLMSData();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case REGISTER: {
                    GXDLMSRegister obj = new GXDLMSRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;

                }
                case EXTENDED_REGISTER: {
                    GXDLMSExtendedRegister obj = new GXDLMSExtendedRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case DEMAND_REGISTER: {
                    GXDLMSDemandRegister obj = new GXDLMSDemandRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case PROFILE_GENERIC: {
                    GXDLMSProfileGeneric obj = new GXDLMSProfileGeneric();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                default: {
                    GXDLMSObject obj = new GXDLMSObject();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
            }
        } else {
            return null;
        }
    }

    static Params setParamDCU(final GXDLMSSecureClient client, final String nometer,
            final HashMap<String, String> authMeter, boolean isDCU) {

        final Params p = new Params();
        try {
            System.out.println("setParam ===>" + nometer);
            String idAntrian = "";
            String collection = "";
            final BasicDBObject query = new BasicDBObject();
            final DBObject Projection = new BasicDBObject("_id", 0).append("LAST_ACTION", 0).append("ACTION", 0)
                    .append("ACTION_ON_SERVER", 0).append("ACTION_STATUS", 0).append("ID_ANTRIAN", 0);
            // System.out.println("authMeter");
            if (authMeter.get("idAntrian") != null && !authMeter.get("idAntrian").equals("")) {
                idAntrian = authMeter.get("idAntrian");
                query.put("ID_ANTRIAN", idAntrian);
                collection = "DATA_METER_ANTRIAN";
            } else {
                query.put("NO_DEVICE", nometer);
                query.put("NO_METER", null);
                collection = "LAP_DATA_METER_PLG";
            }
            System.out.println(collection + " : " + query.toString());
            // List<DBObject> cursor = MongoDB.getData("DATA_METER", query,
            // "",Projection);
            final List<DBObject> cursor = MongoDB.getData(collection, query, "", Projection);
            String noMeter = nometer;
            String typeMeter = "";
            String merkMeter = "";
            String authenticationLevel = "";
            String clientAddress = "";
            String logicalAddress = "";
            String physicalAddress = "";
            String ipAddress = "";
            String port = "";
            String passMeter = "";
            String security = "";
            String authenticationKey = "";
            String blockCipherKey = "";
            String systemTitle = "";
            Integer phasa = 0;
            String kdPusat = "";
            String kdPembangkit = "";
            String kdArea = "";
            String kdUnitBisnis = "";
            String jenis = "";
            String targetAddress = "";
            String interfaceType = "";
            String typeDevice = "";
            String noDevice = "";
            String typeObis = "";
            String noSeluler = "";
            String noSimcard = "";
            String addressSize = "2";

            DBObject csr = null;
            if (cursor.size() > 0) {
                csr = cursor.get(0);
                noMeter = "";//csr.get("NO_DEVICE").toString();
                typeMeter = "";//csr.get("TIPE_DEVICE").toString();
                merkMeter = "";//csr.get("MERK_DEVICE").toString();
                // if(!isDCU){
                authenticationLevel = csr.get("AUTHENTICATION_LEVEL").toString();
                clientAddress = csr.get("CLIENT_ADDRESS").toString();
                logicalAddress = csr.get("LOGICAL_ADDRESS").toString();
                physicalAddress = csr.get("PHYSICAL_ADDRESS").toString();
                targetAddress = csr.get("TARGET_ADDRESS") == null ? "0" : csr.get("TARGET_ADDRESS").toString();
                ipAddress = csr.get("IP_ADDRESS").toString();
                port = csr.get("PORT").toString();
                passMeter = csr.get("PASSWORD_DEVICE").toString();
                security = csr.get("SECURITY").toString();
                authenticationKey = csr.get("AUTHENTICATION_KEY").toString();
                blockCipherKey = csr.get("BLOCK_CIPHER_KEY").toString();
                systemTitle = csr.get("SYSTEM_TITLE").toString();
                addressSize = csr.get("ADDRESS_SIZE") == null ? "" : csr.get("ADDRESS_SIZE").toString();
                interfaceType = csr.get("INTERFACE_TYPE") == null ? "HDLC" : csr.get("INTERFACE_TYPE").toString();
                typeObis = csr.get("TYPE_OBIS") == null ? "SPLN" : csr.get("TYPE_OBIS").toString();
                System.out.println("addressSize => " + addressSize);
//                 }else{

//                     authenticationLevel = csr1.get("AUTHENTICATION_LEVEL").toString();
//                     clientAddress = csr1.get("CLIENT_ADDRESS").toString();
//                     logicalAddress = csr1.get("LOGICAL_ADDRESS").toString();
//                     physicalAddress = csr1.get("PHYSICAL_ADDRESS").toString();
//                     targetAddress = csr.get("TARGET_ADDRESS") == null ? "0" : csr1.get("TARGET_ADDRESS").toString();
//                     ipAddress = csr1.get("IP_ADDRESS").toString();
//                     port = csr1.get("PORT").toString();
//                     passMeter = csr1.get("PASSWORD_DEVICE").toString();
//                     security = csr1.get("SECURITY").toString();
//                     authenticationKey = csr1.get("AUTHENTICATION_KEY").toString();
//                     blockCipherKey = csr1.get("BLOCK_CIPHER_KEY").toString();
//                     systemTitle = csr1.get("SYSTEM_TITLE").toString();
//                     addressSize = csr1.get("ADDRESS_SIZE") == null ? "" : csr1.get("ADDRESS_SIZE").toString();
//                     interfaceType = csr1.get("INTERFACE_TYPE") == null ? "HDLC" : csr1.get("INTERFACE_TYPE").toString();
//                     typeObis = csr1.get("TYPE_OBIS") == null ? "SPLN" : csr1.get("TYPE_OBIS").toString();
//                     System.out.println("addressSize => "+addressSize);
// //                    byte[] ctosChallenge = GXSecure.generateChallenge(Authentication.HIGH);
//                 }
                // phasa = Integer.parseInt(csr.get("PHASA").toString());
                // kdPusat = csr.get("KD_PUSAT").toString();
                // kdPembangkit = csr.get("KD_PEMBANGKIT").toString();
                // kdArea = csr.get("KD_AREA").toString();
                // kdUnitBisnis = csr.get("KD_UNIT_BISNIS").toString();
                // jenis = csr.get("JENIS").toString();
                typeDevice = csr.get("TYPE_DEVICE") == null ? "" : csr.get("TYPE_DEVICE").toString();
                noDevice = csr.get("NO_DEVICE") == null ? "" : csr.get("NO_DEVICE").toString();
                // noSeluler = csr.get("NO_SELULER") == null ? "" : csr.get("NO_SELULER").toString();
                // noSimcard = csr.get("NO_SIMCARD") == null ? "" : csr.get("NO_SIMCARD").toString();
            }
            System.out.println("addressSize : " + addressSize);
            if (authMeter.size() > 0) {
                if (authMeter.get("authenticationLevel") != null && !authMeter.get("authenticationLevel").equals("")) {
                    authenticationLevel = authMeter.get("authenticationLevel");
                }

                if (authMeter.get("clientAddress") != null && !authMeter.get("clientAddress").equals("")) {
                    clientAddress = authMeter.get("clientAddress");
                }

                if (authMeter.get("logicalAddress") != null && !authMeter.get("logicalAddress").equals("")) {
                    logicalAddress = authMeter.get("logicalAddress");
                }

                if (authMeter.get("physicalAddress") != null && !authMeter.get("physicalAddress").equals("")) {
                    physicalAddress = authMeter.get("physicalAddress");
                }

                if (authMeter.get("ipAddress") != null && !authMeter.get("ipAddress").equals("")) {
                    ipAddress = authMeter.get("ipAddress");
                }

                if (authMeter.get("port") != null && !authMeter.get("port").equals("")) {
                    port = authMeter.get("port");
                }

                if (authMeter.get("passMeter") != null && !authMeter.get("passMeter").equals("")) {
                    passMeter = authMeter.get("passMeter");
                }

                if (authMeter.get("security") != null && !authMeter.get("security").equals("")) {
                    security = authMeter.get("security");
                }

                if (authMeter.get("authenticationKey") != null && !authMeter.get("authenticationKey").equals("")) {
                    authenticationKey = authMeter.get("authenticationKey");
                }

                if (authMeter.get("blockCipherKey") != null && !authMeter.get("blockCipherKey").equals("")) {
                    blockCipherKey = authMeter.get("blockCipherKey");
                }

                if (authMeter.get("systemTitle") != null && !authMeter.get("systemTitle").equals("")) {
                    systemTitle = authMeter.get("systemTitle");
                }

                if (authMeter.get("phasa") != null && !authMeter.get("phasa").equals("")) {
                    phasa = Integer.parseInt(authMeter.get("phasa"));
                }

                if (authMeter.get("merkMeter") != null && !authMeter.get("merkMeter").equals("")) {
                    merkMeter = authMeter.get("merkMeter");
                }

                if (authMeter.get("typeMeter") != null && !authMeter.get("typeMeter").equals("")) {
                    typeMeter = authMeter.get("typeMeter");
                }

                if (authMeter.get("targetAddress") != null && !authMeter.get("targetAddress").equals("")) {
                    targetAddress = authMeter.get("targetAddress");
                }

                if (authMeter.get("interfaceType") != null && !authMeter.get("interfaceType").equals("")) {
                    interfaceType = authMeter.get("interfaceType");
                }

                if (authMeter.get("noDevice") != null && !authMeter.get("noDevice").equals("")) {
                    noDevice = authMeter.get("noDevice");
                }

                if (authMeter.get("typeDevice") != null && !authMeter.get("typeDevice").equals("")) {
                    noDevice = authMeter.get("typeDevice");
                }

                if (authMeter.get("typeObis") != null && !authMeter.get("typeObis").equals("")) {
                    typeObis = authMeter.get("typeObis");
                }

                if (authMeter.get("noSeluler") != null && !authMeter.get("noSeluler").equals("")) {
                    noSeluler = authMeter.get("noSeluler");
                }

                if (authMeter.get("noSimcard") != null && !authMeter.get("noSimcard").equals("")) {
                    noSeluler = authMeter.get("noSimcard");
                }

                if (authMeter.get("kdPusat") != null && !authMeter.get("kdPusat").equals("")) {
                    kdPusat = authMeter.get("kdPusat");
                }

                if (authMeter.get("kdUnitBisnis") != null && !authMeter.get("kdUnitBisnis").equals("")) {
                    kdUnitBisnis = authMeter.get("kdUnitBisnis");
                }

                if (authMeter.get("kdPembangkit") != null && !authMeter.get("kdPembangkit").equals("")) {
                    kdPembangkit = authMeter.get("kdPembangkit");
                }

                if (authMeter.get("kdArea") != null && !authMeter.get("kdArea").equals("")) {
                    kdArea = authMeter.get("kdArea");
                }

                if (authMeter.get("jenis") != null && !authMeter.get("jenis").equals("")) {
                    jenis = authMeter.get("jenis");
                }

                if (authMeter.get("addressSize") != null && !authMeter.get("addressSize").equals("")) {
                    addressSize = authMeter.get("addressSize");
                }
            }
            System.out.println("addressSize : " + addressSize);
            p.setDbObject(csr);
            p.setAuthenticationLevel(authenticationLevel.toString());
            p.setUseLogicalNameReferencing(true);

            p.setClientAddress(clientAddress.equals("null") ? 0 : Integer.parseInt(clientAddress));

            p.setLogicalAddress(logicalAddress.equals("null") ? 0 : Integer.parseInt(logicalAddress));

            p.setPhysicalAddress(physicalAddress.equals("null") ? 0 : Integer.parseInt(physicalAddress));

            p.setTargetAddress(targetAddress.equals("") ? 0 : Integer.parseInt(targetAddress));
            p.setPassword(passMeter);
            p.setTraceLevel("OFF");
            p.setHostName(ipAddress);
            if (port != null && !port.equals("")) {
                p.setPort(Integer.parseInt(port));
            }
            p.setSecurity(security);
            p.setAuthenticationKey(authenticationKey);
            p.setBlockCipherKey(blockCipherKey);
            p.setSystemTitle(systemTitle);
            p.setPhasa(phasa);
            p.setNoMeter(noMeter);
            p.setTypeMeter(typeMeter);
            p.setMerkMeter(merkMeter);
            p.setKdArea(kdArea);
            p.setKdPembangkit(kdPembangkit);
            p.setKdPusat(kdPusat);
            p.setKdUnitBisnis(kdUnitBisnis);
            p.setJenis(jenis);
            p.setInterfaceType(interfaceType);
            p.setNoDevice(noDevice);
            p.setTypeDevice(typeDevice);
            p.setTypeObis(typeObis);
            p.setNoSeluler(noSeluler);
            p.setNoSimcard(noSimcard);
            p.setAddressSize(Integer.parseInt(addressSize));
            final int ret = getParameters(client, p);
            if (ret != 0) {
                return null;
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
        return p;
    }

    static Params setParam(final GXDLMSSecureClient client, final String nometer,
            final HashMap<String, String> authMeter, boolean isDCU) {

        final Params p = new Params();
        try {
            System.out.println("setParam ===>" + nometer);
            String idAntrian = "";
            String collection = "";
            final BasicDBObject query = new BasicDBObject();
            final DBObject Projection = new BasicDBObject("_id", 0).append("LAST_ACTION", 0).append("ACTION", 0)
                    .append("ACTION_ON_SERVER", 0).append("ACTION_STATUS", 0).append("ID_ANTRIAN", 0);
            // System.out.println("authMeter");
            if (authMeter.get("idAntrian") != null && !authMeter.get("idAntrian").equals("")) {
                idAntrian = authMeter.get("idAntrian");
                query.put("ID_ANTRIAN", idAntrian);
                collection = "DATA_METER_ANTRIAN";
            } else {
                // if(isDCU)
                //     query.put("NO_METER", authMeter.get("noMeter"));
                // else
                query.put("NO_METER", nometer);

                collection = "LAP_DATA_METER_PLG";
            }
            System.out.println(collection + " : " + query.toString());
            // List<DBObject> cursor = MongoDB.getData("DATA_METER", query,
            // "",Projection);
            final List<DBObject> cursor = MongoDB.getData(collection, query, "", Projection);
            String noMeter = nometer;
            String typeMeter = "";
            String merkMeter = "";
            String authenticationLevel = "";
            String clientAddress = "";
            String logicalAddress = "";
            String physicalAddress = "";
            String ipAddress = "";
            String port = "";
            String passMeter = "";
            String security = "";
            String authenticationKey = "";
            String blockCipherKey = "";
            String systemTitle = "";
            Integer phasa = 0;
            String kdPusat = "";
            String kdPembangkit = "";
            String kdArea = "";
            String kdUnitBisnis = "";
            String jenis = "";
            String targetAddress = "";
            String interfaceType = "";
            String typeDevice = "";
            String noDevice = "";
            String typeObis = "";
            String noSeluler = "";
            String noSimcard = "";
            String addressSize = "2";

            DBObject csr = null;
            if (cursor.size() > 0) {
                csr = cursor.get(0);
                Object dt = csr.get("SECURITY_DEVICE");
                DBObject csr1 = (DBObject) dt;
                // System.out.println(csr1);
                // System.out.println(csr);
                noMeter = csr.get("NO_METER") == null ? "" : csr.get("NO_METER").toString();
                typeMeter = csr.get("TIPE_METER").toString();
                merkMeter = csr.get("MERK_METER").toString();
                if (!isDCU) {
                    authenticationLevel = csr.get("AUTHENTICATION_LEVEL").toString();
                    clientAddress = csr.get("CLIENT_ADDRESS").toString();
                    logicalAddress = csr.get("LOGICAL_ADDRESS").toString();
                    physicalAddress = csr.get("PHYSICAL_ADDRESS").toString();
                    targetAddress = csr.get("TARGET_ADDRESS") == null ? "0" : csr.get("TARGET_ADDRESS").toString();
                    ipAddress = csr.get("IP_ADDRESS").toString();
                    port = csr.get("PORT").toString();
                    passMeter = csr.get("PASSWORD_METER").toString();
                    security = csr.get("SECURITY").toString();
                    authenticationKey = csr.get("AUTHENTICATION_KEY").toString();
                    blockCipherKey = csr.get("BLOCK_CIPHER_KEY").toString();
                    systemTitle = csr.get("SYSTEM_TITLE").toString();
                    addressSize = csr.get("ADDRESS_SIZE") == null ? "" : csr.get("ADDRESS_SIZE").toString();
                    interfaceType = csr.get("INTERFACE_TYPE") == null ? "HDLC" : csr.get("INTERFACE_TYPE").toString();
                    typeObis = csr.get("TYPE_OBIS") == null ? "SPLN" : csr.get("TYPE_OBIS").toString();
                    System.out.println("addressSize => " + addressSize);
                } else {

                    authenticationLevel = csr1.get("AUTHENTICATION_LEVEL").toString();
                    clientAddress = csr1.get("CLIENT_ADDRESS").toString();
                    logicalAddress = csr1.get("LOGICAL_ADDRESS").toString();
                    physicalAddress = csr1.get("PHYSICAL_ADDRESS").toString();
                    targetAddress = csr.get("TARGET_ADDRESS") == null ? "0" : csr1.get("TARGET_ADDRESS").toString();
                    ipAddress = csr1.get("IP_ADDRESS").toString();
                    port = csr1.get("PORT").toString();
                    passMeter = csr1.get("PASSWORD_DEVICE").toString();
                    security = csr1.get("SECURITY").toString();
                    authenticationKey = csr1.get("AUTHENTICATION_KEY").toString();
                    blockCipherKey = csr1.get("BLOCK_CIPHER_KEY").toString();
                    systemTitle = csr1.get("SYSTEM_TITLE").toString();
                    addressSize = csr1.get("ADDRESS_SIZE") == null ? "" : csr1.get("ADDRESS_SIZE").toString();
                    interfaceType = csr1.get("INTERFACE_TYPE") == null ? "HDLC" : csr1.get("INTERFACE_TYPE").toString();
                    typeObis = csr1.get("TYPE_OBIS") == null ? "SPLN" : csr1.get("TYPE_OBIS").toString();
                    System.out.println("addressSize => " + addressSize);

//                    byte[] ctosChallenge = GXSecure.generateChallenge(Authentication.HIGH);
                }

                phasa = Integer.parseInt(csr.get("PHASA").toString());
                kdPusat = csr.get("KD_PUSAT").toString();
                kdPembangkit = csr.get("KD_PEMBANGKIT").toString();
                kdArea = csr.get("KD_AREA").toString();
                kdUnitBisnis = csr.get("KD_UNIT_BISNIS").toString();
                jenis = csr.get("JENIS").toString();
                typeDevice = csr.get("TYPE_DEVICE") == null ? "" : csr.get("TYPE_DEVICE").toString();
                noDevice = csr.get("NO_DEVICE") == null ? "" : csr.get("NO_DEVICE").toString();
                noSeluler = csr.get("NO_SELULER") == null ? "" : csr.get("NO_SELULER").toString();
                noSimcard = csr.get("NO_SIMCARD") == null ? "" : csr.get("NO_SIMCARD").toString();
            }
            System.out.println("addressSize : " + addressSize);
            if (authMeter.size() > 0) {
                if (authMeter.get("authenticationLevel") != null && !authMeter.get("authenticationLevel").equals("")) {
                    authenticationLevel = authMeter.get("authenticationLevel");
                }

                if (authMeter.get("clientAddress") != null && !authMeter.get("clientAddress").equals("")) {
                    clientAddress = authMeter.get("clientAddress");
                }

                if (authMeter.get("logicalAddress") != null && !authMeter.get("logicalAddress").equals("")) {
                    logicalAddress = authMeter.get("logicalAddress");
                }

                if (authMeter.get("physicalAddress") != null && !authMeter.get("physicalAddress").equals("")) {
                    physicalAddress = authMeter.get("physicalAddress");
                }

                if (authMeter.get("ipAddress") != null && !authMeter.get("ipAddress").equals("")) {
                    ipAddress = authMeter.get("ipAddress");
                }

                if (authMeter.get("port") != null && !authMeter.get("port").equals("")) {
                    port = authMeter.get("port");
                }

                if (authMeter.get("passMeter") != null && !authMeter.get("passMeter").equals("")) {
                    passMeter = authMeter.get("passMeter");
                }

                if (authMeter.get("security") != null && !authMeter.get("security").equals("")) {
                    security = authMeter.get("security");
                }

                if (authMeter.get("authenticationKey") != null && !authMeter.get("authenticationKey").equals("")) {
                    authenticationKey = authMeter.get("authenticationKey");
                }

                if (authMeter.get("blockCipherKey") != null && !authMeter.get("blockCipherKey").equals("")) {
                    blockCipherKey = authMeter.get("blockCipherKey");
                }

                if (authMeter.get("systemTitle") != null && !authMeter.get("systemTitle").equals("")) {
                    systemTitle = authMeter.get("systemTitle");
                }

                if (authMeter.get("phasa") != null && !authMeter.get("phasa").equals("")) {
                    phasa = Integer.parseInt(authMeter.get("phasa"));
                }

                if (authMeter.get("merkMeter") != null && !authMeter.get("merkMeter").equals("")) {
                    merkMeter = authMeter.get("merkMeter");
                }

                if (authMeter.get("typeMeter") != null && !authMeter.get("typeMeter").equals("")) {
                    typeMeter = authMeter.get("typeMeter");
                }

                if (authMeter.get("targetAddress") != null && !authMeter.get("targetAddress").equals("")) {
                    targetAddress = authMeter.get("targetAddress");
                }

                if (authMeter.get("interfaceType") != null && !authMeter.get("interfaceType").equals("")) {
                    interfaceType = authMeter.get("interfaceType");
                }

                if (authMeter.get("noDevice") != null && !authMeter.get("noDevice").equals("")) {
                    noDevice = authMeter.get("noDevice");
                }

                if (authMeter.get("typeDevice") != null && !authMeter.get("typeDevice").equals("")) {
                    noDevice = authMeter.get("typeDevice");
                }

                if (authMeter.get("typeObis") != null && !authMeter.get("typeObis").equals("")) {
                    typeObis = authMeter.get("typeObis");
                }

                if (authMeter.get("noSeluler") != null && !authMeter.get("noSeluler").equals("")) {
                    noSeluler = authMeter.get("noSeluler");
                }

                if (authMeter.get("noSimcard") != null && !authMeter.get("noSimcard").equals("")) {
                    noSeluler = authMeter.get("noSimcard");
                }

                if (authMeter.get("kdPusat") != null && !authMeter.get("kdPusat").equals("")) {
                    kdPusat = authMeter.get("kdPusat");
                }

                if (authMeter.get("kdUnitBisnis") != null && !authMeter.get("kdUnitBisnis").equals("")) {
                    kdUnitBisnis = authMeter.get("kdUnitBisnis");
                }

                if (authMeter.get("kdPembangkit") != null && !authMeter.get("kdPembangkit").equals("")) {
                    kdPembangkit = authMeter.get("kdPembangkit");
                }

                if (authMeter.get("kdArea") != null && !authMeter.get("kdArea").equals("")) {
                    kdArea = authMeter.get("kdArea");
                }

                if (authMeter.get("jenis") != null && !authMeter.get("jenis").equals("")) {
                    jenis = authMeter.get("jenis");
                }

                if (authMeter.get("addressSize") != null && !authMeter.get("addressSize").equals("")) {
                    addressSize = authMeter.get("addressSize");
                }
            }
            System.out.println("addressSize : " + addressSize);
            p.setDbObject(csr);
            p.setAuthenticationLevel(authenticationLevel.toString());
            p.setUseLogicalNameReferencing(true);

            p.setClientAddress(clientAddress.equals("null") ? 0 : Integer.parseInt(clientAddress));

            p.setLogicalAddress(logicalAddress.equals("null") ? 0 : Integer.parseInt(logicalAddress));

            p.setPhysicalAddress(physicalAddress.equals("null") ? 0 : Integer.parseInt(physicalAddress));

            p.setTargetAddress(targetAddress.equals("") ? 0 : Integer.parseInt(targetAddress));
            p.setPassword(passMeter);
            p.setTraceLevel("OFF");
            p.setHostName(ipAddress);
            if (port != null && !port.equals("")) {
                p.setPort(Integer.parseInt(port));
            }
            p.setSecurity(security);
            p.setAuthenticationKey(authenticationKey);
            p.setBlockCipherKey(blockCipherKey);
            p.setSystemTitle(systemTitle);
            p.setPhasa(phasa);
            p.setNoMeter(noMeter);
            p.setTypeMeter(typeMeter);
            p.setMerkMeter(merkMeter);
            p.setKdArea(kdArea);
            p.setKdPembangkit(kdPembangkit);
            p.setKdPusat(kdPusat);
            p.setKdUnitBisnis(kdUnitBisnis);
            p.setJenis(jenis);
            p.setInterfaceType(interfaceType);
            p.setNoDevice(noDevice);
            p.setTypeDevice(typeDevice);
            p.setTypeObis(typeObis);
            p.setNoSeluler(noSeluler);
            p.setNoSimcard(noSimcard);
            p.setAddressSize(Integer.parseInt(addressSize));
            final int ret = getParameters(client, p);
            if (ret != 0) {
                return null;
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
        return p;
    }

    static int getParameters(final GXDLMSSecureClient client, final Params p) {

        try {
            System.out.println("getParameters ===>" + p.getNoMeter());
            LOGGER.info("NoMeter : " + p.getNoMeter());
            LOGGER.info("Phasa : " + p.getPhasa());
            LOGGER.info("getUseLogicalNameReferencing : " + p.getUseLogicalNameReferencing());
            LOGGER.info("getAuthenticationLevel : " + p.getAuthenticationLevel());
            LOGGER.info("getClientAddress : " + p.getClientAddress());
            LOGGER.info("getTargetAddress : " + p.getTargetAddress());
            LOGGER.info("getPassword : " + p.getPassword());
            LOGGER.info("getHostName : " + p.getHostName());

            LOGGER.info("getPort : " + p.getPort());
            LOGGER.info("getLogicalAddress : " + p.getLogicalAddress());
            LOGGER.info("getPhysicalAddress : " + p.getPhysicalAddress());
            LOGGER.info("getAddressSize : " + p.getAddressSize());
            LOGGER.info("getAuthenticationKey : " + p.getAuthenticationKey());
            LOGGER.info("getSecurity : " + p.getSecurity());
            LOGGER.info("getBlockCipherKey : " + p.getBlockCipherKey());
            LOGGER.info("getSystemTitle : " + p.getSystemTitle());
            LOGGER.info("getInterface : " + p.getInterfaceType());
            LOGGER.info("IsDynamic : " + p.getIsDynamic());
            LOGGER.info("noDevice : " + p.getNoDevice());
            LOGGER.info("TypeObis : " + p.gettypeObis());

            // GXNet net = null;
            client.setUseLogicalNameReferencing(p.getUseLogicalNameReferencing());
            client.setAuthentication(Authentication.valueOfString(p.getAuthenticationLevel()));
            System.out.println(client.getAuthentication());
            if (p.getClientAddress() != null && p.getClientAddress() != 0) {
                client.setClientAddress(p.getClientAddress());
            }

            // if (p.getHostName() != null && !p.getHostName().equals("")) {
            // // Host address.
            // if (settings.media == null) {
            // settings.media = new GXNet();
            // }
            // net = (GXNet) settings.media;
            // net.setHostName(p.getHostName());
            // }
            // if (p.getPort() != null && p.getPort() != 0) {
            // net.setPort(p.getPort());
            // }
            // settings.trace = p.getTraceLevel();
            // GXDLMSGateway gw = new GXDLMSGateway();
            // gw.setNetworkId((short)0);
            // gw.setPhysicalDeviceAddress(p.getPhysicalAddress().toString().getBytes());
            // settings.client.setGateway(null);
            // settings.iec = p.getUseIEC();
            // if (p.getUseIEC()) {
            // settings.client.setInterfaceType(InterfaceType.WRAPPER);
            // settings.client.setServerAddress(p.getPhysicalAddress());
            // } else {
            client.setInterfaceType(p.getInterfaceType());
            if (p.getLogicalAddress() > 0 || p.getClientAddress() > 0 || p.getTargetAddress() > 0) {
                if (p.getTargetAddress() > 0) {
                    client.setServerAddress(p.getTargetAddress());
                } else {
                    if (p.getInterfaceType().equals(InterfaceType.WRAPPER)) {
                        client.setServerAddress(p.getLogicalAddress());
                    } else {
                        client.setServerAddress(
                                client.getServerAddress(p.getLogicalAddress(), p.getPhysicalAddress(), p.getAddressSize()));
                    }
                }
            }
            System.out.println("getServerAddress : " + client.getServerAddress());
            // }

            // if (p.getSerialPort() != null && !p.getSerialPort().equals("")) {
            // settings.media = new GXSerial();
            // GXSerial serial = (GXSerial) settings.media;
            // serial.setPortName(p.getSerialPort());
            // }
            // Chipper Set
            String sKey = "00000000000000000000000000000000";
            Aes128Ecb gen = new Aes128Ecb(sKey);
            byte[] ctosChallenge = GXSecure.generateChallenge(Authentication.HIGH_GMAC);
            System.out.println("Authentication ===> " + GXCommon.bytesToHex(ctosChallenge));
            System.out.println(gen.byteArrayToHexString(ctosChallenge));
            client.getCiphering().reset();
            if (p.getAuthenticationLevel().equals("HighGMac")) {
                System.out.println(p.getAuthenticationLevel());

                client.getCiphering().setSecurity(p.getSecurity());
                System.out.println("=======================...");

                byte[] blockChipper = gen.encrypt(p.getBlockCipherKey().getBytes("ASCII"));
                System.out.println("AES128 ===> " + GXCommon.bytesToHex(blockChipper));
                // client.getCiphering().setBlockCipherKey(p.getBlockCipherKey().getBytes("ASCII"));
                // client.getCiphering().setAuthenticationKey(p.getAuthenticationKey().getBytes("ASCII"));
                client.getCiphering().setBlockCipherKey(blockChipper);
                client.getCiphering().setAuthenticationKey(blockChipper);

                client.getCiphering().setSystemTitle(p.getSystemTitle().getBytes("ASCII"));
                client.getCiphering().setInvocationCounter(1);
                // settings.client.getCiphering().setInvocationCounter(30105);
                // settings.client.getCiphering().setSecuritySuite(SecuritySuite.AES_GCM_128);
            } else {
                // byte[] blockChipper = gen.encrypt(p.getPassword().getBytes("ASCII"));
                client.setPassword(p.getPassword().getBytes("ASCII"));
                // client.setPassword(blockChipper);
            }

            // if (p.getSelectedObject() != null && !p.getSelectedObject().equals("")) {
            // for (String o : p.getSelectedObject().split("[;,]")) {
            // String[] tmp = o.split("[:]");
            // if (tmp.length != 2) {
            // throw new IllegalArgumentException("Invalid Logical name or attribute
            // index.");
            // }
            // settings.readObjects
            // .add(new GXSimpleEntry<String, Integer>(tmp[0].trim(),
            // Integer.parseInt(tmp[1].trim())));
            // }
            // }
            // if (settings.media == null) {
            // // showHelp();
            // return 1;
            // }
            return 0;
        } catch (final Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public HashMap<String, String> MappingAuthMeter(final String AuthMeter) throws Exception {
        final HashMap<String, String> hmap = new HashMap<String, String>();
        if (AuthMeter != null && !AuthMeter.equals("")) {
            System.out.println("MappingAuthMeter ===>");
            final JSONParser parser = new JSONParser();
            System.out.println(new String(Base64.getDecoder().decode(AuthMeter)));
            final JSONObject json = (JSONObject) parser.parse(new String(Base64.getDecoder().decode(AuthMeter)));
            System.out.println(json);
            hmap.put("authenticationLevel", convertObject(json.get("authenticationLevel")));
            hmap.put("clientAddress", convertObject(json.get("clientAddress")));
            hmap.put("logicalAddress", convertObject(json.get("logicalAddress")));
            hmap.put("physicalAddress", convertObject(json.get("physicalAddress")));
            hmap.put("ipAddress", convertObject(json.get("ipAddress")));
            hmap.put("port", convertObject(json.get("port")));
            hmap.put("passMeter", convertObject(json.get("passMeter")));
            hmap.put("security", convertObject(json.get("security")));
            hmap.put("authenticationKey", convertObject(json.get("authenticationKey")));
            hmap.put("blockCipherKey", convertObject(json.get("blockCipherKey")));
            hmap.put("systemTitle", convertObject(json.get("systemTitle")));
            hmap.put("phasa", convertObject(json.get("phasa")));
            hmap.put("getResult", convertObject(json.get("getResult")));
            hmap.put("phasa", convertObject(json.get("phasa")));
            hmap.put("typeMeter", convertObject(json.get("typeMeter")));
            hmap.put("interfaceType", convertObject(json.get("interfaceType")));
            hmap.put("targetAddress", convertObject(json.get("targetAddress")));
            hmap.put("typeMeter", convertObject(json.get("typeMeter")));
            hmap.put("merkMeter", convertObject(json.get("merkMeter")));
            hmap.put("noDevice", convertObject(json.get("noDevice")));
            hmap.put("typeDevice", convertObject(json.get("typeDevice")));
            hmap.put("typeObis", convertObject(json.get("typeObis")));
            hmap.put("noSeluler", convertObject(json.get("noSeluler")));
            hmap.put("noSimcard", convertObject(json.get("noSimcard")));
            hmap.put("kdPusat", convertObject(json.get("kdPusat")));
            hmap.put("kdUnitBisnis", convertObject(json.get("kdUnitBisnis")));
            hmap.put("kdPembangkit", convertObject(json.get("kdPembangkit")));
            hmap.put("kdArea", convertObject(json.get("kdArea")));
            hmap.put("jenis", convertObject(json.get("jenis")));
            hmap.put("idAntrian", convertObject(json.get("idAntrian")));
            hmap.put("addressSize", convertObject(json.get("addressSize")));
            hmap.put("noMeter", convertObject(json.get("noMeter")));
        }
        return hmap;
    }

    private String convertObject(final Object obj) {
        if (obj != null) {
            return (String) obj;
        } else {
            return "";
        }
    }
}
