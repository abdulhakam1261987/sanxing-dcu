package id.co.tabs.hes.dcu.reader;

import gurux.dlms.enums.Unit;
import java.text.DecimalFormatSymbols;

public class SkaladanUnit {

    private double skala;
    private Unit unit;

    public SkaladanUnit() {
    }

    public SkaladanUnit(String val) {
        String nilaiAsli = String.valueOf(val);
        int spasiKe = 0;
        String scaller = "";
        String unitt = "";
        boolean readSkala = false;
        boolean readUnit = false;
        int upperunit = 0;
        for (int i = 0; i < nilaiAsli.length(); i++) {
            String karakter = nilaiAsli.substring(i, i + 1);
            if (karakter.equals(" ") && spasiKe == 0) {
                spasiKe++;
                readSkala = true;
            } else if (karakter.equals(" ") && spasiKe == 1) {
                spasiKe++;
                readSkala = false;
            } else if (karakter.equals(" ") && spasiKe == 2) {
                spasiKe++;
                readUnit = true;
            } else if (karakter.equals(" ") && spasiKe == 3) {
                break;
            }
            if (readSkala) {
                scaller = scaller + karakter;
            }
            if (readUnit) {
                if (karakter.equals(" ")) {
                    continue;
                }
                if (karakter.equals(karakter.toUpperCase()) && upperunit > 0) {
                    unitt = unitt + "_" + karakter;
                } else {
                    if (karakter.equals(karakter.toUpperCase())) {
                        unitt = unitt + karakter;
                        upperunit++;
                    } else {
                        unitt = unitt + karakter;
                    }
                }
            }
        }
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        int a = dfs.getDecimalSeparator();
        if (a == 46) {
           this.skala = Double.parseDouble(scaller); 
        } else {
            scaller = scaller.replace(dfs.getDecimalSeparator(), '.');
            this.skala = Double.parseDouble(scaller);
        }
        try {
            this.unit = Unit.valueOf(unitt.trim().toUpperCase().replace(" ", "_"));
        } catch (Exception e) {
            if (unitt.equals("NOUNIT")) {
                this.unit = Unit.NO_UNIT;
            } else {
                this.unit = Unit.NONE;
            }
        }
    }

    public double getSkala() {
        return skala;
    }

    public void setSkala(double skala) {
        this.skala = skala;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
