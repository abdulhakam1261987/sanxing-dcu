package id.co.tabs.hes;

import gurux.common.GXCommon;
import java.io.File;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {

    private static final Logger LOG = Logger.getLogger(Utils.class.getName());
    
    public static void prepareDirectory(String path) {
        File f = new File(path);
        if (!f.exists())
            f.mkdirs();
    }
    
    /**
     * Salinan dari method showValue() bawaan Gurux di class
     * gurux.dlms.client.GXDLMSReader. Hanya menambahkan return String.
     *
     * @param pos Index attribute.
     * @param value Hasil baca GXDLMSObject.
     *
     * @return Hasil baca dalam bentuk String.
     */
    public static String lihatNilai(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = new String((byte[]) val);
        } else if (val instanceof Double) {
            NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            LOG.log(Level.INFO, "List");
            StringBuilder sb = new StringBuilder();
            for (Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                if (tmp instanceof byte[]) {
                    LOG.log(Level.INFO, "\tList array byte : " + (new String((byte[]) tmp)));
                    sb.append(new String((byte[]) tmp));
                } else {
                    LOG.log(Level.INFO, "\tList : " + (String.valueOf(tmp)) + "\t" + (tmp instanceof List));
                    sb.append(String.valueOf(tmp));
                    for (Object object : (List) tmp) {
                        if (object instanceof List) {
                            System.out.println("\t\t" + ((List) object).size());
                            for (Object object1 : (List) object) {
                                System.out.println("\t\t\t" + object1);
                            }
                        }else if(object instanceof byte[]){
                            System.out.println("\t\t" + GXCommon.bytesToHex((byte[]) object) + "\t" + 
                                    (new String((byte[]) object)));
                        }else{
                            System.out.println("\t\t" + object);
                        }
                        
                    }
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            LOG.log(Level.INFO, "Array");
            StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        return String.valueOf(val);
    }
    
    public static String arrayByteToString(byte[] bs) {
        String nilai = "";
        for (byte b : bs) {
            char c = (char) b;
            nilai = nilai + c;
        }
        return nilai;
    }
}
